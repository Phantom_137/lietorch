#pragma once
#ifndef LIETORCH_CPU_R2_CPU_H
#define LIETORCH_CPU_R2_CPU_H

#include "../torch_include.h"

namespace lietorch
{
namespace r2
{

/*

*/
std::tuple<torch::Tensor, torch::Tensor>
morphological_convolution_fw_cpu(
    const torch::Tensor& input, const torch::Tensor& kernel);

/*

 */
std::tuple<torch::Tensor, torch::Tensor>
morphological_convolution_bw_cpu(
    const torch::Tensor& grad,
    const torch::Tensor& backindex,
    const torch::IntArrayRef kernel_sizes);

/*

 */
std::tuple<torch::Tensor, torch::Tensor>
convection_fw_cpu(const torch::Tensor& input, const torch::Tensor& g0);

/*

 */
std::tuple<torch::Tensor, torch::Tensor>
convection_bw_cpu(
    const torch::Tensor& g0,
    const torch::Tensor& grad,
    const torch::Tensor& out_grad_field);

} // namespace r2
} // namespace lietorch

#endif