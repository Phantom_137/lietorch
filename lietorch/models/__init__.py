"""
    Models used in various experiments that serve as examples.
"""

import lietorch.models.dca1
import lietorch.models.drive
import lietorch.models.rotnist
