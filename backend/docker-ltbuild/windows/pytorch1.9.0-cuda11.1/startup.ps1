# Add a public Verisign and Level3 DNS server just to make sure DNS resolution is available.
$list = (Get-DnsClientServerAddress -InterfaceIndex 4 -AddressFamily IPv4).ServerAddresses + ("64.6.64.6", "209.244.0.3")
Set-DnsClientServerAddress -InterfaceIndex 4 -ServerAddresses $list