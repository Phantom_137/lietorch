#pragma once
#ifndef LIETORCH_R2_H
#define LIETORCH_R2_H

/** @file r2.h
 *  @brief R2 operators
 *
 */

#include "torch_include.h"

namespace lietorch
{
namespace r2
{

using torch::autograd::AutogradContext;
using torch::autograd::Function;
using torch::autograd::tensor_list;
using torch::autograd::Variable;
using torch::autograd::variable_list;

////////////////////////////////////////////////
/////
/////   Convection
/////
////////////////////////////////////////////////

/**
 *  @brief Left invariant convection on R2 (forward).
 *
 *  @param input Input R2 tensor of shape `[B,C,H,W]`
 *  @param g0 Convection tensor of shape `[C,2]` which consists of a pair of
 * coordinates `{y0, x0}` for each channel that specifies the convection
 * to apply.
 *
 *  @return A tuple of tensors, the first tensor is the transformed input and as
 * such has the same shape. The second tensor contains the gradient at each
 * gridpoint of the output specified in left the left invariant frame which is
 * required for a subsequent backward pass, it has shape `[B,C,H,W,2]`.
 *
 */
std::tuple<torch::Tensor, torch::Tensor>
convection_fw(const torch::Tensor& input, const torch::Tensor& g0);

/**
 *  @brief Left invariant convection on R2 (backward).
 *
 *  @param g0 Convection tensor of shape `[C,2]` which consists of a triple of
 * coordinates `{y0, x0}` for each channel that specifies the convection
 * to apply.
 *  @param grad Gradient of the output.
 *  @param out_grad_field Second return tensor of the forward pass that is
 * needed to ompute the backward pass.
 *
 * @return A tuple of tensors: the first gives the gradient of the input (with
 * shape `[B,C,H,W]`), the second gives the gradient of the convection
 * parameters `g0` with shape `[C,2]`.
 *
 */
std::tuple<torch::Tensor, torch::Tensor>
convection_bw(
    const torch::Tensor& g0,
    const torch::Tensor& grad,
    const torch::Tensor& out_grad_field);

/**
 * @brief Left invariant convection on R2.
 *
 *  Note that `input` and `g0` need not have the same dtype, the output tensor
 * will have the same dtype as the input.
 *
 * @param input Tensor of shape `[B,C,H,W]`
 * @param g0 Convection tensor of shape `[C,2]` which consists of a pair of
 * coordinates `{y0, x0}` for each channel that specifies the convection
 * to apply.
 *
 * @return A tensor with the same dtype and shape as the input (`[B,C,H,W]`).
 */
torch::Tensor
convection(const torch::Tensor& input, const torch::Tensor& g0);

/**
 *
 * @brief Left invariant convection on R2 (Function).
 *
 */
class Convection : public Function<Convection>
{
public:
    static tensor_list
    forward(AutogradContext* ctx, torch::Tensor input, torch::Tensor g0);

    static tensor_list backward(AutogradContext* ctx, tensor_list grads);
};

//////////////////////////////////////////////
///
/// Morphological convolution
///
/////////////////////////////////////////////

/**
 * @brief Morphological Convolution on R2 (Forward)
 *
 * @param input Tensor of shape `[B,C,H,W]`
 * @param kernel Kernel tensor of shape `[C,kH,kW]` of the same scalar type
 * as input.
 *
 * @return A pair of tensors: the first the output of shape `[B,C,H,W]` and a
 * back_index tensor of shape `[B,C,H,W,2]` that is needed for the backward
 * pass.
 */
std::tuple<torch::Tensor, torch::Tensor>
morphological_convolution_fw(
    const torch::Tensor& input, const torch::Tensor& kernel);

/**
 * @brief Morphological Convolution on R2 (Backward)
 *
 * @param grad Gradient of the output
 * @param back_index Back index tensor from the forward pass
 * @param kernel_sizes Size of the original kernel: `{kH, kW}`, i.e. not
 * including the number of channels
 *
 * @return A pair of tensors: the first is the gradient with respect to the
 * input, the second the gradient with respect to the kernel.
 *
 */
std::tuple<torch::Tensor, torch::Tensor>
morphological_convolution_bw(
    const torch::Tensor& grad,
    const torch::Tensor& backindex,
    const torch::IntArrayRef kernel_sizes);

/**
 * @brief Morphological convolution on R2.
 *
 *
 * @param input Tensor of shape `[B,C,H,W]`
 * @param kernel Kernel tensor of shape `[C,kH,kW]`
 *
 * @return A tensor with the same dtype and shape as the input (`[B,C,H,W]`).
 */
torch::Tensor
morphological_convolution(
    const torch::Tensor& input, const torch::Tensor& kernel);

/**
 *
 * @brief Morphological convolution on R2 (Function).
 *
 */
class MorphologicalConvolution : public Function<MorphologicalConvolution>
{
public:
    static tensor_list
    forward(AutogradContext* ctx, torch::Tensor input, torch::Tensor kernel);

    static tensor_list backward(AutogradContext* ctx, tensor_list grads);
};

////////////////////////////////////////////////
/////
/////   Linear Combinations
/////
////////////////////////////////////////////////

/**
 * @brief Linear combination of R2 data
 *
 * @param input Tensor of shape `[B,Cin,H,W]`
 * @param weight Tensor of shape `[Cin,Cout]`
 *
 * @return Tensor of shape `[B,Cout,H,W]`
 */
torch::Tensor
linear(const torch::Tensor& input, const torch::Tensor& weight);

////////////////////////////////////////////////
/////
/////   Morphological kernel
/////
////////////////////////////////////////////////

/**
 * @brief Generate R2 morphological kernels associated with a
 * circular-harmonics-based Finsler function.
 *
 * @param finsler_params Tensor of shape `[... , P]`
 * @param kernel_radius Radius of the grid on which to sample the kernel, i.e.
 * the grid will have a height and width of `2*kernel_radius+1`.
 * @param alpha Alpha factor must be >= 0.55 and <= 1.0
 *
 * @return Tensor of shape `[... , 2*kernel_radius+1, 2*kernel_radius+1]`.
 */
torch::Tensor
morphological_kernel(
    const torch::Tensor& finsler_params,
    const int64_t kernel_radius,
    const double alpha);

////////////////////////////////////////////////
/////
/////   Fractional Dilation
/////
////////////////////////////////////////////////

/**
 * @brief R2 left invariant dilation
 *
 * @param input Tensor of shape `[B,C,H,W]`
 * @param finsler_params Tensor of shape `[C,P]`
 * @param kernel_radius Specifies the size of the grid to sample on as
 * `[2*radius+1, 2*radius+1}`.
 * @param alpha Alpha parameter, needs to be in the range [0.55, 1.0].
 *
 * @return Tensor of shape `[B,C,H,W]`
 */
torch::Tensor
fractional_dilation(
    const torch::Tensor& input,
    const torch::Tensor& finsler_params,
    const int64_t kernel_radius,
    const double alpha);

/////////////////////////////////////////////////////
/////
/////   Fractional Erosion
/////
/////////////////////////////////////////////////////

/**
 * @brief R2 left invariant erosion
 *
 * @param input Tensor of shape `[B,C,H,W]`
 * @param finsler_params Tensor of shape `[C,P]`
 * @param kernel_radius Specifies the size of the grid to sample on as
 * `[2*radius+1, 2*radius+1}`.
 * @param alpha Alpha parameter, needs to be in the range [0.55, 1.0].
 *
 * @return Tensor of shape `[B,C,H,W]`
 */
torch::Tensor
fractional_erosion(
    const torch::Tensor& input,
    const torch::Tensor& finsler_params,
    const int64_t kernel_radius,
    const double alpha);

/////////////////////////////////////////////////////
/////
/////   Fractional Diffusion
/////
/////////////////////////////////////////////////////

/////////////////////////////////////////////////////
/////
/////   Utility functions for constructing kernels
/////
/////////////////////////////////////////////////////

/**
 * @brief Circular harmonics based basis functions for Finsler functions
 * computed on a 2D grid. Each basis function is computed as:
 *  ```
 *          out_i(x,y) = 1/2*log(x^2+y^2)*B_i(atan2(y,x))
 *  ```
 * where `(x,y)=(0,0)` is the center of the specified grid and B_i is the i-th
 * circular harmonic function.
 *
 * @param orders How many basis functions to compute
 * @param h Grid height
 * @param w Grid weight
 * @param options Options the tensor will be constructed with
 *
 * @return Tensor of shape `[orders, h, w]`
 */
torch::Tensor
circular_harmonic_basis(
    const int64_t orders,
    const int64_t height,
    const int64_t width,
    const torch::TensorOptions& options);

/**
 * @brief Euclidean norm on a 2D grid where the origin is taken at the center of
 * the grid.
 *
 * @param h Grid height
 * @param w Grid weight
 * @param options Options the tensor will be constructed with
 *
 * @return Tensor of shape `[h, w]`
 */
torch::Tensor
euclidean_norm_grid(
    const int64_t height,
    const int64_t width,
    const torch::TensorOptions& options);

} // namespace r2
} // namespace lietorch

#endif