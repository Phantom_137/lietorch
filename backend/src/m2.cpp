/** @file m2.cpp
 *  @brief M2 operators
 *
 */

#include "m2.h"
#include "macros.h"

#include "cpu/m2_cpu.h"

#ifdef WITH_CUDA
#include "cuda/m2_cuda.cuh"
#endif

namespace lietorch
{
namespace m2
{

///////////////////////////////////////////////////////////
////
////    Anisotropic Dilated Projection
////
///////////////////////////////////////////////////////////

/*

*/
std::tuple<torch::Tensor, torch::Tensor>
anisotropic_dilated_project_fw(
    const torch::Tensor& input,
    const double longitudinal,
    const double lateral,
    const double alpha,
    const double scale)
{
    if (input.is_cuda())
    {
#if defined(WITH_CUDA)
        return anisotropic_dilated_project_fw_cuda(
            input, longitudinal, lateral, alpha, scale);
#else
        TORCH_CHECK(false, "Not compiled with CUDA support");
#endif
    }
    else
    {
        return anisotropic_dilated_project_fw_cpu(
            input, longitudinal, lateral, alpha, scale);
    }
}

/*

*/
torch::Tensor
anisotropic_dilated_project_bw(
    const torch::Tensor& backindex,
    const torch::Tensor& grad,
    const torch::List<int64_t> input_shape)
{
    auto backindex_arg = torch::TensorArg(backindex, "backindex", 1);
    auto grad_arg = torch::TensorArg(grad, "grad", 2);

    torch::checkAllDefined(__func__, {backindex_arg, grad_arg});
    torch::checkAllContiguous(__func__, {backindex_arg, grad_arg});
    torch::checkDim(__func__, backindex_arg, 5);
    torch::checkDim(__func__, grad_arg, 4);
    torch::checkScalarType(__func__, backindex_arg, torch::kInt64);

    if (grad.is_cuda())
    {
#if defined(WITH_CUDA)
        torch::checkAllSameGPU(__func__, {backindex_arg, grad_arg});
        return anisotropic_dilated_project_bw_cuda(
            backindex, grad, input_shape);
#else
        TORCH_CHECK(false, "Not compiled with CUDA support");
#endif
    }
    else
    {
        return anisotropic_dilated_project_bw_cpu(backindex, grad, input_shape);
    }
}

/*

*/
torch::Tensor
anisotropic_dilated_project(
    const torch::Tensor& input,
    const double longitudinal,
    const double lateral,
    const double alpha,
    const double scale)
{
    auto input_arg = torch::TensorArg(input, "input", 1);

    torch::checkDefined(__func__, input_arg);
    torch::checkDim(__func__, input_arg, 5);
    LT_ARGUMENT_CHECK(longitudinal > 0, __func__, "must be positive");
    LT_ARGUMENT_CHECK(lateral > 0, __func__, "must be positive");
    LT_ARGUMENT_CHECK(
        alpha > 0.5 && alpha <= 1.0, __func__, "must be in range in (0.5, 1]");
    LT_ARGUMENT_CHECK(scale > 0, __func__, "must be positive");

    return AnisotropicDilatedProject::apply(
        input, longitudinal, lateral, alpha, scale)[0];
}

/*

*/
variable_list
AnisotropicDilatedProject::forward(
    AutogradContext* ctx,
    Variable input,
    const double longitudinal,
    const double lateral,
    const double alpha,
    const double scale)
{
    const auto [out, backindex] = anisotropic_dilated_project_fw(
        input.contiguous(), longitudinal, lateral, alpha, scale);

    ctx->save_for_backward({backindex});
    ctx->saved_data["input_shape"] = torch::List<int64_t>(input.sizes());

    return {out};
}

/*

*/
variable_list
AnisotropicDilatedProject::backward(AutogradContext* ctx, variable_list grads)
{
    const auto grad = grads[0];
    const auto backindex = ctx->get_saved_variables()[0];
    const torch::List<int64_t> input_shape =
        ctx->saved_data["input_shape"].toIntList();

    const auto input_grad =
        anisotropic_dilated_project_bw(backindex, grad, input_shape);

    return {input_grad, Variable(), Variable(), Variable(), Variable()};
}

////////////////////////////////////////////////
/////
/////   Convection
/////
////////////////////////////////////////////////

/*
 */
std::tuple<torch::Tensor, torch::Tensor>
convection_fw(
    const torch::Tensor& input, // [B,C,Ors,H,W]
    const torch::Tensor& g0     // [C,3]
)
{
    const auto input_arg = torch::TensorArg(input, "input", 1);
    const auto g0_arg = torch::TensorArg(g0, "g0", 2);

    torch::checkAllDefined(__func__, {input_arg, g0_arg});
    torch::checkDim(__func__, input_arg, 5);
    torch::checkDim(__func__, g0_arg, 2);

    if (input.is_cuda())
    {
#if defined(WITH_CUDA)
        torch::checkAllSameGPU(__func__, {input_arg, g0_arg});
        return convection_fw_cuda(input.contiguous(), g0.contiguous());
#else
        TORCH_CHECK(false, "Not compiled with CUDA support");
#endif
    }
    else
    {
        return convection_fw_cpu(input.contiguous(), g0.contiguous());
    }
}

/*
 */
std::tuple<torch::Tensor, torch::Tensor>
convection_bw(
    const torch::Tensor& g0,            // [C,3]
    const torch::Tensor& grad,          // [B,C,Ors,H,W]
    const torch::Tensor& out_grad_field // [B,C,Ors,H,W,3]
)
{
    const auto grad_arg = torch::TensorArg(grad, "grad", 1);
    const auto out_grad_field_arg =
        torch::TensorArg(out_grad_field, "out_grad_field", 2);
    const auto g0_arg = torch::TensorArg(g0, "g0", 3);

    torch::checkAllDefined(__func__, {grad_arg, out_grad_field_arg, g0_arg});
    torch::checkDim(__func__, grad_arg, 5);
    torch::checkDim(__func__, out_grad_field_arg, 6);
    torch::checkDim(__func__, g0_arg, 2);

    if (grad.is_cuda())
    {
#if defined(WITH_CUDA)
        torch::checkAllSameGPU(__func__, {grad_arg, out_grad_field_arg});
        return convection_bw_cuda(
            g0.contiguous(), grad.contiguous(), out_grad_field.contiguous());
#else
        TORCH_CHECK(false, "Not compiled with CUDA support");
#endif
    }
    else
    {
        return convection_bw_cpu(
            g0.contiguous(), grad.contiguous(), out_grad_field.contiguous());
    }
}

/*

*/
torch::Tensor
convection(
    const torch::Tensor& input, // [B,C,Ors,H,W]
    const torch::Tensor& g0     // [C,3]
)
{
    return Convection::apply(input, g0)[0];
}

/*

*/
tensor_list
Convection::forward(
    AutogradContext* ctx, 
    torch::Tensor input, // [B,C,Ors,H,W]
    torch::Tensor g0     // [C,3]
)
{
    const auto [out, out_grad_field] = convection_fw(input, g0);

    ctx->save_for_backward({g0, out_grad_field});

    return {out};
}

/*

*/
tensor_list
Convection::backward(AutogradContext* ctx, tensor_list grads)
{
    const auto g0 = ctx->get_saved_variables()[0];
    const auto out_grad_field = ctx->get_saved_variables()[1];
    const auto grad = grads[0];

    const auto [input_grad, g0_grad] = convection_bw(g0, grad, out_grad_field);

    return {input_grad, g0_grad};
}

////////////////////////////////////////////////
/////
/////   Linear Convolution
/////
////////////////////////////////////////////////

/*


*/
torch::Tensor
linear_convolution_fw(
    const torch::Tensor& input, // [B,C,Ors,W,H]
    const torch::Tensor& kernel // [C,kOrs,kW,kH]
)
{
    const auto input_arg = torch::TensorArg(input, "input", 1);
    const auto kernel_arg = torch::TensorArg(kernel, "kernel", 2);

    torch::checkAllDefined(__func__, {input_arg, kernel_arg});
    torch::checkDim(__func__, input_arg, 5);
    torch::checkDim(__func__, kernel_arg, 4);
    torch::checkSameType(__func__, input_arg, kernel_arg);

    if (input.is_cuda())
    {
#if defined(WITH_CUDA)
        torch::checkAllSameGPU(__func__, {input_arg, kernel_arg});
        return linear_convolution_fw_cuda(input.contiguous(), kernel.contiguous());
#else
        TORCH_CHECK(false, "Not compiled with CUDA support");
#endif
    }
    else
    {
        return linear_convolution_fw_cpu(input.contiguous(), kernel.contiguous());
    }
}

/*


*/
std::tuple<torch::Tensor, torch::Tensor>
linear_convolution_bw(
    const torch::Tensor& grad,  // [B,C,Ors,H,W]
    const torch::Tensor& input, // [B,C,Ors,H,W]
    const torch::Tensor& kernel // [C,kOrs,kH,kW]
)
{
    const auto grad_arg = torch::TensorArg(grad, "grad", 1);
    const auto input_arg = torch::TensorArg(input, "input", 2);
    const auto kernel_arg = torch::TensorArg(kernel, "kernel", 3);

    torch::checkAllDefined(__func__, {grad_arg, input_arg, kernel_arg});
    torch::checkDim(__func__, grad_arg, 5);
    torch::checkDim(__func__, input_arg, 5);
    torch::checkDim(__func__, kernel_arg, 4);
    torch::checkSameType(__func__, input_arg, kernel_arg);
    torch::checkSameType(__func__, grad_arg, kernel_arg);
    

    if (grad.is_cuda())
    {
#if defined(WITH_CUDA)
        torch::checkAllSameGPU(__func__, {grad_arg, input_arg, kernel_arg});
        return linear_convolution_bw_cuda(grad.contiguous(), input.contiguous(), kernel.contiguous());
#else
        TORCH_CHECK(false, "Not compiled with CUDA support");
#endif
    }
    else
    {
        return linear_convolution_bw_cpu(grad.contiguous(), input.contiguous(), kernel.contiguous());
    }
}

/*


*/
torch::Tensor
linear_convolution(
    const torch::Tensor& input, // [B,C,Ors,H,W]
    const torch::Tensor& kernel // [C,kOrs,kH,kW]
)
{
    return LinearConvolution::apply(input, kernel)[0];
}

/*


*/
tensor_list
LinearConvolution::forward(
    AutogradContext* ctx, 
    torch::Tensor input, // [B,C,Ors,H,W]
    torch::Tensor kernel // [C,kOrs,kH,kW]
)
{
    const auto out = linear_convolution_fw(input, kernel);

    ctx->save_for_backward({input.detach(), kernel.detach()});

    return {out};
}

/*

*/
tensor_list
LinearConvolution::backward(
    AutogradContext* ctx, 
    tensor_list grads
)
{
    const auto input = ctx->get_saved_variables()[0];
    const auto kernel = ctx->get_saved_variables()[1];
    const auto grad = grads[0];

    const auto [input_grad, kernel_grad] = linear_convolution_bw(grad, input, kernel);

    return {input_grad, kernel_grad};
}

////////////////////////////////////////////////
/////
/////   Morphological Convolution
/////
////////////////////////////////////////////////

/*


*/
std::tuple<torch::Tensor, torch::Tensor>
morphological_convolution_fw(
    const torch::Tensor& input, // [B,C,Ors,H,W]
    const torch::Tensor& kernel // [C,kOrs,kH,kW]
)
{
    const auto input_arg = torch::TensorArg(input, "input", 1);
    const auto kernel_arg = torch::TensorArg(kernel, "kernel", 2);

    torch::checkAllDefined(__func__, {input_arg, kernel_arg});
    torch::checkDim(__func__, input_arg, 5);
    torch::checkDim(__func__, kernel_arg, 4);

    if (input.is_cuda())
    {
#if defined(WITH_CUDA)
        torch::checkAllSameGPU(__func__, {input_arg, kernel_arg});
        return morphological_convolution_fw_cuda(
            input.contiguous(), kernel.contiguous());
#else
        TORCH_CHECK(false, "Not compiled with CUDA support");
#endif
    }
    else
    {
        return morphological_convolution_fw_cpu(
            input.contiguous(), kernel.contiguous());
    }
}

/*


*/
std::tuple<torch::Tensor, torch::Tensor>
morphological_convolution_bw(
    const torch::Tensor& grad,            // [B,C,Ors,W,H]
    const torch::Tensor& backindex,       // [B,C,Ors,H,W,3]
    const torch::IntArrayRef kernel_sizes // {C,kOrs,kW,kH}
)
{
    const auto grad_arg = torch::TensorArg(grad, "grad", 1);
    const auto backindex_arg = torch::TensorArg(backindex, "backindex", 2);

    torch::checkAllDefined(__func__, {grad_arg, backindex_arg});
    torch::checkAllContiguous(__func__, {grad_arg, backindex_arg});
    torch::checkDim(__func__, grad_arg, 5);
    torch::checkDim(__func__, backindex_arg, 6);

    if (grad.is_cuda())
    {
#if defined(WITH_CUDA)
        torch::checkAllSameGPU(__func__, {grad_arg, backindex_arg});
        return morphological_convolution_bw_cuda(
            grad.contiguous(), backindex, kernel_sizes);
#else
        TORCH_CHECK(false, "Not compiled with CUDA support");
#endif
    }
    else
    {
        return morphological_convolution_bw_cpu(
            grad.contiguous(), backindex, kernel_sizes);
    }
}

/*


*/
torch::Tensor
morphological_convolution(
    const torch::Tensor& input, // [B,C,Ors,H,W]
    const torch::Tensor& kernel // [C,kOrs,kH,kW]
)
{
    return MorphologicalConvolution::apply(input, kernel)[0];
}

/*


*/
tensor_list
MorphologicalConvolution::forward(
    AutogradContext* ctx, 
    torch::Tensor input, // [B,C,Ors,H,W]
    torch::Tensor kernel // [C,kOrs,kH,kW]
)
{
    const auto [out, back_index] =
        morphological_convolution_fw(input, kernel.to(input.scalar_type()));

    ctx->save_for_backward({back_index});
    ctx->saved_data["kernel_sizes"] = kernel.sizes();
    ctx->saved_data["kernel_scalar_type"] = kernel.scalar_type();

    return {out};
}

/*

*/
tensor_list
MorphologicalConvolution::backward(
    AutogradContext* ctx, 
    tensor_list grads
)
{
    const auto back_index = ctx->get_saved_variables()[0];
    const auto grad = grads[0];
    const auto kernel_sizes = ctx->saved_data["kernel_sizes"].toIntVector();
    const auto kernel_scalar_type =
        ctx->saved_data["kernel_scalar_type"].toScalarType();

    const auto [input_grad, kernel_grad] =
        morphological_convolution_bw(grad, back_index, kernel_sizes);

    return {input_grad, kernel_grad.to(kernel_scalar_type)};
}

/*

Logarithmic Distance Approximation

*/
torch::Tensor
logarithmic_metric_estimate_squared(
    const torch::Tensor& metric_params,
    const torch::IntArrayRef kernel_sizes,
    const double t_scale)
{
    using namespace torch::indexing;
    using co_t = float;

    LT_ARGUMENT_CHECK(
        kernel_sizes.size() == 3,
        __func__,
        "kernel_sizes should have length 3.");

    LT_ARGUMENT_CHECK(
        metric_params.is_floating_point(),
        __func__,
        "metric_params must be a floating point tensor.")

    LT_ARGUMENT_CHECK(
        t_scale >= 1.0, __func__, "t_scale must be larger than or equal to 1.")

    const auto opts = torch::TensorOptions()
                          .dtype(metric_params.dtype())
                          .device(metric_params.device())
                          .requires_grad(false);

    // Here we calculate the range of the domain of the kernel in the 
    // orientation and spatial dimensions.
    // The kernel is a function defined on the domain:
    // [-rw, rw] x [-rh, rh] x [-rors, rors].
    const co_t ors_step = static_cast<co_t>(2 * M_PI / t_scale);
    const co_t rors = static_cast<co_t>(kernel_sizes[0] - 1) * ors_step / 2.0f;
    const co_t rh = static_cast<co_t>(kernel_sizes[1] - 1) / 2.0f;
    const co_t rw = static_cast<co_t>(kernel_sizes[2] - 1) / 2.0f;

    // These are [C, 1, 1, 1] tensors, where 'C' is the amount of channels.
    const auto dMain =
        torch::relu(metric_params).index({Ellipsis, 0, None, None, None});
    const auto dLat =
        torch::relu(metric_params).index({Ellipsis, 1, None, None, None});
    const auto dAng =
        torch::relu(metric_params).index({Ellipsis, 2, None, None, None});

    // std::cout << "dMain" << std::endl;
    // std::cout << dMain << std::endl;

    // std::cout << "dLat" << std::endl;
    // std::cout << dLat << std::endl;

    // std::cout << "dAng" << std::endl;
    // std::cout << dAng << std::endl;

    // Here we discretize the domain of the kernel.
    // The size of the discretization, i.e. the size of ts, ys, and xs,
    // corresponds to the kernel sizes kt, ky, and kx.
    const auto ts = torch::range(-rors, rors, ors_step, opts);
    const auto ys = torch::range(-rh, rh, 1.0f, opts);
    const auto xs = torch::range(-rw, rw, 1.0f, opts);

    // tyx is an [3, kt, ky, kx] tensor.
    const auto tyx = torch::meshgrid({ts, ys, xs}, "ij");
    const auto t = tyx[0];
    const auto y = tyx[1];
    const auto x = tyx[2];

    const auto t2 = t.fmod(2 * M_PI) / 2;
    const auto tan = torch::tan(t2);
    const auto t0 = (tan.abs() < 1e-10);
    const auto cot = 1 / tan;

    // logarithmic coordinates ci
    // it is paramount that t is in [-pi, pi], and thus t2 in [-pi/2, pi/2], 
    // otherwise this formula is not correct!
    // ci_squared are [kt, ky, kx] tensors.
    const auto c1_squared = torch::where(t0, x, t2 * (y + x * cot)).pow(2);
    const auto c2_squared = torch::where(t0, y, t2 * (-x + y * cot)).pow(2);
    const auto c3_squared = t.pow(2);

    // rho_squared is an [C, kt, ky, kx] tensor.
    const auto rho_squared = dMain * c1_squared + dLat * c2_squared + 
        dAng * c3_squared;

    return rho_squared;
}

/*

Half Angle Distance Approximation rho_b_squared

*/
// torch::Tensor
// logarithmic_metric_estimate_squared(
//     const torch::Tensor& metric_params,
//     const torch::IntArrayRef kernel_sizes,
//     const double t_scale)
// {
//     using namespace torch::indexing;
//     using co_t = float;

//     LT_ARGUMENT_CHECK(
//         kernel_sizes.size() == 3,
//         __func__,
//         "kernel_sizes should have length 3.");

//     LT_ARGUMENT_CHECK(
//         metric_params.is_floating_point(),
//         __func__,
//         "metric_params must be a floating point tensor.")

//     LT_ARGUMENT_CHECK(
//         t_scale >= 1.0, __func__, "t_scale must be larger than or equal to 1.")

//     const auto opts = torch::TensorOptions()
//                           .dtype(metric_params.dtype())
//                           .device(metric_params.device())
//                           .requires_grad(false);

//     // Here we calculate the range of the domain of the kernel in the 
//     // orientation and spatial dimensions.
//     // The kernel is a function defined on the domain:
//     // [-rw, rw] x [-rh, rh] x [-rors, rors].
//     const co_t ors_step = static_cast<co_t>(2 * M_PI / t_scale);
//     const co_t rors = static_cast<co_t>(kernel_sizes[0] - 1) * ors_step / 2.0f;
//     const co_t rh = static_cast<co_t>(kernel_sizes[1] - 1) / 2.0f;
//     const co_t rw = static_cast<co_t>(kernel_sizes[2] - 1) / 2.0f;

//     // These are [C, 1, 1, 1] tensors, where 'C' is the amount of channels.
//     const auto dMain =
//         torch::relu(metric_params).index({Ellipsis, 0, None, None, None});
//     const auto dLat =
//         torch::relu(metric_params).index({Ellipsis, 1, None, None, None});
//     const auto dAng =
//         torch::relu(metric_params).index({Ellipsis, 2, None, None, None});

//     // std::cout << "dMain" << std::endl;
//     // std::cout << dMain << std::endl;

//     // std::cout << "dLat" << std::endl;
//     // std::cout << dLat << std::endl;

//     // std::cout << "dAng" << std::endl;
//     // std::cout << dAng << std::endl;

//     // Here we discretize the domain of the kernel.
//     // The size of the discretization, i.e. the size of ts, ys, and xs,
//     // corresponds to the kernel sizes kt, ky, and kx.
//     const auto ts = torch::range(-rors, rors, ors_step, opts);
//     const auto ys = torch::range(-rh, rh, 1.0f, opts);
//     const auto xs = torch::range(-rw, rw, 1.0f, opts);

//     // tyx is an [3, kt, ky, kx] tensor.
//     const auto tyx = torch::meshgrid({ts, ys, xs});
//     const auto t = tyx[0];
//     const auto y = tyx[1];
//     const auto x = tyx[2];

//     const auto t2 = t.fmod(2 * M_PI) / 2;

//     const auto c = torch::cos(t2);
//     const auto s = torch::sin(t2);

//     // ci_squared are [kt, ky, kx] tensors.
//     const auto b1 = x * c + y * s;
//     const auto b2 = - x * s + y * c;
//     const auto b3 = t;

//     // rho_squared is an [C, kt, ky, kx] tensor.
//     const auto rho_b_squared = (dMain * b1).pow(2) + (dLat * b2).pow(2) + 
//         (dAng * b3).pow(2);

//     return rho_b_squared;
// }

/*

Half Angle Sub-Riemannian Distance Approximation rho_b_sr_squared

*/
// torch::Tensor
// logarithmic_metric_estimate_squared(
//     const torch::Tensor& metric_params,
//     const torch::IntArrayRef kernel_sizes,
//     const double t_scale)
// {
//     using namespace torch::indexing;
//     using co_t = float;

//     LT_ARGUMENT_CHECK(
//         kernel_sizes.size() == 3,
//         __func__,
//         "kernel_sizes should have length 3.");

//     LT_ARGUMENT_CHECK(
//         metric_params.is_floating_point(),
//         __func__,
//         "metric_params must be a floating point tensor.")

//     LT_ARGUMENT_CHECK(
//         t_scale >= 1.0, __func__, "t_scale must be larger than or equal to 1.")

//     const auto opts = torch::TensorOptions()
//                           .dtype(metric_params.dtype())
//                           .device(metric_params.device())
//                           .requires_grad(false);

//     // Here we calculate the range of the domain of the kernel in the 
//     // orientation and spatial dimensions.
//     // The kernel is a function defined on the domain:
//     // [-rw, rw] x [-rh, rh] x [-rors, rors].
//     const co_t ors_step = static_cast<co_t>(2 * M_PI / t_scale);
//     const co_t rors = static_cast<co_t>(kernel_sizes[0] - 1) * ors_step / 2.0f;
//     const co_t rh = static_cast<co_t>(kernel_sizes[1] - 1) / 2.0f;
//     const co_t rw = static_cast<co_t>(kernel_sizes[2] - 1) / 2.0f;

//     // These are [C, 1, 1, 1] tensors, where 'C' is the amount of channels.
//     const auto dMain =
//         torch::relu(metric_params).index({Ellipsis, 0, None, None, None});
//     const auto dLat =
//         torch::relu(metric_params).index({Ellipsis, 1, None, None, None});
//     const auto dAng =
//         torch::relu(metric_params).index({Ellipsis, 2, None, None, None});

//     // std::cout << "dMain" << std::endl;
//     // std::cout << dMain << std::endl;

//     // std::cout << "dLat" << std::endl;
//     // std::cout << dLat << std::endl;

//     // std::cout << "dAng" << std::endl;
//     // std::cout << dAng << std::endl;

//     // Here we discretize the domain of the kernel.
//     // The size of the discretization, i.e. the size of ts, ys, and xs,
//     // corresponds to the kernel sizes kt, ky, and kx.
//     const auto ts = torch::range(-rors, rors, ors_step, opts);
//     const auto ys = torch::range(-rh, rh, 1.0f, opts);
//     const auto xs = torch::range(-rw, rw, 1.0f, opts);

//     // tyx is an [3, kt, ky, kx] tensor.
//     const auto tyx = torch::meshgrid({ts, ys, xs});
//     const auto t = tyx[0];
//     const auto y = tyx[1];
//     const auto x = tyx[2];

//     const auto t2 = t.fmod(2 * M_PI) / 2;

//     const auto c = torch::cos(t2);
//     const auto s = torch::sin(t2);

//     // bi are [kt, ky, kx] tensors.
//     const auto b1 = x * c + y * s;
//     const auto b2 = - x * s + y * c;
//     const auto b3 = t;

//     // rho_b_sr_squared is an [C, kt, ky, kx] tensor.
//     const float eps = std::numeric_limits<float>::epsilon();
//     const float nu = 1.6;
//     const auto rho_b_sr_squared = torch::where(
//         dMain <= dLat, 
//         torch::sqrt( (nu*(dMain + dAng)).pow(4)*b2.pow(2) + ((dMain*b1).pow(2) + (dAng*b3).pow(2)).pow(2) + eps), 
//         torch::sqrt( (nu*(dLat + dAng)).pow(4)*b1.pow(2) + ((dLat*b2).pow(2) + (dAng*b3).pow(2)).pow(2) + eps)
//     );

//     return rho_b_sr_squared;
// }

/*

Half Angle Combined Distance Approximation rho_b_com_squared

*/
// torch::Tensor
// logarithmic_metric_estimate_squared(
//     const torch::Tensor& metric_params,
//     const torch::IntArrayRef kernel_sizes,
//     const double t_scale)
// {
//     using namespace torch::indexing;
//     using co_t = float;

//     LT_ARGUMENT_CHECK(
//         kernel_sizes.size() == 3,
//         __func__,
//         "kernel_sizes should have length 3.");

//     LT_ARGUMENT_CHECK(
//         metric_params.is_floating_point(),
//         __func__,
//         "metric_params must be a floating point tensor.")

//     LT_ARGUMENT_CHECK(
//         t_scale >= 1.0, __func__, "t_scale must be larger than or equal to 1.")

//     const auto opts = torch::TensorOptions()
//                           .dtype(metric_params.dtype())
//                           .device(metric_params.device())
//                           .requires_grad(false);

//     // Here we calculate the range of the domain of the kernel in the 
//     // orientation and spatial dimensions.
//     // The kernel is a function defined on the domain:
//     // [-rw, rw] x [-rh, rh] x [-rors, rors].
//     const co_t ors_step = static_cast<co_t>(2 * M_PI / t_scale);
//     const co_t rors = static_cast<co_t>(kernel_sizes[0] - 1) * ors_step / 2.0f;
//     const co_t rh = static_cast<co_t>(kernel_sizes[1] - 1) / 2.0f;
//     const co_t rw = static_cast<co_t>(kernel_sizes[2] - 1) / 2.0f;

//     // These are [C, 1, 1, 1] tensors, where 'C' is the amount of channels.
//     const auto dMain =
//         torch::relu(metric_params).index({Ellipsis, 0, None, None, None});
//     const auto dLat =
//         torch::relu(metric_params).index({Ellipsis, 1, None, None, None});
//     const auto dAng =
//         torch::relu(metric_params).index({Ellipsis, 2, None, None, None});

//     // std::cout << "dMain" << std::endl;
//     // std::cout << dMain << std::endl;

//     // std::cout << "dLat" << std::endl;
//     // std::cout << dLat << std::endl;

//     // std::cout << "dAng" << std::endl;
//     // std::cout << dAng << std::endl;

//     // Here we discretize the domain of the kernel.
//     // The size of the discretization, i.e. the size of ts, ys, and xs,
//     // corresponds to the kernel sizes kt, ky, and kx.
//     const auto ts = torch::range(-rors, rors, ors_step, opts);
//     const auto ys = torch::range(-rh, rh, 1.0f, opts);
//     const auto xs = torch::range(-rw, rw, 1.0f, opts);

//     // tyx is an [3, kt, ky, kx] tensor.
//     const auto tyx = torch::meshgrid({ts, ys, xs});
//     const auto t = tyx[0];
//     const auto y = tyx[1];
//     const auto x = tyx[2];

//     const auto t2 = t.fmod(2 * M_PI) / 2;

//     const auto c = torch::cos(t2);
//     const auto s = torch::sin(t2);

//     // bi are [kt, ky, kx] tensors.
//     const auto b1 = x * c + y * s;
//     const auto b2 = - x * s + y * c;
//     const auto b3 = t;

//     // rho_b_sr_squared is an [C, kt, ky, kx] tensor.
//     const float eps = std::numeric_limits<float>::epsilon();
//     const float nu = 1.6;
//     const auto rho_b_sr_squared = torch::where(
//         dMain <= dLat, 
//         torch::sqrt( (nu*(dMain + dAng)).pow(4)*b2.pow(2) + ((dMain*b1).pow(2) + (dAng*b3).pow(2)).pow(2) + eps), 
//         torch::sqrt( (nu*(dLat + dAng)).pow(4)*b1.pow(2) + ((dLat*b2).pow(2) + (dAng*b3).pow(2)).pow(2) + eps)
//     );

//     const auto rho_b_squared = (dMain * b1).pow(2) + (dLat * b2).pow(2) + 
//         (dAng * b3).pow(2);

//     const auto dMin = torch::min(dMain, dLat);

//     const auto l_squared = (dMin * b1).pow(2) + (dMin * b2).pow(2) + 
//         (dAng * b3).pow(2);

//     const auto rho_b_com_squared = torch::max(l_squared, torch::min(rho_b_squared, rho_b_sr_squared));

//     return rho_b_com_squared;
// }

torch::Tensor
logarithmic_metric_estimate_squared_nondiag(
    const torch::Tensor& metric_params,
    const torch::IntArrayRef kernel_sizes,
    const double t_scale)
{
    using namespace torch::indexing;
    using co_t = float;

    LT_ARGUMENT_CHECK(
        kernel_sizes.size() == 3,
        __func__,
        "kernel_sizes should have length 3.");

    LT_ARGUMENT_CHECK(
        metric_params.is_floating_point(),
        __func__,
        "metric_params must be a floating point tensor.")

    LT_ARGUMENT_CHECK(
        t_scale >= 1.0, __func__, "t_scale must be larger than or equal to 1.")

    const auto opts = torch::TensorOptions()
                          .dtype(metric_params.dtype())
                          .device(metric_params.device())
                          .requires_grad(false);

    const co_t ors_step = static_cast<co_t>(2 * M_PI / t_scale);
    const co_t rors = static_cast<co_t>(kernel_sizes[0] - 1) * ors_step / 2.0f;
    const co_t rh = static_cast<co_t>(kernel_sizes[1] - 1) / 2.0f;
    const co_t rw = static_cast<co_t>(kernel_sizes[2] - 1) / 2.0f;

    const auto ts = torch::range(-rors, rors, ors_step, opts);
    const auto ys = torch::range(-rh, rh, 1.0f, opts);
    const auto xs = torch::range(-rw, rw, 1.0f, opts);

    // [3, kt, ky, kx]
    const auto tyx = torch::meshgrid({ts, ys, xs}, "ij");

    // [kt, ky, kx]
    const auto t = tyx[0];
    const auto y = tyx[1];
    const auto x = tyx[2];

    // [kt, ky, kx]
    const auto t2 = t.fmod(2 * M_PI) / 2;
    const auto tan = torch::tan(t2);
    const auto t0 = (tan.abs() < 1e-10);
    const auto cot = 1 / tan;

    // [kt, ky, kx]
    const auto c1 = torch::where(t0, x, t2 * (y + x * cot));
    const auto c2 = torch::where(t0, y, t2 * (-x + y * cot));
    const auto c3 = t;

    // [C, 3, 3]
    const auto m = metric_params;

    // [C, 1, 1, 1]
    const auto m11 = m.index({Ellipsis, 0, 0, None, None, None});
    const auto m12 = m.index({Ellipsis, 0, 1, None, None, None});
    const auto m13 = m.index({Ellipsis, 0, 2, None, None, None});
    const auto m21 = m.index({Ellipsis, 1, 0, None, None, None});
    const auto m22 = m.index({Ellipsis, 1, 1, None, None, None});
    const auto m23 = m.index({Ellipsis, 1, 2, None, None, None});
    const auto m31 = m.index({Ellipsis, 2, 0, None, None, None});
    const auto m32 = m.index({Ellipsis, 2, 1, None, None, None});
    const auto m33 = m.index({Ellipsis, 2, 2, None, None, None});

    // std::cout << "m11" << std::endl;
    // std::cout << m11 << std::endl;
    // std::cout << "m22" << std::endl;
    // std::cout << m22 << std::endl;
    // std::cout << "m33" << std::endl;
    // std::cout << m33 << std::endl;

    // [C, kt, ky, kx]
    const auto f1 = m11 * c1 + m12 * c2 + m13 * c3;
    const auto f2 = m21 * c1 + m22 * c2 + m23 * c3;
    const auto f3 = m31 * c1 + m32 * c2 + m33 * c3;

    // [C, kt, ky, kx]
    const auto rho_squared = f1*f1 + f2*f2 + f3*f3;

    // I would rather collect c1, c2, and c3 in one vector c 
    // of size [3, kt, ky, kx]
    // and then matrix-vector multiply m and c such that
    // [C, 3, 3] * [3, kt, ky, kx] = [C, 3, kt, ky, kx]
    // and then take the dot product with itself to get a
    // [C, kt, ky, kx]

    return rho_squared;
}

/*


*/
torch::Tensor
logarithmic_metric_estimate(
    const torch::Tensor& metric_params,
    const torch::IntArrayRef kernel_sizes,
    const double t_scale)
{
    const auto rho2 = logarithmic_metric_estimate_squared(
        metric_params, kernel_sizes, t_scale);
    const float eps = std::numeric_limits<float>::epsilon();

    return torch::sqrt(rho2 + eps);
}

/*


*/
torch::Tensor
logarithmic_metric_estimate_nondiag(
    const torch::Tensor& metric_params,
    const torch::IntArrayRef kernel_sizes,
    const double t_scale)
{
    const auto rho2 = logarithmic_metric_estimate_squared_nondiag(
        metric_params, kernel_sizes, t_scale);
    const float eps = std::numeric_limits<float>::epsilon();

    return torch::sqrt(rho2 + eps);
}

/*


 */
torch::Tensor
morphological_kernel(
    const torch::Tensor& metric_params,
    const torch::IntArrayRef kernel_sizes,
    const double t_scale,
    const double alpha)
{
    LT_ARGUMENT_CHECK(
        alpha >= 0.55 && alpha <= 1.0,
        __func__,
        "alpha needs to be >= 0.55 and <= 1.0");

    const auto rho2 = logarithmic_metric_estimate_squared(
        metric_params, kernel_sizes, t_scale);

    const double c =
        (2 * alpha - 1) / std::pow(2 * alpha, (2 * alpha) / (2 * alpha - 1));
    const double p = alpha / (2 * alpha - 1);
    return c * rho2.pow(p);
}

/*


 */
torch::Tensor
morphological_kernel_nondiag(
    const torch::Tensor& metric_params,
    const torch::IntArrayRef kernel_sizes,
    const double t_scale,
    const double alpha)
{
    LT_ARGUMENT_CHECK(
        alpha >= 0.55 && alpha <= 1.0,
        __func__,
        "alpha needs to be >= 0.55 and <= 1.0");

    const auto rho2 = logarithmic_metric_estimate_squared_nondiag(
        metric_params, kernel_sizes, t_scale);

    const double c =
        (2 * alpha - 1) / std::pow(2 * alpha, (2 * alpha) / (2 * alpha - 1));
    const double p = alpha / (2 * alpha - 1);
    return c * rho2.pow(p);
}

/*


 */
torch::Tensor
diffusion_kernel(
    const torch::Tensor& metric_params,
    const torch::IntArrayRef kernel_sizes,
    const double t_scale)
{
    const auto rho2 = logarithmic_metric_estimate_squared(
        metric_params, kernel_sizes, t_scale);

    const auto m = torch::exp(-rho2);

    return m / torch::sum(m, {-3, -2, -1}, true);
}

/*

/*


 */
torch::Tensor
diffusion_kernel_nondiag(
    const torch::Tensor& metric_params,
    const torch::IntArrayRef kernel_sizes,
    const double t_scale)
{
    const auto rho2 = logarithmic_metric_estimate_squared_nondiag(
        metric_params, kernel_sizes, t_scale);

    const auto m = torch::exp(-rho2);

    return m / torch::sum(m, {-3, -2, -1}, true);
}

/*

*/
torch::Tensor
fractional_dilation(
    const torch::Tensor& input,
    const torch::Tensor& metric_params,
    const torch::IntArrayRef kernel_sizes,
    const double alpha)
{
    const double t_scale = static_cast<double>(input.size(2));
    const auto k =
        morphological_kernel(metric_params, kernel_sizes, t_scale, alpha);
    return -morphological_convolution(-input, k);
}

/*


*/
torch::Tensor
fractional_dilation_nondiag(
    const torch::Tensor& input,
    const torch::Tensor& metric_params,
    const torch::IntArrayRef kernel_sizes,
    const double alpha)
{
    const double t_scale = static_cast<double>(input.size(2));
    const auto k =
        morphological_kernel_nondiag(metric_params, kernel_sizes, t_scale, alpha);
    return -morphological_convolution(-input, k);
}

/*


*/
torch::Tensor
fractional_erosion(
    const torch::Tensor& input,
    const torch::Tensor& metric_params,
    const torch::IntArrayRef kernel_sizes,
    const double alpha)
{
    const double t_scale = static_cast<double>(input.size(2));
    const auto k =
        morphological_kernel(metric_params, kernel_sizes, t_scale, alpha);
    return morphological_convolution(input, k);
}


/*


*/
torch::Tensor
fractional_erosion_nondiag(
    const torch::Tensor& input,
    const torch::Tensor& metric_params,
    const torch::IntArrayRef kernel_sizes,
    const double alpha)
{
    const double t_scale = static_cast<double>(input.size(2));
    const auto k =
        morphological_kernel_nondiag(metric_params, kernel_sizes, t_scale, alpha);
    return morphological_convolution(input, k);
}

/*

 */
torch::Tensor
linear_fw(
    const torch::Tensor& input, // [B, Cin, Ors, H, W]
    const torch::Tensor& weight // [Cin, Cout]
)
{
    const auto input_arg = torch::TensorArg(input, "input", 1);
    const auto weight_arg = torch::TensorArg(weight, "weight", 2);

    torch::checkAllDefined(__func__, {input_arg, weight_arg});
    torch::checkDim(__func__, input_arg, 5);
    torch::checkDim(__func__, weight_arg, 2);
    torch::checkSameType(__func__, input_arg, weight_arg);
    TORCH_CHECK(
        input.size(1) == weight.size(0),
        "input.size(1) must equal weight.size(0)");

    if (input.is_cuda())
    {
#if defined(WITH_CUDA)
        torch::checkAllSameGPU(__func__, {input_arg, weight_arg});
        return linear_fw_cuda(input, weight);
#else
        TORCH_CHECK(false, "Not compiled with CUDA support");
#endif
    }
    else
    {
        return linear_fw_cpu(input, weight);
    }
}

/*

 */
std::tuple<torch::Tensor, torch::Tensor>
linear_bw(
    const torch::Tensor& grad,  // [B, Cout, Ors, H, W]
    const torch::Tensor& input, // [B, Cin, Ors, H, W]
    const torch::Tensor& weight // [Cin, Cout]
)
{
    const auto grad_arg = torch::TensorArg(grad, "grad", 1);
    const auto weight_arg = torch::TensorArg(weight, "weight", 2);

    torch::checkAllDefined(__func__, {grad_arg, weight_arg});
    torch::checkDim(__func__, grad_arg, 5);
    torch::checkDim(__func__, weight_arg, 2);
    torch::checkSameType(__func__, grad_arg, weight_arg);
    TORCH_CHECK(
        grad.size(1) == weight.size(1),
        "grad.size(1) must equal weight.size(1)");

    if (grad.is_cuda())
    {
#if defined(WITH_CUDA)
        torch::checkAllSameGPU(__func__, {grad_arg, weight_arg});
        return linear_bw_cuda(grad, input, weight);
#else
        TORCH_CHECK(false, "Not compiled with CUDA support");
#endif
    }
    else
    {
        return linear_bw_cpu(grad, input, weight);
    }
}

/*

*/
torch::Tensor
linear(
    const torch::Tensor& input, // [B, Cin, Ors, H, W]
    const torch::Tensor& weight // [Cin, Cout]
)
{
    return Linear::apply(input, weight)[0];
}

/*

*/
tensor_list
Linear::forward(
    AutogradContext* ctx, 
    torch::Tensor input, // [B, Cin, Ors, H, W]
    torch::Tensor weight // [Cin, Cout]
)
{
    const auto out = linear_fw(input, weight);
    ctx->save_for_backward({input.detach(), weight.detach()});

    return {out};
}

/*

*/
tensor_list
Linear::backward(
    AutogradContext* ctx, 
    tensor_list grads
)
{
    const auto input = ctx->get_saved_variables()[0];
    const auto weight = ctx->get_saved_variables()[1];
    const auto grad = grads[0];

    const auto [input_grad, weight_grad] = linear_bw(grad, input, weight);

    return {input_grad, weight_grad};
}

} // namespace m2
} // namespace lietorch
