/*

    Include torch without setting off a deluge of warning on windows.

*/
#pragma once
#ifndef LIETORCH_TORCH_INCLUDE_H
#define LIETORCH_TORCH_INCLUDE_H

#pragma warning(push, 1)
#pragma warning(disable : 4251)
#pragma warning(disable : 4624)

#include <torch/autograd.h>
#include <torch/script.h>

using torch::Tensor;

#pragma warning(pop)

#endif