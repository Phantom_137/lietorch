# Building the C++/CUDA extension

The `lietorch` python package depends on a native C++/CUDA backend, this backend is included as a shared library in `<lietorch package>/lib/lietorch.dll` (on Windows) or `<lietorch package>/lib/liblietorch.so` (on Linux). This README contains instructions on how to build the shared library from source.


## Windows


### Setup

* Windows 10 (11 is untested)
* A conda environment that includes a CUDA enable version of PyTorch (see [here](https://pytorch.org/get-started/locally/)).
* CMake 3.18+
* MSVC 16.10.4 (other versions are untested)
    download [here](https://download.visualstudio.microsoft.com/download/pr/acfc792d-506b-4868-9924-aeedc61ae654/72ae7ec0c234bbe0e655dc4776110c23178c8fbb7bbcf9b5b96a683b95e8d755/vs_BuildTools.exe),
    other versions [here](https://learn.microsoft.com/en-us/visualstudio/releases/2019/history), you only need the *BuildTools*.
* CUDA Toolkit (the same major version as PyTorch was compiled with is required)
    * The `nvcc.exe` compiler has to be included in `PATH` for CMake to find the CUDA toolkit.
* Ninja build tools (can be installed in the conda environment with `conda install -c conda-forge ninja`)
* Math Kernel Library (MKL), see [here](https://www.intel.com/content/www/us/en/developer/tools/oneapi/onemkl-download.html?operatingsystem=window&distributions=webdownload&options=online)
* CuDNN, see [here](https://developer.nvidia.com/cudnn) and read the [installation guide](https://docs.nvidia.com/deeplearning/cudnn/install-guide/index.html) (pick a version compatible with the CUDA toolkit you installed)


### Compiling

1) start *x64 Native Tools Command Prompt for VS 2019* from the start menu
2) run `powershell`
3) activate the correct conda environment
4) navigate to the `backend` directory
5) run `.\ltbuild.ps1`

If the build was succesfull the output `lietorch.dll` will be placed in the `<lietorch package>\lib` directory in the python package. 
You can run `.\build\lttest.exe` to run unit tests.

#### Note on build type
On Windows shared libraries (DLLs) have a different ABI depending on whether they are built with our without debugging support. Currently PyTorch can only interface with the `Release` build (i.e. without debugging), therefore the build script selects the `Release` build type by default. If debugging is needed on Windows you can select the `RelWithDebInfo` build type, this uses the `Release` ABI but generates debugging symbols, note that the `Release` build type still has a higher level of optimization enabled.

If you want full debugging support you will need to link against the debug version of LibTorch, which can be downloaded separately.

## Linux

### Setup

* CMake
* GCC (recent version and discoverable on the path)
* A conda environment that includes a CUDA enable version of PyTorch
* CUDA Toolkit (the same major version as PyTorch was compiled with) that is symlinked to from `/usr/local/cuda`
* CuDNN, see [here](https://developer.nvidia.com/cudnn) and read the [installation guide](https://docs.nvidia.com/deeplearning/cudnn/install-guide/index.html) (pick a version compatible with the CUDA toolkit you installed)
* Ninja build tools (can be installed in the conda environment with `conda install -c conda-forge ninja` or via your distribution's package manager)


### Compiling

1) activate the correct conda environment
2) navigate to the `backend` directory
3) run `bash ./ltbuild.sh`

You might have to change `python` to `python3` in `ltbuild.sh`.

If the build was successfull the output `liblietorch.so` will be placed in the `<lietorch package>/lib` directory in the python package. You can run `./build/lttest` to run unit tests.