

#include "temperate_cpu.h"

#include <cmath>

namespace lietorch
{
namespace temperate
{

uint8_t
mul_lut_u8(uint8_t x, uint8_t y)
{
    static const uint8_t* LUT = [&]
    {
        auto lut = new uint8_t[256 * 256];

        for (int i = 0; i < 256; i++)
        {
            for (int j = 0; j < 256; j++)
            {
                const float a = static_cast<float>(i) / 256.0f;
                const float b = static_cast<float>(j) / 256.0f;
                const float ab = a * b;
                const float c = ab / (2.0f * ab - a - b + 1.0f);
                const long l = std::lround(256.0f * c);
                lut[256 * i + j] = static_cast<uint8_t>(std::min(l, 255L));
            }
        }
        return lut;
    }();

    return LUT[(x << 8) | y];
}

Tensor
mul_cpu(const Tensor& a, const Tensor& b)
{
    return a + b;
}

Tensor
plus_cpu(const Tensor& a, const Tensor& b)
{
    return torch::max(a, b);
}

} // namespace temperate
} // namespace lietorch