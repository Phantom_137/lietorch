/**
 *
 *
 *
 */

#include "r2_cpu.h"
#include "../dbg.h"
#include "../parallel.h"

namespace lietorch
{
namespace r2
{

/*

*/
template <typename scalar_t>
void
morphological_convolution_fw_cpu_impl(
    const torch::TensorAccessor<scalar_t, 4> input_a,
    const torch::TensorAccessor<scalar_t, 3> kernel_a,
    torch::TensorAccessor<scalar_t, 4> out_a,
    torch::TensorAccessor<int64_t, 5> backindex_a)
{
    const int64_t h = input_a.size(2); // number of y's
    const int64_t w = input_a.size(3); // number of x's

    const int64_t ker_h = kernel_a.size(1);
    const int64_t ker_w = kernel_a.size(2);

    const int64_t r_h = (ker_h - 1) / 2;
    const int64_t r_w = (ker_w - 1) / 2;

    auto mconv_fw_one_instance = [&](const int64_t& b, const int64_t& c) {
        for (int64_t j = 0; j < h; j++)
        {
            const auto jker_from = std::max<int64_t>(0, r_h - j);
            const auto jker_to = std::min<int64_t>(ker_h, h + r_h - j);

            for (int64_t k = 0; k < w; k++)
            {
                const auto kker_from = std::max<int64_t>(0, r_w - k);
                const auto kker_to = std::min<int64_t>(ker_w, w + r_w - k);

                scalar_t val = std::numeric_limits<scalar_t>::max();
                int64_t hit_j, hit_k;

                for (int64_t jker = jker_from; jker < jker_to; jker++)
                {
                    for (int64_t kker = kker_from; kker < kker_to; kker++)
                    {

                        const auto in =
                            input_a[b][c][j - r_h + jker][k - r_w + kker];

                        const auto newval = in + kernel_a[c][jker][kker];
                        if (newval < val)
                        {
                            val = newval;

                            hit_j = j - r_h + jker;
                            hit_k = k - r_w + kker;
                        }
                    }
                }

                out_a[b][c][j][k] = val;
                backindex_a[b][c][j][k][0] = hit_j;
                backindex_a[b][c][j][k][1] = hit_k;
            }
        }
    };

    // Parallelize over batches and channels.
    std::vector<int64_t> batch_range(input_a.size(0));
    std::iota(batch_range.begin(), batch_range.end(), 0);

    std::vector<int64_t> channel_range(input_a.size(1));
    std::iota(channel_range.begin(), channel_range.end(), 0);

    lietorch::for_each(
        batch_range.begin(), batch_range.end(), [&](const int64_t& b) {
            lietorch::for_each(
                channel_range.begin(),
                channel_range.end(),
                [&](const int64_t& c) { mconv_fw_one_instance(b, c); });
        });
}

/*

*/
template <typename scalar_t>
void
morphological_convolution_fw_cpu_impl_nograd(
    const torch::TensorAccessor<scalar_t, 4> input_a,
    const torch::TensorAccessor<scalar_t, 3> kernel_a,
    torch::TensorAccessor<scalar_t, 4> out_a)
{
    const int64_t h = input_a.size(2); // number of y's
    const int64_t w = input_a.size(3); // number of x's

    const int64_t ker_h = kernel_a.size(1);
    const int64_t ker_w = kernel_a.size(2);

    const int64_t r_h = (ker_h - 1) / 2;
    const int64_t r_w = (ker_w - 1) / 2;

    auto mconv_fw_one_instance = [&](const int64_t& b, const int64_t& c) {
        for (int64_t j = 0; j < h; j++)
        {
            const auto jker_from = std::max<int64_t>(0, r_h - j);
            const auto jker_to = std::min<int64_t>(ker_h, h + r_h - j);

            for (int64_t k = 0; k < w; k++)
            {
                const auto kker_from = std::max<int64_t>(0, r_w - k);
                const auto kker_to = std::min<int64_t>(ker_w, w + r_w - k);

                scalar_t val = std::numeric_limits<scalar_t>::max();

                for (int64_t jker = jker_from; jker < jker_to; jker++)
                {
                    for (int64_t kker = kker_from; kker < kker_to; kker++)
                    {

                        const auto in =
                            input_a[b][c][j - r_h + jker][k - r_w + kker];

                        const auto newval = in + kernel_a[c][jker][kker];
                        if (newval < val)
                        {
                            val = newval;
                        }
                    }
                }

                out_a[b][c][j][k] = val;
            }
        }
    };

    // Parallelize over batches and channels.
    std::vector<int64_t> batch_range(input_a.size(0));
    std::iota(batch_range.begin(), batch_range.end(), 0);

    std::vector<int64_t> channel_range(input_a.size(1));
    std::iota(channel_range.begin(), channel_range.end(), 0);

    lietorch::for_each(
        batch_range.begin(), batch_range.end(), [&](const int64_t& b) {
            lietorch::for_each(
                channel_range.begin(),
                channel_range.end(),
                [&](const int64_t& c) { mconv_fw_one_instance(b, c); });
        });
}

/*

*/
std::tuple<torch::Tensor, torch::Tensor>
morphological_convolution_fw_cpu(
    const torch::Tensor& input, // [B,C,H,W]
    const torch::Tensor& kernel // [C,kH,kW]
)
{
    const auto out = torch::zeros_like(input); // [B,C,Or,H,W]

    if (input.requires_grad() || kernel.requires_grad())
    {
        std::vector<int64_t> backindex_size =
            input.sizes().vec();     // = {B,C,H,W}
        backindex_size.push_back(2); // = {B,C,H,W,2}
        const auto backindex = torch::zeros(
            backindex_size,
            torch::TensorOptions().dtype(torch::kInt64).device(input.device()));

        AT_DISPATCH_FLOATING_TYPES(input.scalar_type(), __func__, [&] {
            const auto input_a = input.accessor<scalar_t, 4>();
            const auto kernel_a = kernel.accessor<scalar_t, 3>();
            auto out_a = out.accessor<scalar_t, 4>();
            auto backindex_a = backindex.accessor<int64_t, 5>();

            morphological_convolution_fw_cpu_impl<scalar_t>(
                input_a, kernel_a, out_a, backindex_a);
        });

        return std::make_tuple(out, backindex);
    }
    else
    {
        AT_DISPATCH_FLOATING_TYPES(input.scalar_type(), __func__, [&] {
            const auto input_a = input.accessor<scalar_t, 4>();
            const auto kernel_a = kernel.accessor<scalar_t, 3>();
            auto out_a = out.accessor<scalar_t, 4>();

            morphological_convolution_fw_cpu_impl_nograd<scalar_t>(
                input_a, kernel_a, out_a);
        });

        return std::make_tuple(out, torch::Tensor());
    }
} // namespace r2

/*

*/
template <typename scalar_t>
void
morphological_convolution_bw_cpu_impl(
    const torch::TensorAccessor<scalar_t, 4> grad_a,
    const torch::TensorAccessor<int64_t, 5> backindex_a,
    torch::TensorAccessor<scalar_t, 4> input_grad_a,
    torch::TensorAccessor<scalar_t, 4> kernel_grad_a)
{
    const int64_t h = grad_a.size(2); // number of y's
    const int64_t w = grad_a.size(3); // number of x's

    const int64_t kh = kernel_grad_a.size(2);
    const int64_t kw = kernel_grad_a.size(3);

    const int64_t stack_h = kh % 2 == 0 ? kh + 1 : kh;
    const int64_t stack_w = kw % 2 == 0 ? kw + 1 : kw;

    const int64_t r_h = (stack_h - 1) / 2;
    const int64_t r_w = (stack_w - 1) / 2;

    auto mconv_bw_one_instance = [&](const int64_t& b, const int64_t& c) {
        for (int64_t j = 0; j < h; j++)
        {
            for (int64_t k = 0; k < w; k++)
            {
                const auto hit_j = backindex_a[b][c][j][k][0];
                const auto hit_k = backindex_a[b][c][j][k][1];
                const auto gradval = grad_a[b][c][j][k];

                input_grad_a[b][c][hit_j][hit_k] += gradval;

                const auto ker_j = hit_j - j + r_h;
                const auto ker_k = hit_k - k + r_w;

                if (ker_j >= 0 && ker_j < kh && ker_k >= 0 && ker_k < kw)
                {

                    kernel_grad_a[b][c][ker_j][ker_k] += gradval;
                }
            }
        }
    };

    // Parallelize over batches and channels.
    std::vector<int64_t> batch_range(grad_a.size(0));
    std::iota(batch_range.begin(), batch_range.end(), 0);

    std::vector<int64_t> channel_range(grad_a.size(1));
    std::iota(channel_range.begin(), channel_range.end(), 0);

    lietorch::for_each(
        batch_range.begin(), batch_range.end(), [&](const int64_t& b) {
            lietorch::for_each(
                channel_range.begin(),
                channel_range.end(),
                [&](const int64_t& c) { mconv_bw_one_instance(b, c); });
        });
}

/*

 */
std::tuple<torch::Tensor, torch::Tensor>
morphological_convolution_bw_cpu(
    const torch::Tensor& grad,
    const torch::Tensor& backindex,
    const torch::IntArrayRef kernel_sizes)
{
    const auto input_grad = torch::zeros_like(grad);
    const auto kernel_grad = torch::zeros(
        {grad.size(0), kernel_sizes[0], kernel_sizes[1], kernel_sizes[2]},
        grad.options());
    const auto backindex2 = backindex.to(torch::kInt64);

    AT_DISPATCH_FLOATING_TYPES(grad.scalar_type(), __func__, [&] {
        const auto grad_a = grad.accessor<scalar_t, 4>();
        const auto backindex_a = backindex2.accessor<int64_t, 5>();
        auto input_grad_a = input_grad.accessor<scalar_t, 4>();
        auto kernel_grad_a = kernel_grad.accessor<scalar_t, 4>();

        morphological_convolution_bw_cpu_impl<scalar_t>(
            grad_a, backindex_a, input_grad_a, kernel_grad_a);
    });

    return std::make_tuple(input_grad, kernel_grad.sum({0}));
}

/*

*/
template <typename scalar_t, typename coordinate_t = float>
void
convection_fw_cpu_impl(
    const torch::TensorAccessor<scalar_t, 4> input_a,    // [B,C,H,W]
    const torch::TensorAccessor<coordinate_t, 2> g0_a,   // [C,2]
    torch::TensorAccessor<scalar_t, 4> out_a,            // [B,C,H,W]
    torch::TensorAccessor<scalar_t, 5> out_grad_field_a) // [B,C,H,W,3]
{
    const int64_t h = input_a.size(2); // number of y's
    const int64_t w = input_a.size(3); // number of x's

    // Lambda for doing convection for a single channel and single
    // batch-element, i.e. 'instance'.
    auto convection_fw_one_instance = [&](const int64_t& b, const int64_t& c) {
        const auto input = input_a[b][c];
        const auto g0 = g0_a[c];
        auto out = out_a[b][c];
        auto out_grad_field = out_grad_field_a[b][c];

        const coordinate_t y_floor = std::floor(g0[0]);
        const coordinate_t x_floor = std::floor(g0[1]);
        const scalar_t y_rem = static_cast<scalar_t>(g0[0] - y_floor);
        const scalar_t x_rem = static_cast<scalar_t>(g0[1] - x_floor);
        const int64_t j_offset = static_cast<int64_t>(y_floor);
        const int64_t k_offset = static_cast<int64_t>(x_floor);

        const scalar_t w11 = (1 - y_rem) * (1 - x_rem);
        const scalar_t w10 = (1 - y_rem) * x_rem;
        const scalar_t w01 = y_rem * (1 - x_rem);
        const scalar_t w00 = y_rem * x_rem;

        const int64_t j0 = std::max(j_offset, (int64_t)0);
        const int64_t j1 = std::min(h, h + j_offset + 1);
        const int64_t k0 = std::max(k_offset, (int64_t)0);
        const int64_t k1 = std::min(w, w + k_offset + 1);

        for (int64_t j = j0; j < j1; j++)
        {
            for (int64_t k = k0; k < k1; k++)
            {
                scalar_t v00 = 0.0;
                scalar_t v01 = 0.0;
                scalar_t v10 = 0.0;
                scalar_t v11 = 0.0;

                if (j - j_offset - 1 >= 0 && k - k_offset - 1 >= 0)
                {
                    v00 = input[j - j_offset - 1][k - k_offset - 1];
                }

                if (j - j_offset - 1 >= 0 && k - k_offset < w)
                {
                    v01 = input[j - j_offset - 1][k - k_offset];
                }

                if (j - j_offset < h && k - k_offset - 1 >= 0)
                {
                    v10 = input[j - j_offset][k - k_offset - 1];
                }

                if (j - j_offset < h && k - k_offset < w)
                {
                    v11 = input[j - j_offset][k - k_offset];
                }

                out[j][k] = w00 * v00 + w01 * v01 + w10 * v10 + w11 * v11;
                const scalar_t dy =
                    x_rem * (v00 - v10) + (1 - x_rem) * (v01 - v11);
                const scalar_t dx =
                    y_rem * (v00 - v01) + (1 - y_rem) * (v10 - v11);
                out_grad_field[j][k][0] = dy;
                out_grad_field[j][k][1] = dx;
            }
        }
    };

    // Parallelize over batches and channels.
    std::vector<int64_t> batch_range(input_a.size(0));
    std::iota(batch_range.begin(), batch_range.end(), 0);

    std::vector<int64_t> channel_range(input_a.size(1));
    std::iota(channel_range.begin(), channel_range.end(), 0);

    lietorch::for_each(
        batch_range.begin(), batch_range.end(), [&](const int64_t& b) {
            lietorch::for_each(
                channel_range.begin(),
                channel_range.end(),
                [&](const int64_t& c) { convection_fw_one_instance(b, c); });
        });
}

/*

*/
template <typename scalar_t, typename coordinate_t = float>
void
convection_fw_cpu_impl_nograd(
    const torch::TensorAccessor<scalar_t, 4> input_a,  // [B,C,H,W]
    const torch::TensorAccessor<coordinate_t, 2> g0_a, // [C,2]
    torch::TensorAccessor<scalar_t, 4> out_a)          // [B,C,H,W]
{
    const int64_t h = input_a.size(2); // number of y's
    const int64_t w = input_a.size(3); // number of x's

    // Lambda for doing convection for a single channel and single
    // batch-element, i.e. 'instance'.
    auto convection_fw_one_instance = [&](const int64_t& b, const int64_t& c) {
        const auto input = input_a[b][c];
        const auto g0 = g0_a[c];
        auto out = out_a[b][c];

        const coordinate_t y_floor = std::floor(g0[0]);
        const coordinate_t x_floor = std::floor(g0[1]);
        const scalar_t y_rem = static_cast<scalar_t>(g0[0] - y_floor);
        const scalar_t x_rem = static_cast<scalar_t>(g0[1] - x_floor);
        const int64_t j_offset = static_cast<int64_t>(y_floor);
        const int64_t k_offset = static_cast<int64_t>(x_floor);

        const scalar_t w11 = (1 - y_rem) * (1 - x_rem);
        const scalar_t w10 = (1 - y_rem) * x_rem;
        const scalar_t w01 = y_rem * (1 - x_rem);
        const scalar_t w00 = y_rem * x_rem;

        const int64_t j0 = std::max(j_offset, (int64_t)0);
        const int64_t j1 = std::min(h, h + j_offset + 1);
        const int64_t k0 = std::max(k_offset, (int64_t)0);
        const int64_t k1 = std::min(w, w + k_offset + 1);

        for (int64_t j = j0; j < j1; j++)
        {
            for (int64_t k = k0; k < k1; k++)
            {
                scalar_t v00 = 0.0;
                scalar_t v01 = 0.0;
                scalar_t v10 = 0.0;
                scalar_t v11 = 0.0;

                if (j - j_offset - 1 >= 0 && k - k_offset - 1 >= 0)
                {
                    v00 = input[j - j_offset - 1][k - k_offset - 1];
                }

                if (j - j_offset - 1 >= 0 && k - k_offset < w)
                {
                    v01 = input[j - j_offset - 1][k - k_offset];
                }

                if (j - j_offset < h && k - k_offset - 1 >= 0)
                {
                    v10 = input[j - j_offset][k - k_offset - 1];
                }

                if (j - j_offset < h && k - k_offset < w)
                {
                    v11 = input[j - j_offset][k - k_offset];
                }

                out[j][k] = w00 * v00 + w01 * v01 + w10 * v10 + w11 * v11;
            }
        }
    };

    // Parallelize over batches and channels.
    std::vector<int64_t> batch_range(input_a.size(0));
    std::iota(batch_range.begin(), batch_range.end(), 0);

    std::vector<int64_t> channel_range(input_a.size(1));
    std::iota(channel_range.begin(), channel_range.end(), 0);

    lietorch::for_each(
        batch_range.begin(), batch_range.end(), [&](const int64_t& b) {
            lietorch::for_each(
                channel_range.begin(),
                channel_range.end(),
                [&](const int64_t& c) { convection_fw_one_instance(b, c); });
        });
}

/*

 */
std::tuple<torch::Tensor, torch::Tensor>
convection_fw_cpu(const torch::Tensor& input, const torch::Tensor& g0)
{
    const auto out = torch::zeros_like(input);

    if (input.requires_grad() || g0.requires_grad())
    {

        std::vector<int64_t> field_size = input.sizes().vec(); // = {B,C,H,W}
        field_size.push_back(2);                               // = {B,C,H,W,2}
        const auto out_grad_field = input.new_zeros(field_size);

        AT_DISPATCH_FLOATING_TYPES(input.scalar_type(), __func__, [&] {
            const auto input_a = input.accessor<scalar_t, 4>();
            const auto out_a = out.accessor<scalar_t, 4>();
            const auto out_grad_field_a =
                out_grad_field.accessor<scalar_t, 5>();

            switch (g0.scalar_type())
            {

            case at::ScalarType::Float:
            {
                const auto g0_a = g0.accessor<float, 2>();
                convection_fw_cpu_impl<scalar_t, float>(
                    input_a, g0_a, out_a, out_grad_field_a);
                break;
            }

            case at::ScalarType::Double:
            {
                const auto g0_a = g0.accessor<double, 2>();
                convection_fw_cpu_impl<scalar_t, double>(
                    input_a, g0_a, out_a, out_grad_field_a);
                break;
            }

            default:
                TORCH_CHECK(
                    false,
                    __func__,
                    " not implemented for '",
                    toString(g0.scalar_type()),
                    "' for the convection vector.");
            }
        });

        return std::make_tuple(out, out_grad_field);
    }
    else
    {
        AT_DISPATCH_FLOATING_TYPES(input.scalar_type(), __func__, [&] {
            const auto input_a = input.accessor<scalar_t, 4>();
            const auto out_a = out.accessor<scalar_t, 4>();

            switch (g0.scalar_type())
            {

            case at::ScalarType::Float:
            {
                const auto g0_a = g0.accessor<float, 2>();
                convection_fw_cpu_impl_nograd<scalar_t, float>(
                    input_a, g0_a, out_a);
                break;
            }

            case at::ScalarType::Double:
            {
                const auto g0_a = g0.accessor<double, 2>();
                convection_fw_cpu_impl_nograd<scalar_t, double>(
                    input_a, g0_a, out_a);
                break;
            }

            default:
                TORCH_CHECK(
                    false,
                    __func__,
                    " not implemented for '",
                    toString(g0.scalar_type()),
                    "' for the convection vector.");
            }
        });

        return std::make_tuple(out, torch::Tensor());
    }
}

/*

*/
template <typename scalar_t, typename coordinate_t = float>
void
convection_bw_cpu_impl(
    const torch::TensorAccessor<coordinate_t, 2> g0_a,         // [C,2]
    const torch::TensorAccessor<scalar_t, 4> grad_a,           // [B,C,H,W]
    torch::TensorAccessor<scalar_t, 4> input_grad_a,           // [B,C,H,W]
    const torch::TensorAccessor<scalar_t, 5> out_grad_field_a, // [B,C,H,W,2]
    torch::TensorAccessor<coordinate_t, 5> g0_splits_a)        // [B,C,H,W,2]
{
    const int64_t h = grad_a.size(2); // number of y's
    const int64_t w = grad_a.size(3); // number of x's

    // Backward for one batch and channel or 'instance'.
    auto convection_bw_one_instance = [&](const int64_t& b, const int64_t c) {
        const auto g0 = g0_a[c];
        const auto grad = grad_a[b][c];
        auto in_grad = input_grad_a[b][c];
        const auto out_grad_field = out_grad_field_a[b][c];
        auto g0_splits = g0_splits_a[b][c];

        const coordinate_t y_floor = std::floor(-g0[0]);
        const coordinate_t x_floor = std::floor(-g0[1]);
        const scalar_t y_rem = static_cast<scalar_t>(-g0[0] - y_floor);
        const scalar_t x_rem = static_cast<scalar_t>(-g0[1] - x_floor);
        const int64_t j_offset = static_cast<int64_t>(y_floor);
        const int64_t k_offset = static_cast<int64_t>(x_floor);

        const scalar_t w11 = (1 - y_rem) * (1 - x_rem);
        const scalar_t w10 = (1 - y_rem) * x_rem;
        const scalar_t w01 = y_rem * (1 - x_rem);
        const scalar_t w00 = y_rem * x_rem;

        const int64_t j0 = std::max(j_offset, (int64_t)0);
        const int64_t j1 = std::min(h, h + j_offset + 1);
        const int64_t k0 = std::max(k_offset, (int64_t)0);
        const int64_t k1 = std::min(w, w + k_offset + 1);

        for (int64_t j = j0; j < j1; j++)
        {
            for (int64_t k = k0; k < k1; k++)
            {
                scalar_t v00 = 0.0;
                scalar_t v01 = 0.0;
                scalar_t v10 = 0.0;
                scalar_t v11 = 0.0;

                if (j - j_offset - 1 >= 0 && k - k_offset - 1 >= 0)
                {
                    v00 = grad[j - j_offset - 1][k - k_offset - 1];
                }

                if (j - j_offset - 1 >= 0 && k - k_offset < w)
                {
                    v01 = grad[j - j_offset - 1][k - k_offset];
                }

                if (j - j_offset < h && k - k_offset - 1 >= 0)
                {
                    v10 = grad[j - j_offset][k - k_offset - 1];
                }

                if (j - j_offset < h && k - k_offset < w)
                {
                    v11 = grad[j - j_offset][k - k_offset];
                }

                in_grad[j][k] = w00 * v00 + w01 * v01 + w10 * v10 + w11 * v11;

                const int64_t j2 = h - j - 1;
                const int64_t k2 = w - k - 1;
                const coordinate_t dy =
                    static_cast<coordinate_t>(out_grad_field[j2][k2][0]);
                const coordinate_t dx =
                    static_cast<coordinate_t>(out_grad_field[j2][k2][1]);
                const coordinate_t gval =
                    static_cast<coordinate_t>(grad[j2][k2]);

                g0_splits[j2][k2][0] = dy * gval;
                g0_splits[j2][k2][1] = dx * gval;
            }
        }
    };

    // Parallelize over batches and channels.
    std::vector<int64_t> batch_range(grad_a.size(0));
    std::iota(batch_range.begin(), batch_range.end(), 0);

    std::vector<int64_t> channel_range(grad_a.size(1));
    std::iota(channel_range.begin(), channel_range.end(), 0);

    lietorch::for_each(
        batch_range.begin(), batch_range.end(), [&](const int64_t& b) {
            lietorch::for_each(
                channel_range.begin(),
                channel_range.end(),
                [&](const int64_t& c) { convection_bw_one_instance(b, c); });
        });
}

/*

 */
std::tuple<torch::Tensor, torch::Tensor>
convection_bw_cpu(
    const torch::Tensor& g0,
    const torch::Tensor& grad,
    const torch::Tensor& out_grad_field)
{
    using namespace torch::indexing;

    const auto input_grad = torch::zeros_like(grad);
    const auto g0_splits = torch::zeros_like(
        out_grad_field, torch::TensorOptions().dtype(g0.scalar_type()));

    AT_DISPATCH_FLOATING_TYPES(grad.scalar_type(), __func__, [&] {
        const auto grad_a = grad.accessor<scalar_t, 4>();
        const auto input_grad_a = input_grad.accessor<scalar_t, 4>();
        const auto out_grad_field_a = out_grad_field.accessor<scalar_t, 5>();

        switch (g0.scalar_type())
        {

        case at::ScalarType::Float:
        {
            const auto g0_a = g0.accessor<float, 2>();
            const auto g0_splits_a = g0_splits.accessor<float, 5>();
            convection_bw_cpu_impl<scalar_t, float>(
                g0_a, grad_a, input_grad_a, out_grad_field_a, g0_splits_a);
            break;
        }

        case at::ScalarType::Double:
        {
            const auto g0_a = g0.accessor<double, 2>();
            const auto g0_splits_a = g0_splits.accessor<double, 5>();
            convection_bw_cpu_impl<scalar_t, double>(
                g0_a, grad_a, input_grad_a, out_grad_field_a, g0_splits_a);
            break;
        }

        default:
            TORCH_CHECK(
                false,
                __func__,
                " not implemented for '",
                toString(g0.scalar_type()),
                "' for the convection vector.");
        }
    });

    const auto g0_grad = g0_splits.sum({0, 2, 3});

    return std::make_tuple(input_grad, g0_grad);
}

} // namespace r2
} // namespace lietorch