#pragma once
#ifndef LIETORCH_VFORMAT_H
#define LIETORCH_VFORMAT_H

/** @file vformat.h
 *  @brief Helper functions dealing with strings and formatting.
 *
 * */

#include <cstdarg>
#include <iostream>
#include <string>
#include <vector>

/**
 *
 *  @brief Takes the filename from a full path. Intended to be used with the
 * __FILE__ macro as: get_file_name(__FILE__)
 */
constexpr auto*
get_file_name(const char* const path)
{
    const auto* startPosition = path;
    for (const auto* currentCharacter = path; *currentCharacter != '\0';
         ++currentCharacter)
    {
        if (*currentCharacter == '\\' || *currentCharacter == '/')
        {
            startPosition = currentCharacter;
        }
    }

    if (startPosition != path)
    {
        ++startPosition;
    }

    return startPosition;
}

/**
 *  @brief C-style string formatting if std::vformat (C++20) is not available.
 */
inline const std::string
vformat(const char* const zcFormat, ...)
{
    // initialize use of the variable argument array
    va_list vaArgs;
    va_start(vaArgs, zcFormat);

    // reliably acquire the size
    // from a copy of the variable argument array
    // and a functionally reliable call to mock the formatting
    va_list vaArgsCopy;
    va_copy(vaArgsCopy, vaArgs);
    const int iLen = std::vsnprintf(NULL, 0, zcFormat, vaArgsCopy);
    va_end(vaArgsCopy);

    // return a formatted string without risking memory mismanagement
    // and without assuming any compiler or platform specific behavior
    std::vector<char> zc(iLen + 1);
    std::vsnprintf(zc.data(), zc.size(), zcFormat, vaArgs);
    va_end(vaArgs);
    return std::string(zc.data(), iLen);
}

#endif