<#
.SYNOPSIS
    Powershell script for running the Docker container that will compile the native backend for Windows.

.PARAMETER BuildType
    Specifies the type of build, must be one of Release, RelWithDebInfo, Debug or MinRelSize.

.PARAMETER Parallel
    Specifies the amount of concurrent build jobs to execute, defaults to 16.

.PARAMETER Cpus
    Specifies the amount of cpus assigned to the container, defaults to 8.

.PARAMETER Memory
    Specifies the amount of memory assigned to the container, defaults to "8g", i.e. 8 gigabyte.

.NOTES
    Calling the script with the switch '-Interactive' will start the container in interactive mode without running any build tasks.

#>
param ($BuildType = "RelWithDebInfo", $Parallel = 16, $Cpus = 8, $Memory = "8g")

$LtBuildImage = "bmnsmets/ltbuild:windows20H2-pytorch1.12.1-cuda11.7"

$ProjectPath = (Get-Item $PSScriptRoot).parent.FullName


# Check if the BuildType is allowed
$BuildTypes = @("Release", "RelWithDebInfo", "Debug", "MinRelSize")
if ($BuildTypes -NotContains $BuildType)
{
    Write-Host -Separator "" -ForegroundColor Red `
    ("[ltbuild.ps1 ", (Get-Date -Format HH:mm:ss) ,"] BuildType must be Release, RelWithDebInfo, Debug or MinRelSize.")
    Exit 1
}

if ($args[0] -eq "-Interactive") # Interactive mode
{
    Write-Host -Separator "" -ForegroundColor Green `
    ("[ltbuild.ps1 ", (Get-Date -Format HH:mm:ss) ,"] Starting container in interactive mode")
    Write-Host -Separator "" -ForegroundColor Green `
    ("[ltbuild.ps1 ", (Get-Date -Format HH:mm:ss) ,"] Container image: ", $LtBuildImage)

    docker run --rm -it --cpus $Cpus -m $Memory -a STDOUT --mount type=bind,source="$ProjectPath",target="C:\SRC" `
        $LtBuildImage

    if (-not $?)
    {
        Write-Host -Separator "" -ForegroundColor Red `
        ("[ltbuild.ps1 ", (Get-Date -Format HH:mm:ss) ,"] Container launch returned with code ", $LASTEXITCODE, ".")
        Exit $LASTEXITCODE
    }
} else # Build mode
{
    Write-Host -Separator "" -ForegroundColor Green `
    ("[ltbuild.ps1 ", (Get-Date -Format HH:mm:ss) ,"] Starting build (", $BuildType ,")")
    Write-Host -Separator "" -ForegroundColor Green `
    ("[ltbuild.ps1 ", (Get-Date -Format HH:mm:ss) ,"] Container image: ", $LtBuildImage)
    
    docker run --rm --cpus $Cpus -m $Memory -a STDOUT --mount type=bind,source="$ProjectPath",target="C:\SRC" `
        $LtBuildImage `
        cmake -B"C:\SRC\backend\build" -S"C:\SRC\backend" -GNinja -DCMAKE_BUILD_TYPE="$BuildType" `& `
        cmake --build "C:\SRC\backend\build" --target all --config "$BuildType" -j $Parallel

    if (-not $?)
    {
        Write-Host -Separator "" -ForegroundColor Red `
        ("[ltbuild.ps1 ", (Get-Date -Format HH:mm:ss) ,"] Build process failed with code ", $LASTEXITCODE, ".")
        Exit $LASTEXITCODE
    }

    Write-Host -Separator "" -ForegroundColor Green `
    ("[ltbuild.ps1 ", (Get-Date -Format HH:mm:ss) ,"] Copy lietorch.dll to \lietorch\lib\")
    New-Item -ItemType Directory -Force -Path "$ProjectPath\lietorch\lib"
    Copy-Item -Path "$ProjectPath\backend\build\lietorch.dll" -Destination "$ProjectPath\lietorch\lib\" -Force
}


Write-Host -Separator "" -ForegroundColor Green `
("[ltbuild.ps1 ", (Get-Date -Format HH:mm:ss) ,"] Done.")
Exit 0

