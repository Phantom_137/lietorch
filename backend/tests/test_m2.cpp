

#include "../src/torch_include.h"
#include "gtest/gtest.h"

#include "../src/cpu/m2_cpu.h"
#include "../src/cpu/m2_interpolation_cpu.h"
#include "../src/cuda/m2_cuda.cuh"
#include "../src/dbg.h"
#include "../src/m2.h"
#include "../src/m2_element.h"

const auto options = torch::TensorOptions().dtype(torch::kFloat64);

const auto tensor1 =
    torch::tensor({{{0, 0}, {0, 0}}, {{1, 1}, {1, 1}}}, options);
const auto acc1 = tensor1.accessor<double, 3>();

const auto tensor2 =
    torch::tensor({{{0, 0}, {1, 1}}, {{0, 0}, {1, 1}}}, options);
const auto acc2 = tensor2.accessor<double, 3>();

const auto tensor3 =
    torch::tensor({{{0, 1}, {0, 1}}, {{0, 1}, {0, 1}}}, options);
const auto acc3 = tensor3.accessor<double, 3>();

const auto tensor4 = tensor1 + tensor2 + tensor3;
const auto acc4 = tensor4.accessor<double, 3>();

const auto tensor5 =
    torch::tensor({{{1, 2}, {3, 4}}, {{11, 12}, {13, 14}}}, options);
const auto acc5 = tensor5.accessor<double, 3>();

TEST(M2, interpolate_trilinear)
{

    auto ipol = [](const torch::TensorAccessor<double, 3>& input,
                   double i,
                   double j,
                   double k) {
        return ::lietorch::m2::interpolate_trilinear<double, double>(
            input, i, j, k);
    };

    EXPECT_DOUBLE_EQ(ipol(acc1, 0.5, 0.5, 0.5), 0.5);
    EXPECT_DOUBLE_EQ(ipol(acc2, 0.5, 0.5, 0.5), 0.5);
    EXPECT_DOUBLE_EQ(ipol(acc3, 0.5, 0.5, 0.5), 0.5);

    EXPECT_DOUBLE_EQ(ipol(acc1, 0.0, 0.0, 0.0), 0.0);
    EXPECT_DOUBLE_EQ(ipol(acc2, 0.0, 0.0, 0.0), 0.0);
    EXPECT_DOUBLE_EQ(ipol(acc3, 0.0, 0.0, 0.0), 0.0);

    EXPECT_DOUBLE_EQ(ipol(acc1, 0.0, 0.5, 0.5), 0.0);
    EXPECT_DOUBLE_EQ(ipol(acc1, 1.0, 0.0, 0.0), 1.0);
    EXPECT_DOUBLE_EQ(ipol(acc1, 1.0, 0.5, 0.5), 1.0);

    EXPECT_DOUBLE_EQ(ipol(acc2, 0.5, 0.0, 0.5), 0.0);
    EXPECT_DOUBLE_EQ(ipol(acc2, 0.0, 1.0, 0.0), 1.0);
    EXPECT_DOUBLE_EQ(ipol(acc2, 1.0, 1.0, 1.0), 1.0);

    EXPECT_DOUBLE_EQ(ipol(acc3, 0.5, 0.5, 0.0), 0.0);
    EXPECT_DOUBLE_EQ(ipol(acc3, 0.0, 0.0, 1.0), 1.0);
    EXPECT_DOUBLE_EQ(ipol(acc3, 1.0, 1.0, 1.0), 1.0);

    EXPECT_DOUBLE_EQ(ipol(acc1, -2.0, 0.0, 0.0), 0.0);
    EXPECT_DOUBLE_EQ(ipol(acc1, -1.5, 0.0, 0.0), 0.5);
    EXPECT_DOUBLE_EQ(ipol(acc1, -1.0, 0.0, 0.0), 1.0);
    EXPECT_DOUBLE_EQ(ipol(acc1, 2.0, 0.0, 0.0), 0.0);
    EXPECT_DOUBLE_EQ(ipol(acc1, 2.5, 0.0, 0.0), 0.5);
    EXPECT_DOUBLE_EQ(ipol(acc1, 3.0, 0.0, 0.0), 1.0);

    EXPECT_DOUBLE_EQ(ipol(acc2, 0.0, 0.5, 0.0), 0.5);
    EXPECT_DOUBLE_EQ(ipol(acc2, 0.0, 1.0, 0.0), 1.0);
    EXPECT_DOUBLE_EQ(ipol(acc2, 0.0, 1.5, 0.0), 0.5);
    EXPECT_DOUBLE_EQ(ipol(acc2, 0.0, 2.0, 0.0), 0.0);

    EXPECT_DOUBLE_EQ(ipol(acc3, 1.0, 0.0, -0.5), 0.0);
    EXPECT_DOUBLE_EQ(ipol(acc3, 1.0, 0.0, 0.5), 0.5);
    EXPECT_DOUBLE_EQ(ipol(acc3, 1.0, 0.0, 1.0), 1.0);
    EXPECT_DOUBLE_EQ(ipol(acc3, 1.0, 0.0, 1.5), 0.5);
    EXPECT_DOUBLE_EQ(ipol(acc3, 1.0, 0.0, 2.0), 0.0);

    EXPECT_DOUBLE_EQ(ipol(acc1, 0.25, 0.5, 0.5), 0.25);
    EXPECT_DOUBLE_EQ(ipol(acc1, -0.25, 0.5, 0.5), 0.25);
    EXPECT_DOUBLE_EQ(ipol(acc1, 1.75, 0.5, 0.5), 0.25);

    EXPECT_DOUBLE_EQ(ipol(acc5, 0.0, 0.0, -0.5), 0.5);
}

namespace
{
inline void
expect_ipolg_eq(
    std::tuple<double, double, double, double> computed,
    const double v,
    const double dz,
    const double dy,
    const double dx)
{
    auto [_v, _dz, _dy, _dx] = computed;
    EXPECT_NEAR(_v, v, 0.001);
    EXPECT_NEAR(_dz, dz, 0.001);
    EXPECT_NEAR(_dy, dy, 0.001);
    EXPECT_NEAR(_dx, dx, 0.001);
}
} // namespace

TEST(M2, interpolate_trilinear_with_grad)
{
    auto ipolg = [](const torch::TensorAccessor<double, 3>& input,
                    double i,
                    double j,
                    double k) {
        return ::lietorch::m2::interpolate_trilinear_with_grad<double, double>(
            input, i, j, k, 0.0);
    };
    const double SQ2 = 1.0 / sqrt(2.0);

    expect_ipolg_eq(ipolg(acc1, 0.5, 0.5, 0.5), 0.5, 1.0, 0.0, 0.0);

    expect_ipolg_eq(ipolg(acc3, -0.25, 0.0, 0.0), 0.0, 0.0, 0.0, 1.0);
    expect_ipolg_eq(ipolg(acc3, 0.0, 0.0, 0.0), 0.0, 0.0, 0.0, 1.0);
    expect_ipolg_eq(ipolg(acc3, 0.5, 0.0, 0.0), 0.0, 0.0, 0.0, 1.0);
    expect_ipolg_eq(ipolg(acc3, 1.0, 0.0, 0.0), 0.0, 0.0, 0.0, 1.0);
    expect_ipolg_eq(ipolg(acc3, 1.5, 0.0, 0.0), 0.0, 0.0, 0.0, 1.0);
    expect_ipolg_eq(ipolg(acc3, 2.0, 0.0, 0.0), 0.0, 0.0, 0.0, 1.0);

    expect_ipolg_eq(ipolg(acc2, 0.0, 0.0, 0.0), 0.0, 0.0, 1.0, 0.0);
    expect_ipolg_eq(ipolg(acc2, 0.5, 0.0, 0.0), 0.0, 0.0, 1.0, 0.0);
    expect_ipolg_eq(ipolg(acc2, 1.0, 0.0, 0.0), 0.0, 0.0, 1.0, 0.0);
    expect_ipolg_eq(ipolg(acc2, 1.5, 0.0, 0.0), 0.0, 0.0, 1.0, 0.0);

    expect_ipolg_eq(ipolg(acc2, -0.25, 0.5, 0.5), 0.5, 0.0, 1.0, 0.0);

    expect_ipolg_eq(ipolg(acc1, 0.0, 0.0, 0.0), 0.0, 1.0, 0.0, 0.0);
    expect_ipolg_eq(ipolg(acc2, 0.0, 0.0, 0.0), 0.0, 0.0, 1.0, 0.0);
    expect_ipolg_eq(ipolg(acc3, 0.0, 0.0, 0.0), 0.0, 0.0, 0.0, 1.0);
    expect_ipolg_eq(ipolg(acc4, 0.0, 0.0, 0.0), 0.0, 1.0, 1.0, 1.0);

    expect_ipolg_eq(ipolg(acc1, 0.25, 0.0, 0.0), 0.25, 1.0, 0.0, 0.0);
    expect_ipolg_eq(ipolg(acc2, 0.25, 0.0, 0.0), 0.0, 0.0, 1.0, 0.0);
    expect_ipolg_eq(ipolg(acc3, 0.25, 0.0, 0.0), 0.0, 0.0, 0.0, 1.0);
    expect_ipolg_eq(ipolg(acc4, 0.25, 0.0, 0.0), 0.25, 1.0, 1.0, 1.0);

    expect_ipolg_eq(ipolg(acc1, -0.25, 0.0, 0.0), 0.25, -1.0, 0.0, 0.0);
    expect_ipolg_eq(ipolg(acc2, -0.25, 0.0, 0.0), 0.0, 0.0, 1.0, 0.0);
    expect_ipolg_eq(ipolg(acc3, -0.25, 0.0, 0.0), 0.0, 0.0, 0.0, 1.0);
    expect_ipolg_eq(ipolg(acc4, -0.25, 0.0, 0.0), 0.25, -1.0, 1.0, 1.0);

    expect_ipolg_eq(ipolg(acc1, -0.5, 0.0, 0.0), 0.5, -1.0, 0.0, 0.0);
    expect_ipolg_eq(ipolg(acc2, -0.5, 0.0, 0.0), 0.0, 0.0, 1.0, 0.0);
    expect_ipolg_eq(ipolg(acc3, -0.5, 0.0, 0.0), 0.0, 0.0, 0.0, 1.0);
    expect_ipolg_eq(ipolg(acc4, -0.5, 0.0, 0.0), 0.5, -1.0, 1.0, 1.0);
}

TEST(M2, convection_fw_cpu)
{
    using namespace torch::indexing;

    auto options =
        torch::TensorOptions().dtype(torch::kFloat32).device(torch::kCPU);

    const auto d = torch::tensor({{{{{1, 2}, {3, 4}}}}}, options);

    const auto g1 = torch::tensor({{0.0, 0.0, 0.5}}, options);
    const auto d1f = torch::tensor({{{{{0.5, 1.5}, {1.5, 3.5}}}}}, options);
    const auto d1_field = torch::tensor(
        {{{{{{0.0, 1.0, 1.0}, {0.0, 2.0, 1.0}},
            {{0.0, -1.5, 3.0}, {0.0, -3.5, 1.0}}}}}},
        options);

    const auto [d1f_out, d1_field_out] =
        ::lietorch::m2::convection_fw_cpu(d, g1);

    EXPECT_TRUE(d1f_out.allclose(d1f));
    EXPECT_TRUE(d1_field_out.allclose(d1_field));

    const auto g2 = torch::tensor({{0.0, 0.5, 0.0}}, options);
    const auto d2f = torch::tensor({{{{{0.5, 1.0}, {2.0, 3.0}}}}}, options);
    const auto d2_field = torch::tensor(
        {{{{{{0.0, 1.0, 0.5}, {0.0, 2.0, -1.0}},
            {{0.0, 2.0, 1.0}, {0.0, 2.0, -3.0}}}}}},
        options);

    const auto [d2f_out, d2_field_out] =
        ::lietorch::m2::convection_fw_cpu(d, g2);

    EXPECT_TRUE(d2f_out.allclose(d2f));
    EXPECT_TRUE(d2_field_out.allclose(d2_field));
}

TEST(M2, convection_bw_cpu)
{
    using namespace torch::indexing;

    std::vector<int64_t> size{2, 3, 8, 5, 5};
    std::vector<int64_t> field_size = size;
    field_size.push_back(3);

    auto options =
        torch::TensorOptions().dtype(torch::kFloat32).device(torch::kCPU);

    auto a = torch::ones(size, options);
    auto field = torch::ones(field_size, options);
    auto g0 = torch::zeros({3, 3}, options);

    auto [input_grad, g0_grad] =
        ::lietorch::m2::convection_bw_cpu(g0, a, field);

    const auto g0_grad_expected =
        torch::tensor(
            {{-1.0, 0.0, 0.0}, {-1.0, 0.0, 0.0}, {-1.0, 0.0, 0.0}}, options) *
        400.0;

    EXPECT_TRUE(g0_grad.allclose(g0_grad_expected, 1e-5, 1e-4));
    EXPECT_TRUE(size == input_grad.sizes());
    EXPECT_TRUE((g0_grad.sizes() == std::vector<int64_t>{3, 3}));
}

TEST(M2, convection_fw_cuda)
{
    using namespace torch::indexing;

    auto options = torch::TensorOptions()
                       .dtype(torch::kFloat32)
                       .device(torch::kCUDA)
                       .requires_grad(true);

    const auto d = torch::tensor({{{{{1, 2}, {3, 4}}}}}, options);

    const auto g1 = torch::tensor({{0.0, 0.0, 0.5}}, options);
    const auto d1f = torch::tensor({{{{{0.5, 1.5}, {1.5, 3.5}}}}}, options);
    const auto d1_field = torch::tensor(
        {{{{{{0.0, 1.0, 1.0}, {0.0, 2.0, 1.0}},
            {{0.0, -1.5, 3.0}, {0.0, -3.5, 1.0}}}}}},
        options);

    const auto [d1f_out, d1_field_out] =
        ::lietorch::m2::convection_fw_cuda(d, g1);

    EXPECT_TRUE(d1f_out.allclose(d1f));
    EXPECT_TRUE(d1_field_out.allclose(d1_field));

    const auto g2 = torch::tensor({{0.0, 0.5, 0.0}}, options);
    const auto d2f = torch::tensor({{{{{0.5, 1.0}, {2.0, 3.0}}}}}, options);
    const auto d2_field = torch::tensor(
        {{{{{{0.0, 1.0, 0.5}, {0.0, 2.0, -1.0}},
            {{0.0, 2.0, 1.0}, {0.0, 2.0, -3.0}}}}}},
        options);

    const auto [d2f_out, d2_field_out] =
        ::lietorch::m2::convection_fw_cuda(d, g2);

    EXPECT_TRUE(d2f_out.allclose(d2f));
    EXPECT_TRUE(d2_field_out.allclose(d2_field));
}

TEST(M2, convection_bw_cuda)
{
    using namespace torch::indexing;

    std::vector<int64_t> size{2, 3, 8, 5, 5};
    std::vector<int64_t> field_size = size;
    field_size.push_back(3);

    auto options =
        torch::TensorOptions().dtype(torch::kFloat32).device(torch::kCUDA);

    auto a = torch::ones(size, options);
    auto field = torch::ones(field_size, options);
    auto g0 = torch::zeros({3, 3}, options);

    auto [input_grad, g0_grad] =
        ::lietorch::m2::convection_bw_cuda(g0, a, field);

    const auto g0_grad_expected =
        torch::tensor(
            {{-1.0, 0.0, 0.0}, {-1.0, 0.0, 0.0}, {-1.0, 0.0, 0.0}}, options) *
        400.0;

    EXPECT_TRUE(g0_grad.allclose(g0_grad_expected, 1e-5, 1e-4));
    EXPECT_TRUE(size == input_grad.sizes());
    EXPECT_TRUE((g0_grad.sizes() == std::vector<int64_t>{3, 3}));
}

TEST(M2, convection_fw_compare)
{
    auto options = torch::TensorOptions()
                       .dtype(torch::kFloat32)
                       .device(torch::kCPU)
                       .requires_grad(true);

    const auto input = torch::randn({2, 3, 8, 10, 10}, options);
    const auto g1 = torch::tensor(
        {{0.0, 0.5, 1.2}, {-1.2, 1.5, 0.7}, {1.4, -1.0, -0.3}}, options);

    const auto [out_cpu, field_cpu] =
        ::lietorch::m2::convection_fw_cpu(input, g1);
    const auto [out_cuda, field_cuda] =
        ::lietorch::m2::convection_fw_cuda(input.cuda(), g1.cuda());

    EXPECT_TRUE(out_cpu.allclose(out_cuda.cpu(), 1e-3, 1e-5));
    EXPECT_TRUE(field_cpu.allclose(field_cuda.cpu(), 1e-3, 1e-5));

    auto options2 =
        torch::TensorOptions().dtype(torch::kFloat64).device(torch::kCPU);

    const auto input2 = torch::randn({2, 3, 8, 10, 10}, options2);

    const auto [out_cpu2, field_cpu2] =
        ::lietorch::m2::convection_fw_cpu(input2, g1);
    const auto [out_cuda2, field_cuda2] =
        ::lietorch::m2::convection_fw_cuda(input2.cuda(), g1.cuda());

    EXPECT_TRUE(out_cpu2.allclose(out_cuda2.cpu(), 1e-3, 1e-5));
    EXPECT_TRUE(field_cpu2.allclose(field_cuda2.cpu(), 1e-3, 1e-5));
}

TEST(M2, convection_bw_compare)
{
    auto options =
        torch::TensorOptions().dtype(torch::kFloat32).device(torch::kCPU);

    const auto grad = torch::randn({2, 3, 8, 10, 10}, options);
    const auto grad_field = torch::randn({2, 3, 8, 10, 10, 3}, options);
    const auto g1 = torch::tensor(
        {{0.0, 0.5, 1.2}, {-1.2, 1.5, 0.7}, {1.4, -1.0, -0.3}}, options);

    const auto [input_grad_cpu, g0_grad_cpu] =
        lietorch::m2::convection_bw_cpu(g1, grad, grad_field);
    const auto [input_grad_cuda, g0_grad_cuda] =
        lietorch::m2::convection_bw_cuda(
            g1.cuda(), grad.cuda(), grad_field.cuda());

    EXPECT_TRUE(input_grad_cpu.allclose(input_grad_cuda.cpu(), 1e-3, 1e-5));
    EXPECT_TRUE(g0_grad_cpu.allclose(g0_grad_cuda.cpu(), 1e-3, 1e-5));

    auto options2 =
        torch::TensorOptions().dtype(torch::kFloat64).device(torch::kCPU);

    const auto grad2 = torch::randn({2, 3, 8, 10, 10}, options2);
    const auto grad_field2 = torch::randn({2, 3, 8, 10, 10, 3}, options2);

    const auto [input_grad_cpu2, g0_grad_cpu2] =
        lietorch::m2::convection_bw_cpu(g1, grad2, grad_field2);
    const auto [input_grad_cuda2, g0_grad_cuda2] =
        lietorch::m2::convection_bw_cuda(
            g1.cuda(), grad2.cuda(), grad_field2.cuda());

    EXPECT_TRUE(input_grad_cpu2.allclose(input_grad_cuda2.cpu(), 1e-3, 1e-5));
    EXPECT_TRUE(g0_grad_cpu2.allclose(g0_grad_cuda2.cpu(), 1e-3, 1e-5));
}

TEST(M2, element)
{
    using elem = ::lietorch::m2::element<double>;

    const elem e(0.0, 0.0, 0.0);
    EXPECT_TRUE(e == e.inv());
    EXPECT_TRUE(e == elem::e());
    EXPECT_TRUE(elem::e().inv() == elem::e());

    const elem g1(0.0, 0.0, 1.0);
    EXPECT_TRUE(g1.inv() == elem(0.0, 0.0, -1.0));
    const elem g2(0.0, 1.0, 0.0);
    EXPECT_TRUE(g2.inv() == elem(0.0, -1.0, 0.0));
    const elem g3(1.0, 0.0, 0.0);
    EXPECT_TRUE(g3.inv() == elem(-1.0, 0.0, 0.0));

    EXPECT_THROW(elem(1.0, 1.0, 1.0, 0.0), std::invalid_argument);

    const elem g4(M_PI / 2.0, 1.0, 1.5);
    const elem g5(2.0, 1.0, 1.5, 8.0);
    EXPECT_TRUE(g4.close_to(g5));
    EXPECT_TRUE(g4.inv().close_to(g5.inv()));
    EXPECT_TRUE(g4.inv().close_to(elem(-M_PI / 2.0, 1.5, -1.0)));

    EXPECT_TRUE(e.close_to(g4 * g4.inv()));
    EXPECT_TRUE(e.close_to(g4 * g5.inv()));
    EXPECT_TRUE(e.close_to(g5 * g4.inv()));

    const elem g6(1.0, 0.0, 1.0, 3.0);
    EXPECT_TRUE((g6 * g6 * g6).close_to(e));

    const elem g7(1.0, 0.1, 0.5, 8.0);
    const elem g8(0.5, 0.1, 0.5, 4.0);
    EXPECT_TRUE(g7.close_to(g8));
    EXPECT_TRUE((g7 * g8).t_scale == 8.0);
    EXPECT_TRUE((g4 * g7).close_to(g5 * g8));

    EXPECT_TRUE(elem(1.0, 1.0, 1.0).close_to(elem(1.0 - 2 * M_PI, 1.0, 1.0)));
    EXPECT_TRUE(elem(1.0, 1.0, 1.0).close_to(elem(1.0 + 2 * M_PI, 1.0, 1.0)));
    EXPECT_TRUE(elem(M_PI, 1.0, 1.0).close_to(elem(-4.0, 1.0, 1.0, 8.0)));
    EXPECT_TRUE(elem(M_PI, 1.0, 1.0).close_to(elem(4.0, 1.0, 1.0, 8.0)));

    const elem g9(1.0, M_SQRT1_2, M_SQRT1_2, 8.0);
    const elem g10(-2.5, 0.5, 1.5, 10.0);
    const elem g11(-1.0, 2.12132, 1.41421, 8.0);
    EXPECT_TRUE(g11.close_to(g9 * g10));
    EXPECT_TRUE((g9 * g10 * g11.inv()).close_to(e));
    EXPECT_TRUE((g11 * (g9 * g10).inv()).close_to(e));
}

TEST(M2, rotated_kernel_stack_trilinear_cpu)
{
    auto options = torch::TensorOptions().dtype(torch::kFloat32);
    const auto kernel = torch::tensor(
        {{{{0, 0, 0}, {0, 0, 0}, {0, 0, 0}},
          {{0, 0, 0}, {0, 1, 1}, {0, 0, 0}},
          {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}}}},
        options);
    const auto stack = torch::empty({8, 1, 3, 3, 3}, options);
    auto kernel_a = kernel.accessor<float, 4>();
    auto stack_a = stack.accessor<float, 5>();
    ::lietorch::m2::rotated_kernel_stack_trilinear_cpu<float>(
        kernel_a, stack_a, 0.0);

    const auto kernel1 = torch::tensor(
        {{{{0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}},
          {{0.0, 0.0858, 0.0}, {0.0858, 1.0, 0.2929}, {0.0, 0.2929, 0.5858}},
          {{0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}}}},
        options);

    const auto kernel2 = torch::tensor(
        {{{{0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}},
          {{0.0, 0.0, 0.0}, {0.0, 1.0, 0.0}, {0.0, 1.0, 0.0}},
          {{0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}}}},
        options);

    EXPECT_TRUE(kernel.allclose(stack[0], 1e-3));
    EXPECT_TRUE(kernel1.allclose(stack[1], 1e-3));
    EXPECT_TRUE((kernel2 - stack[2]).abs().sum().item().toFloat() < 1e-4);

    const auto m = 100.0f;
    const auto kernelb = torch::tensor(
        {{{{m, m, m}, {1.0f, 2.0f, 3.0f}, {m, m, m}},
          {{m, m, m}, {m, m, m}, {m, m, m}},
          {{m, m, m}, {m, m, m}, {m, m, m}}}},
        options);
    const auto stackb = torch::empty({4, 1, 3, 3, 3}, options);
    auto kernelb_a = kernelb.accessor<float, 4>();
    auto stackb_a = stackb.accessor<float, 5>();
    ::lietorch::m2::rotated_kernel_stack_trilinear_cpu<float>(
        kernelb_a, stackb_a, m);

    const auto kernelb_1 = torch::tensor(
        {{{{m, 1.0f, m}, {m, 2.0f, m}, {m, 3.0f, m}},
          {{m, m, m}, {m, m, m}, {m, m, m}},
          {{m, m, m}, {m, m, m}, {m, m, m}}}},
        options);

    const auto kernelb_2 = torch::tensor(
        {{{{m, m, m}, {3.0f, 2.0f, 1.0f}, {m, m, m}},
          {{m, m, m}, {m, m, m}, {m, m, m}},
          {{m, m, m}, {m, m, m}, {m, m, m}}}},
        options);

    const auto kernelb_3 = torch::tensor(
        {{{{m, 3.0f, m}, {m, 2.0f, m}, {m, 1.0f, m}},
          {{m, m, m}, {m, m, m}, {m, m, m}},
          {{m, m, m}, {m, m, m}, {m, m, m}}}},
        options);

    EXPECT_TRUE(kernelb.allclose(stackb[0], 1e-3));
    EXPECT_TRUE(kernelb_1.allclose(stackb[1], 1e-3));
    EXPECT_TRUE(kernelb_2.allclose(stackb[2], 1e-3));
    EXPECT_TRUE(kernelb_3.allclose(stackb[3], 1e-3));
}

TEST(M2, rotated_kernel_stack_nearest_cpu)
{
    const auto options = torch::TensorOptions().dtype(torch::kFloat64);

    auto f1 = torch::zeros({1, 1, 8, 9, 9}, options);
    f1[0][0][0][4][4] = 1.0;
    const auto kernel1 = torch::tensor(
        {{{{0, 0, 0}, {0, 0, 1}, {0, 0, 0}},
          {{0, 0, 0}, {0, 0, 2}, {0, 0, 0}},
          {{0, 0, 0}, {0, 0, 3}, {0, 0, 0}}}},
        options);

    const auto stack1 =
        ::lietorch::m2::rotated_kernel_stack_nearest_cpu(kernel1, 8);

    const auto kernel1_1 = torch::tensor(
        {{{{0, 0, 0}, {0, 0, 0}, {0, 0, 1}},
          {{0, 0, 0}, {0, 0, 0}, {0, 0, 2}},
          {{0, 0, 0}, {0, 0, 0}, {0, 0, 3}}}},
        options);

    const auto kernel1_2 = torch::tensor(
        {{{{0, 0, 0}, {0, 0, 0}, {0, 1, 0}},
          {{0, 0, 0}, {0, 0, 0}, {0, 2, 0}},
          {{0, 0, 0}, {0, 0, 0}, {0, 3, 0}}}},
        options);

    const auto kernel1_3 = torch::tensor(
        {{{{0, 0, 0}, {0, 0, 0}, {1, 0, 0}},
          {{0, 0, 0}, {0, 0, 0}, {2, 0, 0}},
          {{0, 0, 0}, {0, 0, 0}, {3, 0, 0}}}},
        options);

    EXPECT_TRUE(kernel1.allclose(stack1[0], 1e-3));
    EXPECT_TRUE(kernel1_1.allclose(stack1[1], 1e-3));
    EXPECT_TRUE(kernel1_2.allclose(stack1[2], 1e-3));
    EXPECT_TRUE(kernel1_3.allclose(stack1[3], 1e-3));
}

TEST(M2, rotated_kernel_stack_nearest_cuda)
{
    const auto options =
        torch::TensorOptions().dtype(torch::kFloat64).device(torch::kCUDA);

    auto f1 = torch::zeros({1, 1, 8, 9, 9}, options);
    f1[0][0][0][4][4] = 1.0;
    const auto kernel1 = torch::tensor(
        {{{{0, 0, 0}, {0, 0, 1}, {0, 0, 0}},
          {{0, 0, 0}, {0, 0, 2}, {0, 0, 0}},
          {{0, 0, 0}, {0, 0, 3}, {0, 0, 0}}}},
        options);

    auto stack1 = ::lietorch::m2::rotated_kernel_stack_nearest_cuda(kernel1, 8);

    const auto kernel1_1 = torch::tensor(
        {{{{0, 0, 0}, {0, 0, 0}, {0, 0, 1}},
          {{0, 0, 0}, {0, 0, 0}, {0, 0, 2}},
          {{0, 0, 0}, {0, 0, 0}, {0, 0, 3}}}},
        options);

    const auto kernel1_2 = torch::tensor(
        {{{{0, 0, 0}, {0, 0, 0}, {0, 1, 0}},
          {{0, 0, 0}, {0, 0, 0}, {0, 2, 0}},
          {{0, 0, 0}, {0, 0, 0}, {0, 3, 0}}}},
        options);

    const auto kernel1_3 = torch::tensor(
        {{{{0, 0, 0}, {0, 0, 0}, {1, 0, 0}},
          {{0, 0, 0}, {0, 0, 0}, {2, 0, 0}},
          {{0, 0, 0}, {0, 0, 0}, {3, 0, 0}}}},
        options);

    EXPECT_TRUE(kernel1.allclose(stack1[0], 1e-3));
    EXPECT_TRUE(kernel1_1.allclose(stack1[1], 1e-3));
    EXPECT_TRUE(kernel1_2.allclose(stack1[2], 1e-3));
    EXPECT_TRUE(kernel1_3.allclose(stack1[3], 1e-3));
}

TEST(M2, morphological_convolution_fw_cpu)
{
    const auto options = torch::TensorOptions().dtype(torch::kFloat64);
    const double m = 10.0;

    auto f1 = torch::zeros({1, 1, 8, 9, 9}, options);
    f1[0][0][0][4][4] = -1.0;
    const auto kernel1 = torch::tensor(
        {{{{m, m, m}, {m, 0.0, m}, {m, m, m}},
          {{m, m, m}, {0.0, 0.0, m}, {m, m, m}},
          {{m, m, m}, {m, 0.0, m}, {m, m, m}}}},
        options);

    const auto [out1_1, backindex1_1] =
        ::lietorch::m2::morphological_convolution_fw_cpu(f1, kernel1);
    const auto [out1_2, backindex1_2] =
        ::lietorch::m2::morphological_convolution_fw_cpu(out1_1, kernel1);
    const auto [out1_3, backindex1_3] =
        ::lietorch::m2::morphological_convolution_fw_cpu(out1_2, kernel1);
    const auto [out1_4, backindex1_4] =
        ::lietorch::m2::morphological_convolution_fw_cpu(out1_3, kernel1);

    // dbg(out1_4.abs());

    auto f2 = torch::zeros({1, 1, 4, 3, 3}, options);
    f2[0][0][0][1][1] = -1.0;
    const auto kernel2 = torch::tensor(
        {{{{m, m, m}, {m, 0.0, m}, {m, m, m}},
          {{m, m, m}, {0.0, 0.0, 0.0}, {m, m, m}},
          {{m, m, m}, {m, 0.0, m}, {m, m, m}}}},
        options);
    const auto [out2_1, backindex2_1] =
        ::lietorch::m2::morphological_convolution_fw_cpu(f2, kernel2);

    const auto out2_1_expected = torch::tensor(
        {{{{0, 0, 0}, {1, 1, 1}, {0, 0, 0}},
          {{0, 0, 0}, {0, 1, 0}, {0, 0, 0}},
          {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}},
          {{0, 0, 0}, {0, 1, 0}, {0, 0, 0}}}},
        options);

    EXPECT_TRUE(out2_1.abs().allclose(out2_1_expected, 1e-3));
}

TEST(M2, morphological_convolution_bw_cpu)
{
    const auto options = torch::TensorOptions().dtype(torch::kFloat64);
    const double m = 10.0;

    auto f1 = torch::zeros({1, 1, 8, 9, 9}, options);
    f1[0][0][0][4][4] = -1.0;
    const auto kernel1 = torch::tensor(
        {{{{m, m, m}, {m, 0.0, m}, {m, m, m}},
          {{m, m, m}, {0.0, 0.0, m}, {m, m, m}},
          {{m, m, m}, {m, 0.0, m}, {m, m, m}}}},
        options);

    const auto [out1, backindex1] =
        ::lietorch::m2::morphological_convolution_fw_cpu(f1, kernel1);
    const auto out1_grad = torch::randn_like(out1);

    const auto [f1_grad, kernel1_grad] =
        ::lietorch::m2::morphological_convolution_bw_cpu(
            out1_grad, backindex1, kernel1.sizes());

    EXPECT_TRUE(out1.sizes() == f1.sizes());
    EXPECT_TRUE(f1_grad.sizes() == f1.sizes());
    EXPECT_TRUE(kernel1_grad.sizes() == kernel1.sizes());

    const auto f2 = torch::randn({3, 2, 8, 9, 9}, options);
    const auto kernel2 = torch::randn({2, 4, 4, 4}, options);

    const auto [out2, backindex2] =
        ::lietorch::m2::morphological_convolution_fw_cpu(f2, kernel2);

    const auto out2_grad = torch::randn_like(out2);
    const auto [f2_grad, kernel2_grad] =
        ::lietorch::m2::morphological_convolution_bw_cpu(
            out2_grad, backindex2, kernel2.sizes());

    EXPECT_TRUE(out2.sizes() == f2.sizes());
    EXPECT_TRUE(f2_grad.sizes() == f2.sizes());
    EXPECT_TRUE(kernel2_grad.sizes() == kernel2.sizes());
}

TEST(M2, morphological_convolution_fw_compare)
{
    const auto options =
        torch::TensorOptions().dtype(torch::kFloat32).device(torch::kCPU);

    const auto input = torch::randn({2, 3, 8, 9, 9}, options);
    const auto kernel = torch::randn({3, 5, 5, 5}, options);

    const auto kstack_cpu =
        ::lietorch::m2::rotated_kernel_stack_nearest_cpu(kernel, 8);

    const auto kstack_cuda =
        ::lietorch::m2::rotated_kernel_stack_nearest_cuda(kernel.cuda(), 8);

    EXPECT_TRUE(kstack_cpu.allclose(kstack_cuda.cpu(), 1e-4, 1e-5));

    const auto [out_cpu, backindex_cpu] =
        ::lietorch::m2::morphological_convolution_fw(input, kernel);
    const auto [out_cuda, backindex_cuda] =
        ::lietorch::m2::morphological_convolution_fw(
            input.cuda(), kernel.cuda());

    EXPECT_TRUE(out_cpu.allclose(out_cuda.cpu(), 1e-4, 1e-5));

    const auto input2 = torch::randn({3, 2, 9, 10, 10}, options);
    const auto kernel2 = torch::randn({2, 4, 2, 2}, options);
    const auto [out2_cpu, backindex2_cpu] =
        ::lietorch::m2::morphological_convolution_fw(input2, kernel2);
    const auto [out2_cuda, backindex2_cuda] =
        ::lietorch::m2::morphological_convolution_fw(
            input2.cuda(), kernel2.cuda());

    EXPECT_TRUE(out2_cpu.allclose(out2_cuda.cpu(), 1e-4, 1e-5));
}

TEST(M2, morphological_convolution_bw_compare)
{
    const auto options = torch::TensorOptions()
                             .dtype(torch::kFloat64)
                             .device(torch::kCPU)
                             .requires_grad(true);

    const auto input = torch::randn({2, 3, 8, 9, 9}, options);
    const auto kernel = torch::randn({3, 4, 4, 4}, options);

    const auto kstack_cpu =
        ::lietorch::m2::rotated_kernel_stack_nearest_cpu(kernel, 8);

    const auto kstack_cuda =
        ::lietorch::m2::rotated_kernel_stack_nearest_cuda(kernel.cuda(), 8);

    EXPECT_TRUE(
        ((kstack_cpu - kstack_cuda.cpu()).abs() > 1e-3).sum().item().toInt() <
        50);

    const auto [out_cpu, backindex_cpu] =
        ::lietorch::m2::morphological_convolution_fw(input, kernel);
    const auto [out_cuda, backindex_cuda] =
        ::lietorch::m2::morphological_convolution_fw(
            input.cuda(), kernel.cuda());

    EXPECT_TRUE(
        ((out_cpu - out_cuda.cpu()).abs() > 1e-3).sum().item().toInt() < 500);

    const auto grad = torch::randn_like(input);
    const auto [input_grad_cpu, kernel_grad_cpu] =
        ::lietorch::m2::morphological_convolution_bw(
            grad, backindex_cpu, kernel.sizes());

    EXPECT_EQ(input.sizes(), input_grad_cpu.sizes());
    EXPECT_EQ(kernel.sizes(), kernel_grad_cpu.sizes());

    const auto [input_grad_cuda, kernel_grad_cuda] =
        ::lietorch::m2::morphological_convolution_bw(
            grad.cuda(), backindex_cuda, kernel.sizes());

    EXPECT_EQ(input.sizes(), input_grad_cuda.sizes());
    EXPECT_EQ(kernel.sizes(), kernel_grad_cuda.sizes());

    EXPECT_LT(
        ((input_grad_cpu - input_grad_cuda.cpu()).abs() > 1e-3)
            .sum()
            .item()
            .toInt(),
        300);
    EXPECT_LT(
        ((kernel_grad_cpu - kernel_grad_cuda.cpu()).abs() > 1e-3)
            .sum()
            .item()
            .toInt(),
        80);
}

TEST(M2, logarithmic_metric_estimate)
{
    const auto options =
        torch::TensorOptions().dtype(torch::kFloat64).device(torch::kCPU);

    const auto params = torch::rand({2, 2, 3}, options);
    const auto metric =
        ::lietorch::m2::logarithmic_metric_estimate(params, {5, 5, 5}, 5);

    EXPECT_EQ(metric.sizes(), torch::IntArrayRef({2, 2, 5, 5, 5}));
    EXPECT_EQ(params.requires_grad(), metric.requires_grad());
    EXPECT_EQ(params.scalar_type(), metric.scalar_type());

    params.requires_grad_(true);
    const auto metric2 =
        ::lietorch::m2::logarithmic_metric_estimate(params, {5, 5, 5}, 5);

    EXPECT_TRUE(metric.allclose(metric2));
    EXPECT_EQ(params.requires_grad(), metric2.requires_grad());
}

// We test logarithmic_metric_estimate_nondiag by comparing it to the 
// , assumed to be correct, logarithmic_metric_estimate by only testing
// diagional metrics.
TEST(M2, logarithmic_metric_estimate_nondiag)
{
    const auto options =
        torch::TensorOptions().dtype(torch::kFloat64).device(torch::kCPU);

    const int channels = 3;
    const int64_t kernel_sizes_a[3] = {6,5,4};
    const auto kernel_sizes = torch::IntArrayRef(kernel_sizes_a);
    const double t_scale = 5.0;

    // std::cout << "kernel_sizes" << std::endl;
    // std::cout << kernel_sizes << std::endl;

    const auto params = torch::rand({channels, 3}, options);
    const auto params2 = params.pow(2);
    const auto params_as_diag = torch::diag_embed(params);

    // std::cout << "params:" << std::endl;
    // std::cout << params << std::endl;

    // std::cout << "params2:" << std::endl;
    // std::cout << params2 << std::endl;

    // std::cout << "params_as_diag:" << std::endl;
    // std::cout << params_as_diag << std::endl;

    const auto metric1 =
        ::lietorch::m2::logarithmic_metric_estimate(params2, kernel_sizes, t_scale);

    const auto metric2 =
        ::lietorch::m2::logarithmic_metric_estimate_nondiag(params_as_diag, kernel_sizes, t_scale);

    // std::cout << "metric1" << std::endl;
    // std::cout << metric1 << std::endl;

    // std::cout << "metric2" << std::endl;
    // std::cout << metric2 << std::endl;

    EXPECT_TRUE(metric1.allclose(metric2));
}

TEST(M2, morphological_kernel)
{
    const auto options = torch::TensorOptions()
                             .dtype(torch::kFloat64)
                             .device(torch::kCPU)
                             .requires_grad(true);

    const auto params = torch::tensor({{1.0, 1.0, 0.5}}, options);
    const auto kernel =
        ::lietorch::m2::morphological_kernel(params, {5, 5, 5}, 8, 1.0);
    const auto kernel_cuda =
        ::lietorch::m2::morphological_kernel(params.cuda(), {5, 5, 5}, 8, 1.0);

    EXPECT_EQ(kernel.sizes(), torch::IntArrayRef({1, 5, 5, 5}));
    EXPECT_TRUE(kernel_cuda.is_cuda());
    EXPECT_TRUE(kernel.allclose(kernel_cuda.cpu()));
    EXPECT_EQ(params.requires_grad(), kernel_cuda.requires_grad());
}

TEST(M2, diffusion_kernel)
{
    const auto options = torch::TensorOptions()
                             .dtype(torch::kFloat64)
                             .device(torch::kCPU)
                             .requires_grad(true);

    const auto params = torch::tensor({{1.0, 1.0, 0.5}}, options);
    const auto kernel = ::lietorch::m2::diffusion_kernel(params, {5, 5, 5}, 8);
    const auto kernel_cuda =
        ::lietorch::m2::diffusion_kernel(params.cuda(), {5, 5, 5}, 8);

    EXPECT_EQ(kernel.sizes(), torch::IntArrayRef({1, 5, 5, 5}));
    EXPECT_TRUE(kernel_cuda.is_cuda());
    EXPECT_TRUE(kernel.allclose(kernel_cuda.cpu()));
    EXPECT_EQ(params.requires_grad(), kernel_cuda.requires_grad());
}