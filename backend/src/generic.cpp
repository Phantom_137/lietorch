/** @file generic.cpp
 *  @brief Generic functions mainly used as examples of how to implement new
 * ops.
 */

#include "generic.h"

#include "cpu/generic_cpu.h"

#ifdef WITH_CUDA
#include "cuda/generic_cuda.cuh"
#endif

namespace lietorch
{
namespace generic
{

/**
 *  @brief Add two tensors of the same shape and type.
 */
torch::Tensor
add_fw(const torch::Tensor& a, const torch::Tensor& b)
{
    auto a_arg = torch::TensorArg(a, "a", 0),
         b_arg = torch::TensorArg(b, "b", 1);

    torch::checkAllDefined(__func__, {a_arg, b_arg});

    if (a.is_cuda())
    {
#if defined(WITH_CUDA)
        torch::checkAllSameGPU(__func__, {a_arg, b_arg});
        return add_fw_cuda(a, b);
#else
        TORCH_CHECK(false, "Not compiled with CUDA support");
#endif
    }
    else
    {
        return add_fw_cpu(a, b);
    }
}

/**
 *  @brief Backpropagation of add_fw.
 */
std::tuple<torch::Tensor, torch::Tensor>
add_bw(const torch::Tensor& grad)
{
    auto grad_arg = torch::TensorArg(grad, "grad", 0);

    torch::checkDefined(__func__, grad_arg);

    if (grad.is_cuda())
    {
#if defined(WITH_CUDA)
        return add_bw_cuda(grad);
#else
        TORCH_CHECK(false, "Not compiled with CUDA support");
#endif
    }
    else
    {
        return add_bw_cpu(grad);
    }
}

/**
 *  @brief Dilation of 2D grayscale image.
 */
std::tuple<torch::Tensor, torch::Tensor>
grayscale_dilation_2d_fw(
    const torch::Tensor& image, const torch::Tensor& filter)
{
    auto image_arg = torch::TensorArg(image, "image", 1),
         filter_arg = torch::TensorArg(filter, "filter", 2);

    torch::checkAllDefined(__func__, {image_arg, filter_arg});
    torch::checkAllContiguous(__func__, {image_arg, filter_arg});
    torch::checkAllSameType(__func__, {image_arg, filter_arg});
    torch::checkDim(__func__, image_arg, 2);
    torch::checkDim(__func__, filter_arg, 2);

    if (image.is_cuda())
    {
#if defined(WITH_CUDA)
        torch::checkAllSameGPU(__func__, {image_arg, filter_arg});
        return grayscale_dilation_2d_fw_cuda(image, filter);
#else
        TORCH_CHECK(false, "Not compiled with CUDA support");
#endif
    }
    else
    {
        return grayscale_dilation_2d_fw_cpu(image, filter);
    }
}

/**
 *  @brief Backpropagation of lietorch::generic::grayscale_dilation_2d_fw .
 */
std::tuple<torch::Tensor, torch::Tensor>
grayscale_dilation_2d_bw(
    const torch::Tensor& backindex,
    const torch::Tensor& grad,
    const int64_t filter_h,
    const int64_t filter_w)
{
    auto backindex_arg = torch::TensorArg(backindex, "backindex", 1);
    auto grad_arg = torch::TensorArg(grad, "grad", 2);

    torch::checkAllDefined(__func__, {backindex_arg, grad_arg});
    torch::checkAllContiguous(__func__, {backindex_arg, grad_arg});
    torch::checkDim(__func__, backindex_arg, 3);
    torch::checkDim(__func__, grad_arg, 2);
    torch::checkScalarType(__func__, backindex_arg, torch::kInt64);

    TORCH_CHECK(
        filter_h > 0 && filter_w > 0,
        "filter_h and filter_w need to be positive integers");

    if (grad.is_cuda())
    {
#if defined(WITH_CUDA)
        torch::checkAllSameGPU(__func__, {backindex_arg, grad_arg});
        return grayscale_dilation_2d_bw_cuda(
            backindex, grad, filter_h, filter_w);
#else
        TORCH_CHECK(false, "Not compiled with CUDA support");
#endif
    }
    else
    {
        return grayscale_dilation_2d_bw_cpu(
            backindex, grad, filter_h, filter_w);
    }
}

/**
 *  @brief Erosion of 2D grayscale image.
 *
 */
std::tuple<torch::Tensor, torch::Tensor>
grayscale_erosion_2d_fw(const torch::Tensor& image, const torch::Tensor& filter)
{
    auto image_arg = torch::TensorArg(image, "image", 0),
         filter_arg = torch::TensorArg(filter, "filter", 1);

    torch::checkAllDefined(__func__, {image_arg, filter_arg});
    torch::checkAllContiguous(__func__, {image_arg, filter_arg});
    torch::checkAllSameType(__func__, {image_arg, filter_arg});
    torch::checkDim(__func__, image_arg, 2);
    torch::checkDim(__func__, filter_arg, 2);

    if (image.is_cuda())
    {
#if defined(WITH_CUDA)
        torch::checkAllSameGPU(__func__, {image_arg, filter_arg});
        return grayscale_erosion_2d_fw_cuda(image, filter);
#else
        TORCH_CHECK(false, "Not compiled with CUDA support");
#endif
    }
    else
    {
        return grayscale_erosion_2d_fw_cpu(image, filter);
    }
}

/**
 *  @brief Backpropagation of lietorch::generic::grayscale_erosion_2d_fw .
 */
std::tuple<torch::Tensor, torch::Tensor>
grayscale_erosion_2d_bw(
    const torch::Tensor& backindex,
    const torch::Tensor& grad,
    const int64_t filter_h,
    const int64_t filter_w)
{
    auto backindex_arg = torch::TensorArg(backindex, "backindex", 0);
    auto grad_arg = torch::TensorArg(grad, "grad", 1);

    torch::checkAllDefined(__func__, {backindex_arg, grad_arg});
    torch::checkAllContiguous(__func__, {backindex_arg, grad_arg});
    torch::checkDim(__func__, backindex_arg, 3);
    torch::checkDim(__func__, grad_arg, 2);
    torch::checkScalarType(__func__, backindex_arg, torch::kInt64);

    TORCH_CHECK(
        filter_h > 0 && filter_w > 0,
        "filter_h and filter_w need to be positive integers");

    if (grad.is_cuda())
    {
#if defined(WITH_CUDA)
        torch::checkAllSameGPU(__func__, {backindex_arg, grad_arg});
        return grayscale_erosion_2d_bw_cuda(
            backindex, grad, filter_h, filter_w);
#else
        TORCH_CHECK(false, "Not compiled with CUDA support");
#endif
    }
    else
    {
        return grayscale_erosion_2d_bw_cpu(backindex, grad, filter_h, filter_w);
    }
}

variable_list
GrayscaleDilation2D::forward(
    AutogradContext* ctx, Variable image, Variable filter)
{
    const auto [out, backindex] =
        grayscale_dilation_2d_fw(image.contiguous(), filter.contiguous());

    ctx->save_for_backward({backindex});
    ctx->saved_data["filter_h"] = filter.size(0);
    ctx->saved_data["filter_w"] = filter.size(1);

    return {out};
}

variable_list
GrayscaleDilation2D::backward(AutogradContext* ctx, variable_list grads)
{
    const auto grad = grads[0];
    const auto backindex = ctx->get_saved_variables()[0];
    const auto filter_h = ctx->saved_data["filter_h"].toInt();
    const auto filter_w = ctx->saved_data["filter_w"].toInt();

    const auto [image_grad, filter_grad] =
        grayscale_dilation_2d_bw(backindex, grad, filter_h, filter_w);

    return {image_grad, filter_grad};
}

torch::Tensor
grayscale_dilation_2d(const torch::Tensor& image, const torch::Tensor& filter)
{
    return GrayscaleDilation2D::apply(image, filter)[0];
}

variable_list
GrayscaleErosion2D::forward(
    AutogradContext* ctx, Variable image, Variable filter)
{
    const auto [out, backindex] =
        grayscale_erosion_2d_fw(image.contiguous(), filter.contiguous());

    ctx->save_for_backward({backindex});
    ctx->saved_data["filter_h"] = filter.size(0);
    ctx->saved_data["filter_w"] = filter.size(1);

    return {out};
}

variable_list
GrayscaleErosion2D::backward(AutogradContext* ctx, variable_list grads)
{
    const auto grad = grads[0];
    const auto backindex = ctx->get_saved_variables()[0];
    const auto filter_h = ctx->saved_data["filter_h"].toInt();
    const auto filter_w = ctx->saved_data["filter_w"].toInt();

    const auto [image_grad, filter_grad] =
        grayscale_erosion_2d_bw(backindex, grad, filter_h, filter_w);

    return {image_grad, filter_grad};
}

torch::Tensor
grayscale_erosion_2d(const torch::Tensor& image, const torch::Tensor& filter)
{
    return GrayscaleErosion2D::apply(image, filter)[0];
}

} // namespace generic
} // namespace lietorch
