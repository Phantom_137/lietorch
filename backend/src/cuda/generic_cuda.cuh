#pragma once
#ifndef LIETORCH_CUDA_GENERIC_CUDA_H
#define LIETORCH_CUDA_GENERIC_CUDA_H
/**
 * @file generic_cuda.cuh
 *
 *
 */

#include "../torch_include.h"

#include <vector>

namespace lietorch
{
namespace generic
{

// Add
torch::Tensor
add_fw_cuda(const torch::Tensor& a, const torch::Tensor& b);

std::tuple<torch::Tensor, torch::Tensor>
add_bw_cuda(const torch::Tensor& grad);

// Grayscale dilation 2D
std::tuple<torch::Tensor, torch::Tensor>
grayscale_dilation_2d_fw_cuda(
    const torch::Tensor& image, const torch::Tensor& filter);

std::tuple<torch::Tensor, torch::Tensor>
grayscale_dilation_2d_bw_cuda(
    const torch::Tensor& backindex,
    const torch::Tensor& grad,
    const int64_t filter_h,
    const int64_t filter_w);

// Grayscale erosion 2D
std::tuple<torch::Tensor, torch::Tensor>
grayscale_erosion_2d_fw_cuda(
    const torch::Tensor& image, const torch::Tensor& filter);

std::tuple<torch::Tensor, torch::Tensor>
grayscale_erosion_2d_bw_cuda(
    const torch::Tensor& backindex,
    const torch::Tensor& grad,
    const int64_t filter_h,
    const int64_t filter_w);

} // namespace generic
} // namespace lietorch

#endif