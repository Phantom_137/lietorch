
#include "../src/torch_include.h"
#include "gtest/gtest.h"

#include "../src/cuda/dispatch.cuh"
#include "../src/cuda/math_cuda.cuh"
#include "../src/m2_element.h"

using namespace ::lietorch;

namespace
{

template <typename scalar_t>
__global__ void
lt_cu_cossinpi_kernel(
    const torch::PackedTensorAccessor32<scalar_t, 1, torch::RestrictPtrTraits>
        x_a,
    torch::PackedTensorAccessor32<scalar_t, 1, torch::RestrictPtrTraits>
        out_cos_x_a,
    torch::PackedTensorAccessor32<scalar_t, 1, torch::RestrictPtrTraits>
        out_sin_x_a)
{
    const int32_t thread_idx = linear_idx();

    const auto cossin = lt_cu_cossinpi<scalar_t>(x_a[thread_idx]);
    const scalar_t cos = std::get<0>(cossin);
    const scalar_t sin = std::get<1>(cossin);

    out_cos_x_a[thread_idx] = cos;
    out_sin_x_a[thread_idx] = sin;
}

template <typename scalar_t>
__global__ void
lt_cu_cossin_kernel(
    const torch::PackedTensorAccessor32<scalar_t, 1, torch::RestrictPtrTraits>
        x_a,
    torch::PackedTensorAccessor32<scalar_t, 1, torch::RestrictPtrTraits>
        out_cos_x_a,
    torch::PackedTensorAccessor32<scalar_t, 1, torch::RestrictPtrTraits>
        out_sin_x_a)
{
    const int32_t thread_idx = linear_idx();

    const auto cossin = lt_cu_cossin<scalar_t>(x_a[thread_idx]);
    const scalar_t cos = std::get<0>(cossin);
    const scalar_t sin = std::get<1>(cossin);

    out_cos_x_a[thread_idx] = cos;
    out_sin_x_a[thread_idx] = sin;
}

template <typename scalar_t>
__global__ void
lt_cu_intfrac_kernel(
    const torch::PackedTensorAccessor32<scalar_t, 1, torch::RestrictPtrTraits>
        x_a,
    torch::PackedTensorAccessor32<scalar_t, 1, torch::RestrictPtrTraits>
        out_int_x_a,
    torch::PackedTensorAccessor32<scalar_t, 1, torch::RestrictPtrTraits>
        out_frac_x_a)
{
    const int32_t thread_idx = linear_idx();

    const auto intfrac = lt_cu_intfrac<scalar_t>(x_a[thread_idx]);
    const scalar_t inte = std::get<0>(intfrac);
    const scalar_t frac = std::get<1>(intfrac);

    out_int_x_a[thread_idx] = inte;
    out_frac_x_a[thread_idx] = frac;
}

template <typename scalar_t>
__global__ void
lt_cu_fmod_kernel(
    const torch::PackedTensorAccessor32<scalar_t, 1, torch::RestrictPtrTraits>
        x1_a,
    const torch::PackedTensorAccessor32<scalar_t, 1, torch::RestrictPtrTraits>
        x2_a,
    torch::PackedTensorAccessor32<scalar_t, 1, torch::RestrictPtrTraits> out_a)
{
    const int32_t thread_idx = linear_idx();

    out_a[thread_idx] =
        lt_cu_fmod<scalar_t>(x1_a[thread_idx], x2_a[thread_idx]);
}

} // namespace

TEST(MATH_CUDA, lt_cu_cossinpi)
{

    const auto options =
        torch::TensorOptions().dtype(torch::kFloat32).device(torch::kCUDA);
    const auto x = torch::tensor(
        {-0.2, -0.1, 0.0, 0.1, 0.5, 1.0, 2.0, 2.1, 2.2, 2.3, 2.4, 2.5},
        options);
    const auto cos_x = (x * M_PI).cos();
    const auto sin_x = (x * M_PI).sin();

    auto out_cos_x = torch::zeros_like(x);
    auto out_sin_x = torch::zeros_like(x);

    auto x_a = x.packed_accessor32<float, 1, torch::RestrictPtrTraits>();
    auto out_cos_x_a =
        out_cos_x.packed_accessor32<float, 1, torch::RestrictPtrTraits>();
    auto out_sin_x_a =
        out_sin_x.packed_accessor32<float, 1, torch::RestrictPtrTraits>();

    void* args[] = {&x_a, &out_cos_x_a, &out_sin_x_a};

    CUDA_CALL(cudaLaunchKernel(
        (void*)lt_cu_cossinpi_kernel<float>, blocks(x), threads(x), args));
    CUDA_CALL(cudaDeviceSynchronize());

    EXPECT_TRUE(cos_x.allclose(out_cos_x, 1e-3, 1e-5));
    EXPECT_TRUE(sin_x.allclose(out_sin_x, 1e-3, 1e-5));

    const auto y = x.to(torch::kFloat64);
    const auto cos_y = (y * M_PI).cos();
    const auto sin_y = (y * M_PI).sin();

    auto out_cos_y = torch::zeros_like(y);
    auto out_sin_y = torch::zeros_like(y);

    auto y_a = y.packed_accessor32<double, 1, torch::RestrictPtrTraits>();
    auto out_cos_y_a =
        out_cos_y.packed_accessor32<double, 1, torch::RestrictPtrTraits>();
    auto out_sin_y_a =
        out_sin_y.packed_accessor32<double, 1, torch::RestrictPtrTraits>();

    void* args2[] = {&y_a, &out_cos_y_a, &out_sin_y_a};

    CUDA_CALL(cudaLaunchKernel(
        (void*)lt_cu_cossinpi_kernel<double>, blocks(y), threads(y), args2));
    CUDA_CALL(cudaDeviceSynchronize());

    EXPECT_TRUE(cos_y.allclose(out_cos_y, 1e-3, 1e-5));
    EXPECT_TRUE(sin_y.allclose(out_sin_y, 1e-3, 1e-5));
}

TEST(MATH_CUDA, lt_cu_cossin)
{

    const auto options =
        torch::TensorOptions().dtype(torch::kFloat32).device(torch::kCUDA);
    const auto x = torch::tensor(
        {-0.2, -0.1, 0.0, 0.1, 0.5, 1.0, 2.0, 2.1, 2.2, 2.3, 2.4, 2.5},
        options);
    const auto cos_x = x.cos();
    const auto sin_x = x.sin();

    auto out_cos_x = torch::zeros_like(x);
    auto out_sin_x = torch::zeros_like(x);

    auto x_a = x.packed_accessor32<float, 1, torch::RestrictPtrTraits>();
    auto out_cos_x_a =
        out_cos_x.packed_accessor32<float, 1, torch::RestrictPtrTraits>();
    auto out_sin_x_a =
        out_sin_x.packed_accessor32<float, 1, torch::RestrictPtrTraits>();

    void* args[] = {&x_a, &out_cos_x_a, &out_sin_x_a};

    CUDA_CALL(cudaLaunchKernel(
        (void*)lt_cu_cossin_kernel<float>, blocks(x), threads(x), args));
    CUDA_CALL(cudaDeviceSynchronize());

    EXPECT_TRUE(cos_x.allclose(out_cos_x, 1e-3, 1e-5));
    EXPECT_TRUE(sin_x.allclose(out_sin_x, 1e-3, 1e-5));

    const auto y = x.to(torch::kFloat64);
    const auto cos_y = y.cos();
    const auto sin_y = y.sin();

    auto out_cos_y = torch::zeros_like(y);
    auto out_sin_y = torch::zeros_like(y);

    auto y_a = y.packed_accessor32<double, 1, torch::RestrictPtrTraits>();
    auto out_cos_y_a =
        out_cos_y.packed_accessor32<double, 1, torch::RestrictPtrTraits>();
    auto out_sin_y_a =
        out_sin_y.packed_accessor32<double, 1, torch::RestrictPtrTraits>();

    void* args2[] = {&y_a, &out_cos_y_a, &out_sin_y_a};

    CUDA_CALL(cudaLaunchKernel(
        (void*)lt_cu_cossin_kernel<double>, blocks(y), threads(y), args2));
    CUDA_CALL(cudaDeviceSynchronize());

    EXPECT_TRUE(cos_y.allclose(out_cos_y, 1e-3, 1e-5));
    EXPECT_TRUE(sin_y.allclose(out_sin_y, 1e-3, 1e-5));
}

TEST(MATH_CUDA, lt_cu_intfrac)
{
    const auto options =
        torch::TensorOptions().dtype(torch::kFloat32).device(torch::kCUDA);
    const auto x =
        torch::tensor({-1.1, -1.0, -0.5, 0.0, 0.5, 1.0, 1.6}, options);
    const auto x_frac =
        torch::tensor({0.9, 0.0, 0.5, 0.0, 0.5, 0.0, 0.6}, options);
    const auto x_int =
        torch::tensor({-2.0, -1.0, -1.0, 0.0, 0.0, 1.0, 1.0}, options);

    const auto x_frac_out = torch::zeros_like(x);
    const auto x_int_out = torch::zeros_like(x);

    auto x_a = x.packed_accessor32<float, 1, torch::RestrictPtrTraits>();
    auto x_frac_out_a =
        x_frac_out.packed_accessor32<float, 1, torch::RestrictPtrTraits>();
    auto x_int_out_a =
        x_int_out.packed_accessor32<float, 1, torch::RestrictPtrTraits>();

    void* args1[] = {&x_a, &x_int_out_a, &x_frac_out_a};

    CUDA_CALL(cudaLaunchKernel(
        (void*)lt_cu_intfrac_kernel<float>, blocks(x), threads(x), args1));
    CUDA_CALL(cudaDeviceSynchronize());

    EXPECT_TRUE(x_frac.allclose(x_frac_out));
    EXPECT_TRUE(x_int.allclose(x_int_out));

    const auto y = x.to(torch::kFloat64);
    const auto y_frac = x_frac.to(torch::kFloat64);
    const auto y_int = x_int.to(torch::kFloat64);

    const auto y_int_out = torch::zeros_like(y);
    const auto y_frac_out = torch::zeros_like(y);

    auto y_a = y.packed_accessor32<double, 1, torch::RestrictPtrTraits>();
    auto y_frac_out_a =
        y_frac_out.packed_accessor32<double, 1, torch::RestrictPtrTraits>();
    auto y_int_out_a =
        y_int_out.packed_accessor32<double, 1, torch::RestrictPtrTraits>();

    void* args2[] = {&y_a, &y_int_out_a, &y_frac_out_a};

    CUDA_CALL(cudaLaunchKernel(
        (void*)lt_cu_intfrac_kernel<double>, blocks(x), threads(x), args2));
    CUDA_CALL(cudaDeviceSynchronize());

    EXPECT_TRUE(y_frac.allclose(y_frac_out));
    EXPECT_TRUE(y_int.allclose(y_int_out));
}

TEST(MATH_CUDA, lt_cu_fmod)
{
    const auto options =
        torch::TensorOptions().dtype(torch::kFloat32).device(torch::kCUDA);
    const auto x1 = 10 * torch::randn({100}, options);
    const auto x2 = torch::randn({100}, options);

    const auto modx = x1.fmod(x2);

    auto modx_out = torch::zeros_like(x1);

    auto x1_a = x1.packed_accessor32<float, 1, torch::RestrictPtrTraits>();
    auto x2_a = x2.packed_accessor32<float, 1, torch::RestrictPtrTraits>();
    auto modx_out_a =
        modx_out.packed_accessor32<float, 1, torch::RestrictPtrTraits>();

    void* args1[] = {&x1_a, &x2_a, &modx_out_a};

    CUDA_CALL(cudaLaunchKernel(
        (void*)lt_cu_fmod_kernel<float>, blocks(x1), threads(x1), args1));
    CUDA_CALL(cudaDeviceSynchronize());

    EXPECT_TRUE(modx.allclose(modx_out));

    auto y1 = x1.to(torch::kFloat64);
    auto y2 = x2.to(torch::kFloat64);

    const auto mody = y1.fmod(y2);

    auto mody_out = torch::zeros_like(y1);

    auto y1_a = y1.packed_accessor32<double, 1, torch::RestrictPtrTraits>();
    auto y2_a = y2.packed_accessor32<double, 1, torch::RestrictPtrTraits>();
    auto mody_out_a =
        mody_out.packed_accessor32<double, 1, torch::RestrictPtrTraits>();

    void* args2[] = {&y1_a, &y2_a, &mody_out_a};

    CUDA_CALL(cudaLaunchKernel(
        (void*)lt_cu_fmod_kernel<double>, blocks(y1), threads(y1), args2));
    CUDA_CALL(cudaDeviceSynchronize());

    EXPECT_TRUE(mody.allclose(mody_out));
}

namespace
{

__global__ void
element_cuda_kernel(int* d_status)
{
    using elem = ::lietorch::m2::element<double>;
    int n = 0;
    *d_status = -2;

#define LT_EXPECT_TRUE(X)                                                      \
    n++;                                                                       \
    if (!(X))                                                                  \
    {                                                                          \
        *d_status = n;                                                         \
        return;                                                                \
    }

    const elem e(0.0, 0.0, 0.0);
    LT_EXPECT_TRUE(e == e.inv());
    LT_EXPECT_TRUE(e == elem::e());
    LT_EXPECT_TRUE(elem::e().inv() == elem::e());

    const elem g1(0.0, 0.0, 1.0);
    LT_EXPECT_TRUE(g1.inv() == elem(0.0, 0.0, -1.0));
    const elem g2(0.0, 1.0, 0.0);
    LT_EXPECT_TRUE(g2.inv() == elem(0.0, -1.0, 0.0));
    const elem g3(1.0, 0.0, 0.0);
    LT_EXPECT_TRUE(g3.inv() == elem(-1.0, 0.0, 0.0));

    // EXPECT_THROW(elem(1.0, 1.0, 1.0, 0.0), std::invalid_argument);

    const elem g4(M_PI / 2.0, 1.0, 1.5);
    const elem g5(2.0, 1.0, 1.5, 8.0);
    LT_EXPECT_TRUE(g4.close_to(g5));
    LT_EXPECT_TRUE(g4.inv().close_to(g5.inv()));
    LT_EXPECT_TRUE(g4.inv().close_to(elem(-M_PI / 2.0, 1.5, -1.0)));

    LT_EXPECT_TRUE(e.close_to(g4 * g4.inv()));
    LT_EXPECT_TRUE(e.close_to(g4 * g5.inv()));
    LT_EXPECT_TRUE(e.close_to(g5 * g4.inv()));

    const elem g6(1.0, 0.0, 1.0, 3.0);
    LT_EXPECT_TRUE((g6 * g6 * g6).close_to(e));

    const elem g7(1.0, 0.1, 0.5, 8.0);
    const elem g8(0.5, 0.1, 0.5, 4.0);
    LT_EXPECT_TRUE(g7.close_to(g8));
    LT_EXPECT_TRUE((g7 * g8).t_scale == 8.0);
    LT_EXPECT_TRUE((g4 * g7).close_to(g5 * g8));

    LT_EXPECT_TRUE(
        elem(1.0, 1.0, 1.0).close_to(elem(1.0 - 2 * M_PI, 1.0, 1.0)));
    LT_EXPECT_TRUE(
        elem(1.0, 1.0, 1.0).close_to(elem(1.0 + 2 * M_PI, 1.0, 1.0)));
    LT_EXPECT_TRUE(elem(M_PI, 1.0, 1.0).close_to(elem(-4.0, 1.0, 1.0, 8.0)));
    LT_EXPECT_TRUE(elem(M_PI, 1.0, 1.0).close_to(elem(4.0, 1.0, 1.0, 8.0)));

    const elem g9(1.0, M_SQRT1_2, M_SQRT1_2, 8.0);
    const elem g10(-2.5, 0.5, 1.5, 10.0);
    const elem g11(-1.0, 2.12132, 1.41421, 8.0);
    LT_EXPECT_TRUE(g11.close_to(g9 * g10));
    LT_EXPECT_TRUE((g9 * g10 * g11.inv()).close_to(e));
    LT_EXPECT_TRUE((g11 * (g9 * g10).inv()).close_to(e));

    *d_status = 0;
}

} // namespace

TEST(M2, element_cuda)
{
    int h_status = -1;
    int* d_status;
    CUDA_CALL(cudaMalloc(&d_status, sizeof(int)));
    CUDA_CALL(cudaDeviceSynchronize());

    void* args[] = {&d_status};

    CUDA_CALL(cudaLaunchKernel((void*)element_cuda_kernel, 1, 1, args));
    CUDA_CALL(cudaDeviceSynchronize());
    CUDA_CALL(
        cudaMemcpy(&h_status, d_status, sizeof(int), cudaMemcpyDeviceToHost));
    CUDA_CALL(cudaDeviceSynchronize());

    EXPECT_EQ(h_status, 0);
}

#undef LT_EXPECT_TRUE