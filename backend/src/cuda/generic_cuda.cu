#include "generic_cuda.cuh"

#pragma warning(push, 1)
#include <ATen/ATen.h>
#include <ATen/cuda/NumericLimits.cuh>
#pragma warning(pop)

#include "atomics.cuh"
#include "dispatch.cuh"
#include <cuda_runtime.h>
#include <device_launch_parameters.h>

namespace // internal namespace
{

using namespace lietorch;

// Addition CUDA kernel
template <typename scalar_t>
__global__ void
add_fw_cuda_kernel(
    const scalar_t* __restrict__ a,
    const scalar_t* __restrict__ b,
    scalar_t* __restrict__ out,
    const int64_t numel)
{
    const int64_t thread_idx = blockIdx.x * blockDim.x + threadIdx.x;

    if (thread_idx < numel)
    {
        out[thread_idx] = a[thread_idx] + b[thread_idx];
    }
}

// Forward 2D grayscale dilation kernel
template <typename scalar_t>
__global__ void
grayscale_dilation_2d_fw_kernel(
    const torch::PackedTensorAccessor32<scalar_t, 2, torch::RestrictPtrTraits>
        image_a,
    const torch::PackedTensorAccessor32<scalar_t, 2, torch::RestrictPtrTraits>
        filter_a,
    torch::PackedTensorAccessor32<scalar_t, 2, torch::RestrictPtrTraits> out_a,
    torch::PackedTensorAccessor32<int64_t, 3, torch::RestrictPtrTraits>
        backindex_a)
{
    const uint32_t h = image_a.size(0);
    const uint32_t w = image_a.size(1);
    const uint32_t fh = filter_a.size(0);
    const uint32_t fw = filter_a.size(1);

    const uint32_t f_top = (fh - 1) / 2 + (fh - 1) % 2;
    const uint32_t f_bottom = (fh - 1) / 2;
    const uint32_t f_left = (fw - 1) / 2 + (fw - 1) % 2;
    const uint32_t f_right = (fw - 1) / 2;

    const uint32_t thread_idx = linear_idx();

    const uint32_t i = thread_idx / w;
    const uint32_t j = thread_idx % w;

    if (i >= h || j >= w) // Important check right here
    {
        return;
    }

    scalar_t out_val = at::numeric_limits<scalar_t>::lowest();
    uint32_t back_h = 0, back_w = 0;

    for (uint32_t k = (i < f_top ? f_top - i : 0);
         k < (i < h - f_bottom ? fh : h - i + f_top);
         k++)
    {
        for (uint32_t l = (j < f_left ? f_left - j : 0);
             l < (j < w - f_right ? fw : w - j + f_left);
             l++)
        {
            const scalar_t new_val =
                image_a[i + k - f_top][j + l - f_left] + filter_a[k][l];

            if (new_val > out_val)
            {
                out_val = new_val;
                back_h = i + k - f_top;
                back_w = j + l - f_left;
            }
        }
    }

    out_a[i][j] = out_val;
    backindex_a[i][j][0] = static_cast<int64_t>(back_h);
    backindex_a[i][j][1] = static_cast<int64_t>(back_w);
}

// Backward 2D dilation kernel
template <typename scalar_t>
__global__ void
grayscale_dilation_2d_bw_kernel(
    const torch::PackedTensorAccessor32<int64_t, 3, torch::RestrictPtrTraits>
        backindex_a,
    const torch::PackedTensorAccessor32<scalar_t, 2, torch::RestrictPtrTraits>
        grad_a,
    torch::PackedTensorAccessor32<scalar_t, 2, torch::RestrictPtrTraits>
        image_grad_a,
    torch::PackedTensorAccessor32<scalar_t, 2, torch::RestrictPtrTraits>
        filter_grad_a)
{
    const uint32_t h = image_grad_a.size(0);
    const uint32_t w = image_grad_a.size(1);
    const uint32_t fh = filter_grad_a.size(0);
    const uint32_t fw = filter_grad_a.size(1);

    const uint32_t f_top = (fh - 1) / 2 + (fh - 1) % 2;
    const uint32_t f_left = (fw - 1) / 2 + (fw - 1) % 2;

    const uint32_t thread_idx = linear_idx();

    const uint32_t i = thread_idx / w;
    const uint32_t j = thread_idx % w;

    if (i >= h || j >= w) // Important check right here
    {
        return;
    }

    scalar_t grad_val = grad_a[i][j];

    const uint32_t back_h = static_cast<uint32_t>(backindex_a[i][j][0]);
    const uint32_t back_w = static_cast<uint32_t>(backindex_a[i][j][1]);
    atomicAdd(&image_grad_a[back_h][back_w], grad_val);

    const uint32_t back_filter_h = back_h - i + f_top;
    const uint32_t back_filter_w = back_w - j + f_left;
    atomicAdd(&filter_grad_a[back_filter_h][back_filter_w], grad_val);
}

// Forward 2D grayscale erosion kernel
template <typename scalar_t>
__global__ void
grayscale_erosion_2d_fw_kernel(
    const torch::PackedTensorAccessor32<scalar_t, 2, torch::RestrictPtrTraits>
        image_a,
    const torch::PackedTensorAccessor32<scalar_t, 2, torch::RestrictPtrTraits>
        filter_a,
    torch::PackedTensorAccessor32<scalar_t, 2, torch::RestrictPtrTraits> out_a,
    torch::PackedTensorAccessor32<int64_t, 3, torch::RestrictPtrTraits>
        backindex_a)
{
    const uint32_t h = image_a.size(0);
    const uint32_t w = image_a.size(1);
    const uint32_t fh = filter_a.size(0);
    const uint32_t fw = filter_a.size(1);

    const uint32_t f_top = (fh - 1) / 2 + (fh - 1) % 2;
    const uint32_t f_bottom = (fh - 1) / 2;
    const uint32_t f_left = (fw - 1) / 2 + (fw - 1) % 2;
    const uint32_t f_right = (fw - 1) / 2;

    const uint32_t thread_idx = linear_idx();

    const uint32_t i = thread_idx / w;
    const uint32_t j = thread_idx % w;

    if (i >= h || j >= w) // Important check right here
    {
        return;
    }

    scalar_t out_val = at::numeric_limits<scalar_t>::max();
    uint32_t back_h = 0, back_w = 0;

    for (uint32_t k = (i < f_top ? f_top - i : 0);
         k < (i < h - f_bottom ? fh : h - i + f_top);
         k++)
    {
        for (uint32_t l = (j < f_left ? f_left - j : 0);
             l < (j < w - f_right ? fw : w - j + f_left);
             l++)
        {
            const scalar_t new_val =
                image_a[i + k - f_top][j + l - f_left] - filter_a[k][l];

            if (new_val < out_val)
            {
                out_val = new_val;
                back_h = i + k - f_top;
                back_w = j + l - f_left;
            }
        }
    }

    out_a[i][j] = out_val;
    backindex_a[i][j][0] = static_cast<int64_t>(back_h);
    backindex_a[i][j][1] = static_cast<int64_t>(back_w);
}

// Backward 2D erosion kernel
template <typename scalar_t>
__global__ void
grayscale_erosion_2d_bw_kernel(
    const torch::PackedTensorAccessor32<int64_t, 3, torch::RestrictPtrTraits>
        backindex_a,
    const torch::PackedTensorAccessor32<scalar_t, 2, torch::RestrictPtrTraits>
        grad_a,
    torch::PackedTensorAccessor32<scalar_t, 2, torch::RestrictPtrTraits>
        image_grad_a,
    torch::PackedTensorAccessor32<scalar_t, 2, torch::RestrictPtrTraits>
        filter_grad_a)
{
    const uint32_t h = image_grad_a.size(0);
    const uint32_t w = image_grad_a.size(1);
    const uint32_t fh = filter_grad_a.size(0);
    const uint32_t fw = filter_grad_a.size(1);

    const uint32_t f_top = (fh - 1) / 2 + (fh - 1) % 2;
    const uint32_t f_left = (fw - 1) / 2 + (fw - 1) % 2;

    const uint32_t thread_idx = linear_idx();

    const uint32_t i = thread_idx / w;
    const uint32_t j = thread_idx % w;

    if (i >= h || j >= w) // Important check right here
    {
        return;
    }

    scalar_t grad_val = grad_a[i][j];

    const uint32_t back_h = static_cast<uint32_t>(backindex_a[i][j][0]);
    const uint32_t back_w = static_cast<uint32_t>(backindex_a[i][j][1]);
    atomicAdd(&image_grad_a[back_h][back_w], grad_val);

    const uint32_t back_filter_h = back_h - i + f_top;
    const uint32_t back_filter_w = back_w - j + f_left;
    atomicSub(&filter_grad_a[back_filter_h][back_filter_w], grad_val);
}

} // namespace

namespace lietorch
{
namespace generic
{

// Add, forward
torch::Tensor
add_fw_cuda(const torch::Tensor& a, const torch::Tensor& b)
{
    auto out = torch::zeros_like(a);

    AT_DISPATCH_ALL_TYPES(a.scalar_type(), __func__, [&] {
        auto numel = a.numel();
        auto a_ptr = a.data_ptr();
        auto b_ptr = b.data_ptr();
        auto out_ptr = out.data_ptr();

        void* args[] = {&a_ptr, &b_ptr, &out_ptr, &numel};

        CUDA_CALL(cudaLaunchKernel(
            (void*)add_fw_cuda_kernel<scalar_t>, blocks(a), threads(a), args));
    });

    return out;
}

// Add, backward
std::tuple<torch::Tensor, torch::Tensor>
add_bw_cuda(const torch::Tensor& grad)
{
    return std::make_tuple(grad, grad);
}

// Grayscale dilation 2D, forward
std::tuple<torch::Tensor, torch::Tensor>
grayscale_dilation_2d_fw_cuda(
    const torch::Tensor& image, const torch::Tensor& filter)
{
    std::vector<int64_t> backindex_size = image.sizes().vec();
    backindex_size.push_back(2);
    torch::Tensor out = torch::empty_like(image);
    torch::Tensor backindex = torch::empty(
        backindex_size,
        torch::TensorOptions().dtype(torch::kInt64).device(image.device()));

    AT_DISPATCH_ALL_TYPES(image.scalar_type(), __func__, [&] {
        auto image_a =
            image.packed_accessor32<scalar_t, 2, torch::RestrictPtrTraits>();
        auto filter_a =
            filter.packed_accessor32<scalar_t, 2, torch::RestrictPtrTraits>();
        auto out_a =
            out.packed_accessor32<scalar_t, 2, torch::RestrictPtrTraits>();
        auto backindex_a =
            backindex.packed_accessor32<int64_t, 3, torch::RestrictPtrTraits>();

        void* args[] = {&image_a, &filter_a, &out_a, &backindex_a};

        CUDA_CALL(cudaLaunchKernel(
            (void*)grayscale_dilation_2d_fw_kernel<scalar_t>,
            blocks(image),
            threads(image),
            args));
    });

    return std::make_tuple(out, backindex);
}

// Grayscale dilation 2D, backward
std::tuple<torch::Tensor, torch::Tensor>
grayscale_dilation_2d_bw_cuda(
    const torch::Tensor& backindex,
    const torch::Tensor& grad,
    const int64_t filter_h,
    const int64_t filter_w)
{
    auto image_grad = torch::zeros_like(grad);
    auto filter_grad = torch::zeros(
        {filter_h, filter_w},
        torch::TensorOptions().dtype(grad.dtype()).device(grad.device()));

    AT_DISPATCH_ALL_TYPES(grad.scalar_type(), __func__, [&] {
        auto backindex_a =
            backindex.packed_accessor32<int64_t, 3, torch::RestrictPtrTraits>();
        auto grad_a =
            grad.packed_accessor32<scalar_t, 2, torch::RestrictPtrTraits>();
        auto image_grad_a =
            image_grad
                .packed_accessor32<scalar_t, 2, torch::RestrictPtrTraits>();
        auto filter_grad_a =
            filter_grad
                .packed_accessor32<scalar_t, 2, torch::RestrictPtrTraits>();

        void* args[] = {&backindex_a, &grad_a, &image_grad_a, &filter_grad_a};

        CUDA_CALL(cudaLaunchKernel(
            (void*)grayscale_dilation_2d_bw_kernel<scalar_t>,
            blocks(grad),
            threads(grad),
            args));
    });

    return std::make_tuple(image_grad, filter_grad);
}

// Grayscale erosion 2D, forward
std::tuple<torch::Tensor, torch::Tensor>
grayscale_erosion_2d_fw_cuda(
    const torch::Tensor& image, const torch::Tensor& filter)
{
    std::vector<int64_t> backindex_size = image.sizes().vec();
    backindex_size.push_back(2);
    torch::Tensor out = torch::empty_like(image);
    torch::Tensor backindex = torch::empty(
        backindex_size,
        torch::TensorOptions().dtype(torch::kInt64).device(image.device()));

    AT_DISPATCH_ALL_TYPES(image.scalar_type(), __func__, [&] {
        auto image_a =
            image.packed_accessor32<scalar_t, 2, torch::RestrictPtrTraits>();
        auto filter_a =
            filter.packed_accessor32<scalar_t, 2, torch::RestrictPtrTraits>();
        auto out_a =
            out.packed_accessor32<scalar_t, 2, torch::RestrictPtrTraits>();
        auto backindex_a =
            backindex.packed_accessor32<int64_t, 3, torch::RestrictPtrTraits>();

        void* args[] = {&image_a, &filter_a, &out_a, &backindex_a};

        CUDA_CALL(cudaLaunchKernel(
            (void*)grayscale_erosion_2d_fw_kernel<scalar_t>,
            blocks(image),
            threads(image),
            args));
    });

    return std::make_tuple(out, backindex);
}

// Grayscale erosion 2D, backward
std::tuple<torch::Tensor, torch::Tensor>
grayscale_erosion_2d_bw_cuda(
    const torch::Tensor& backindex,
    const torch::Tensor& grad,
    const int64_t filter_h,
    const int64_t filter_w)
{
    auto image_grad = torch::zeros_like(grad);
    auto filter_grad = torch::zeros(
        {filter_h, filter_w},
        torch::TensorOptions().dtype(grad.dtype()).device(grad.device()));

    AT_DISPATCH_ALL_TYPES(grad.scalar_type(), __func__, [&] {
        auto backindex_a =
            backindex.packed_accessor32<int64_t, 3, torch::RestrictPtrTraits>();
        auto grad_a =
            grad.packed_accessor32<scalar_t, 2, torch::RestrictPtrTraits>();
        auto image_grad_a =
            image_grad
                .packed_accessor32<scalar_t, 2, torch::RestrictPtrTraits>();
        auto filter_grad_a =
            filter_grad
                .packed_accessor32<scalar_t, 2, torch::RestrictPtrTraits>();

        void* args[] = {&backindex_a, &grad_a, &image_grad_a, &filter_grad_a};

        CUDA_CALL(cudaLaunchKernel(
            (void*)grayscale_erosion_2d_bw_kernel<scalar_t>,
            blocks(grad),
            threads(grad),
            args));
    });

    return std::make_tuple(image_grad, filter_grad);
}

} // namespace generic
} // namespace lietorch