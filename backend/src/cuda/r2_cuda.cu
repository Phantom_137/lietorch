/**
 *
 *
 *
 */

#include "r2_cuda.cuh"

#pragma warning(push, 1)
#include <ATen/ATen.h>
#include <ATen/cuda/NumericLimits.cuh>
#pragma warning(pop)

#include "atomics.cuh"
#include "dispatch.cuh"
#include "math_cuda.cuh"
#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <math_constants.h>

namespace lietorch
{
namespace r2
{

/*

*/
template <typename scalar_t>
__global__ void
morphological_convolution_fw_kernel(
    const torch::PackedTensorAccessor32<scalar_t, 4, torch::RestrictPtrTraits>
        input_a,
    const torch::PackedTensorAccessor32<scalar_t, 3, torch::RestrictPtrTraits>
        kernel_a,
    torch::PackedTensorAccessor32<scalar_t, 4, torch::RestrictPtrTraits> out_a,
    torch::PackedTensorAccessor32<int32_t, 5, torch::RestrictPtrTraits>
        backindex_a)
{
    const int32_t thread_idx = linear_idx();

    const int32_t b = thread_idx / input_a.stride(0);
    const int32_t c = (thread_idx % input_a.stride(0)) / input_a.stride(1);
    const int32_t j = (thread_idx % input_a.stride(1)) / input_a.stride(2);
    const int32_t k = thread_idx % input_a.stride(2);

    if (b >= input_a.size(0))
    {
        return;
    }

    const int32_t h = input_a.size(2); // number of y's
    const int32_t w = input_a.size(3); // number of x's

    const int32_t ker_h = kernel_a.size(1);
    const int32_t ker_w = kernel_a.size(2);

    const int32_t r_h = (ker_h - 1) / 2;
    const int32_t r_w = (ker_w - 1) / 2;

    scalar_t val = at::numeric_limits<scalar_t>::max();
    int32_t hit_j, hit_k;

    const int32_t jker_from = max(0, r_h - j);
    const int32_t jker_to = min(ker_h, h + r_h - j);
    const int32_t kker_from = max(0, r_w - k);
    const int32_t kker_to = min(ker_w, w + r_w - k);

    for (int32_t jker = jker_from; jker < jker_to; jker++)
    {
        for (int32_t kker = kker_from; kker < kker_to; kker++)
        {
            const scalar_t in = input_a[b][c][j - r_h + jker][k - r_w + kker];

            const scalar_t newval = in + kernel_a[c][jker][kker];
            if (newval < val)
            {
                val = newval;

                hit_j = j - r_h + jker;
                hit_k = k - r_w + kker;
            }
        }
    }

    out_a[b][c][j][k] = val;
    backindex_a[b][c][j][k][0] = hit_j;
    backindex_a[b][c][j][k][1] = hit_k;
}

/*

*/
template <typename scalar_t>
__global__ void
morphological_convolution_fw_nograd_kernel(
    const torch::PackedTensorAccessor32<scalar_t, 4, torch::RestrictPtrTraits>
        input_a,
    const torch::PackedTensorAccessor32<scalar_t, 3, torch::RestrictPtrTraits>
        kernel_a,
    torch::PackedTensorAccessor32<scalar_t, 4, torch::RestrictPtrTraits> out_a)
{
    const int32_t thread_idx = linear_idx();

    const int32_t b = thread_idx / input_a.stride(0);
    const int32_t c = (thread_idx % input_a.stride(0)) / input_a.stride(1);
    const int32_t j = (thread_idx % input_a.stride(1)) / input_a.stride(2);
    const int32_t k = thread_idx % input_a.stride(2);

    if (b >= input_a.size(0))
    {
        return;
    }

    const int32_t h = input_a.size(2); // number of y's
    const int32_t w = input_a.size(3); // number of x's

    const int32_t ker_h = kernel_a.size(1);
    const int32_t ker_w = kernel_a.size(2);

    const int32_t r_h = (ker_h - 1) / 2;
    const int32_t r_w = (ker_w - 1) / 2;

    scalar_t val = at::numeric_limits<scalar_t>::max();

    const int32_t jker_from = max(0, r_h - j);
    const int32_t jker_to = min(ker_h, h + r_h - j);
    const int32_t kker_from = max(0, r_w - k);
    const int32_t kker_to = min(ker_w, w + r_w - k);

    for (int32_t jker = jker_from; jker < jker_to; jker++)
    {
        for (int32_t kker = kker_from; kker < kker_to; kker++)
        {
            const scalar_t in = input_a[b][c][j - r_h + jker][k - r_w + kker];

            const scalar_t newval = in + kernel_a[c][jker][kker];
            if (newval < val)
            {
                val = newval;
            }
        }
    }

    out_a[b][c][j][k] = val;
}

/*

*/
std::tuple<torch::Tensor, torch::Tensor>
morphological_convolution_fw_cuda(
    const torch::Tensor& input, const torch::Tensor& kernel)
{
    const auto out = torch::zeros_like(input);

    if (input.requires_grad() || kernel.requires_grad())
    {
        std::vector<int64_t> backindex_size =
            input.sizes().vec();     // = {B,C,H,W}
        backindex_size.push_back(3); // = {B,C,H,W,2}
        const auto backindex = torch::zeros(
            backindex_size,
            torch::TensorOptions().dtype(torch::kInt32).device(input.device()));

        AT_DISPATCH_FLOATING_TYPES(input.scalar_type(), __func__, [&] {
            auto input_a =
                input
                    .packed_accessor32<scalar_t, 4, torch::RestrictPtrTraits>();
            auto kernel_a =
                kernel
                    .packed_accessor32<scalar_t, 3, torch::RestrictPtrTraits>();
            auto out_a =
                out.packed_accessor32<scalar_t, 4, torch::RestrictPtrTraits>();
            auto backindex_a =
                backindex
                    .packed_accessor32<int32_t, 5, torch::RestrictPtrTraits>();

            void* args[] = {&input_a, &kernel_a, &out_a, &backindex_a};

            CUDA_CALL(cudaLaunchKernel(
                (void*)morphological_convolution_fw_kernel<scalar_t>,
                blocks(input),
                threads(input),
                args));
        });

        return std::make_tuple(out, backindex);
    }
    else
    {
        AT_DISPATCH_FLOATING_TYPES(input.scalar_type(), __func__, [&] {
            auto input_a =
                input
                    .packed_accessor32<scalar_t, 4, torch::RestrictPtrTraits>();
            auto kernel_a =
                kernel
                    .packed_accessor32<scalar_t, 3, torch::RestrictPtrTraits>();
            auto out_a =
                out.packed_accessor32<scalar_t, 4, torch::RestrictPtrTraits>();

            void* args[] = {&input_a, &kernel_a, &out_a};

            CUDA_CALL(cudaLaunchKernel(
                (void*)morphological_convolution_fw_nograd_kernel<scalar_t>,
                blocks(input),
                threads(input),
                args));
        });

        return std::make_tuple(out, torch::Tensor());
    }
}

/*



*/
template <typename scalar_t>
__global__ void
morphological_convolution_bw_kernel(
    const torch::PackedTensorAccessor32<scalar_t, 4, torch::RestrictPtrTraits>
        grad_a,
    const torch::PackedTensorAccessor32<int32_t, 5, torch::RestrictPtrTraits>
        backindex_a,
    torch::PackedTensorAccessor32<scalar_t, 4, torch::RestrictPtrTraits>
        input_grad_a,
    torch::PackedTensorAccessor32<scalar_t, 4, torch::RestrictPtrTraits>
        kernel_grad_a)
{
    using co_t = float;

    const int32_t thread_idx = linear_idx();

    const int32_t b = thread_idx / grad_a.stride(0);
    const int32_t c = (thread_idx % grad_a.stride(0)) / grad_a.stride(1);
    const int32_t j = (thread_idx % grad_a.stride(1)) / grad_a.stride(2);
    const int32_t k = thread_idx % grad_a.stride(2);

    if (b >= grad_a.size(0))
    {
        return;
    }

    const int32_t h = grad_a.size(2); // number of y's
    const int32_t w = grad_a.size(3); // number of x's

    const int32_t kh = kernel_grad_a.size(2);
    const int32_t kw = kernel_grad_a.size(3);

    const int32_t stack_h = kh % 2 == 0 ? kh + 1 : kh;
    const int32_t stack_w = kw % 2 == 0 ? kw + 1 : kw;

    const int32_t r_h = (stack_h - 1) / 2;
    const int32_t r_w = (stack_w - 1) / 2;

    const int32_t hit_j = backindex_a[b][c][j][k][0];
    const int32_t hit_k = backindex_a[b][c][j][k][1];

    const scalar_t gradval = grad_a[b][c][j][k];

    atomicAdd(&input_grad_a[b][c][hit_j][hit_k], gradval);

    const auto ker_j = hit_j - j + r_h;
    const auto ker_k = hit_k - k + r_w;

    if (ker_j >= 0 && ker_j < kh && ker_k >= 0 && ker_k < kw)
    {
        atomicAdd(&kernel_grad_a[b][c][ker_j][ker_k], gradval);
    }
}

/*

 */
std::tuple<torch::Tensor, torch::Tensor>
morphological_convolution_bw_cuda(
    const torch::Tensor& grad,
    const torch::Tensor& backindex,
    const torch::IntArrayRef kernel_sizes)
{
    const auto B = grad.size(0);
    const auto C = kernel_sizes[0];
    const auto kH = kernel_sizes[1];
    const auto kW = kernel_sizes[2];
    const auto input_grad = torch::zeros_like(grad);
    const auto kernel_grad = torch::zeros({B, C, kH, kW}, grad.options());
    const auto backindex2 = backindex.to(torch::kInt32);

    AT_DISPATCH_FLOATING_TYPES(grad.scalar_type(), __func__, [&] {
        auto grad_a =
            grad.packed_accessor32<scalar_t, 4, torch::RestrictPtrTraits>();
        auto backindex_a =
            backindex2
                .packed_accessor32<int32_t, 5, torch::RestrictPtrTraits>();
        auto input_grad_a =
            input_grad
                .packed_accessor32<scalar_t, 4, torch::RestrictPtrTraits>();
        auto kernel_grad_a =
            kernel_grad
                .packed_accessor32<scalar_t, 4, torch::RestrictPtrTraits>();

        void* args[] = {&grad_a, &backindex_a, &input_grad_a, &kernel_grad_a};

        CUDA_CALL(cudaLaunchKernel(
            (void*)morphological_convolution_bw_kernel<scalar_t>,
            blocks(grad),
            threads(grad),
            args));
    });

    return std::make_tuple(input_grad, kernel_grad.sum({0}));
}

/*

*/
template <typename scalar_t, typename coordinate_t>
__global__ void
convection_fw_kernel(
    const torch::PackedTensorAccessor32<scalar_t, 4, torch::RestrictPtrTraits>
        input_a,
    const torch::
        PackedTensorAccessor32<coordinate_t, 2, torch::RestrictPtrTraits> g0_a,
    torch::PackedTensorAccessor32<scalar_t, 4, torch::RestrictPtrTraits> out_a,
    torch::PackedTensorAccessor32<scalar_t, 5, torch::RestrictPtrTraits>
        out_grad_field_a)
{
    const int32_t thread_idx = linear_idx();

    const int32_t h = input_a.size(2);
    const int32_t w = input_a.size(3);

    const int32_t b = thread_idx / input_a.stride(0);
    const int32_t c = (thread_idx % input_a.stride(0)) / input_a.stride(1);
    const int32_t j = (thread_idx % input_a.stride(1)) / input_a.stride(2);
    const int32_t k = thread_idx % input_a.stride(2);

    if (b >= input_a.size(0))
    {
        return;
    }

    const coordinate_t y_sample = -g0_a[c][0] + static_cast<coordinate_t>(j);
    const coordinate_t x_sample = -g0_a[c][1] + static_cast<coordinate_t>(k);

    if (y_sample <= -1 || y_sample >= h || x_sample <= -1 || x_sample >= w)
    {
        return;
    }

    const auto base_offset_j = lt_cu_intfrac<coordinate_t>(y_sample);
    const int32_t base_j = static_cast<int32_t>(std::get<0>(base_offset_j));
    const scalar_t offset_j = static_cast<scalar_t>(std::get<1>(base_offset_j));

    const auto base_offset_k = lt_cu_intfrac<coordinate_t>(x_sample);
    const int32_t base_k = static_cast<int32_t>(std::get<0>(base_offset_k));
    const scalar_t offset_k = static_cast<scalar_t>(std::get<1>(base_offset_k));

    const scalar_t v00 =
        (base_j < 0 || base_k < 0) ? 0 : input_a[b][c][base_j][base_k];
    const scalar_t v01 =
        (base_j < 0 || base_k + 1 >= w) ? 0 : input_a[b][c][base_j][base_k + 1];
    const scalar_t v10 =
        (base_j + 1 >= h || base_k < 0) ? 0 : input_a[b][c][base_j + 1][base_k];
    const scalar_t v11 = (base_j + 1 >= h || base_k + 1 >= w)
                             ? 0
                             : input_a[b][c][base_j + 1][base_k + 1];

    const scalar_t v0 = (1 - offset_k) * v00 + offset_k * v01;
    const scalar_t v1 = (1 - offset_k) * v10 + offset_k * v11;
    const scalar_t v = (1 - offset_j) * v0 + offset_j * v1;

    const scalar_t dy = v1 - v0;
    const scalar_t dx = (1 - offset_j) * (v01 - v00) + offset_j * (v11 - v10);

    out_a[b][c][j][k] = v;
    out_grad_field_a[b][c][j][k][0] = -dy;
    out_grad_field_a[b][c][j][k][1] = -dx;
}

/*

*/
template <typename scalar_t, typename coordinate_t>
__global__ void
convection_fw_no_grad_kernel(
    const torch::PackedTensorAccessor32<scalar_t, 4, torch::RestrictPtrTraits>
        input_a,
    const torch::
        PackedTensorAccessor32<coordinate_t, 2, torch::RestrictPtrTraits> g0_a,
    torch::PackedTensorAccessor32<scalar_t, 4, torch::RestrictPtrTraits> out_a)
{
    const int32_t thread_idx = linear_idx();

    const int32_t h = input_a.size(2);
    const int32_t w = input_a.size(3);

    const int32_t b = thread_idx / input_a.stride(0);
    const int32_t c = (thread_idx % input_a.stride(0)) / input_a.stride(1);
    const int32_t j = (thread_idx % input_a.stride(1)) / input_a.stride(2);
    const int32_t k = thread_idx % input_a.stride(2);

    if (b >= input_a.size(0))
    {
        return;
    }

    const coordinate_t y_sample = -g0_a[c][0] + static_cast<coordinate_t>(j);
    const coordinate_t x_sample = -g0_a[c][1] + static_cast<coordinate_t>(k);

    if (y_sample <= -1 || y_sample >= h || x_sample <= -1 || x_sample >= w)
    {
        return;
    }

    const auto base_offset_j = lt_cu_intfrac<coordinate_t>(y_sample);
    const int32_t base_j = static_cast<int32_t>(std::get<0>(base_offset_j));
    const scalar_t offset_j = static_cast<scalar_t>(std::get<1>(base_offset_j));

    const auto base_offset_k = lt_cu_intfrac<coordinate_t>(x_sample);
    const int32_t base_k = static_cast<int32_t>(std::get<0>(base_offset_k));
    const scalar_t offset_k = static_cast<scalar_t>(std::get<1>(base_offset_k));

    const scalar_t v00 =
        (base_j < 0 || base_k < 0) ? 0 : input_a[b][c][base_j][base_k];
    const scalar_t v01 =
        (base_j < 0 || base_k + 1 >= w) ? 0 : input_a[b][c][base_j][base_k + 1];
    const scalar_t v10 =
        (base_j + 1 >= h || base_k < 0) ? 0 : input_a[b][c][base_j + 1][base_k];
    const scalar_t v11 = (base_j + 1 >= h || base_k + 1 >= w)
                             ? 0
                             : input_a[b][c][base_j + 1][base_k + 1];

    const scalar_t v0 = (1 - offset_k) * v00 + offset_k * v01;
    const scalar_t v1 = (1 - offset_k) * v10 + offset_k * v11;
    const scalar_t v = (1 - offset_j) * v0 + offset_j * v1;

    out_a[b][c][j][k] = v;
}

/*

 */
std::tuple<torch::Tensor, torch::Tensor>
convection_fw_cuda(
    const torch::Tensor& input, // [B,C,H,W]
    const torch::Tensor& g0     // [C,2]
)
{
    const auto out = torch::zeros_like(input);

    if (input.requires_grad() || g0.requires_grad())
    {
        std::vector<int64_t> field_size = input.sizes().vec(); // = {B,C,H,W}
        field_size.push_back(2);                               // = {B,C,H,W,2}
        const auto out_grad_field = input.new_zeros(field_size);

        AT_DISPATCH_FLOATING_TYPES(input.scalar_type(), __func__, [&] {
            auto input_a =
                input
                    .packed_accessor32<scalar_t, 4, torch::RestrictPtrTraits>();
            auto out_a =
                out.packed_accessor32<scalar_t, 4, torch::RestrictPtrTraits>();
            auto out_grad_field_a =
                out_grad_field
                    .packed_accessor32<scalar_t, 5, torch::RestrictPtrTraits>();

            switch (g0.scalar_type())
            {
            case torch::kFloat32:
            {
                auto g0_a =
                    g0.packed_accessor32<float, 2, torch::RestrictPtrTraits>();

                void* args[] = {&input_a, &g0_a, &out_a, &out_grad_field_a};
                CUDA_CALL(cudaLaunchKernel(
                    (void*)convection_fw_kernel<scalar_t, float>,
                    blocks(input),
                    threads(input),
                    args));
                break;
            }

            case torch::kFloat64:
            {
                auto g0_a =
                    g0.packed_accessor32<double, 2, torch::RestrictPtrTraits>();

                void* args[] = {&input_a, &g0_a, &out_a, &out_grad_field_a};
                CUDA_CALL(cudaLaunchKernel(
                    (void*)convection_fw_kernel<scalar_t, double>,
                    blocks(input),
                    threads(input),
                    args));
                break;
            }

            default:
                TORCH_CHECK(
                    false,
                    __func__,
                    " not implemented for '",
                    toString(g0.scalar_type()),
                    "' for the convection vector.");
            }
        });

        return std::make_tuple(out, out_grad_field);
    }
    else
    {
        AT_DISPATCH_FLOATING_TYPES(input.scalar_type(), __func__, [&] {
            auto input_a =
                input
                    .packed_accessor32<scalar_t, 4, torch::RestrictPtrTraits>();
            auto out_a =
                out.packed_accessor32<scalar_t, 4, torch::RestrictPtrTraits>();

            switch (g0.scalar_type())
            {
            case torch::kFloat32:
            {
                auto g0_a =
                    g0.packed_accessor32<float, 2, torch::RestrictPtrTraits>();

                void* args[] = {&input_a, &g0_a, &out_a};
                CUDA_CALL(cudaLaunchKernel(
                    (void*)convection_fw_no_grad_kernel<scalar_t, float>,
                    blocks(input),
                    threads(input),
                    args));
                break;
            }

            case torch::kFloat64:
            {
                auto g0_a =
                    g0.packed_accessor32<double, 2, torch::RestrictPtrTraits>();

                void* args[] = {&input_a, &g0_a, &out_a};
                CUDA_CALL(cudaLaunchKernel(
                    (void*)convection_fw_no_grad_kernel<scalar_t, double>,
                    blocks(input),
                    threads(input),
                    args));
                break;
            }

            default:
                TORCH_CHECK(
                    false,
                    __func__,
                    " not implemented for '",
                    toString(g0.scalar_type()),
                    "' for the convection vector.");
            }
        });

        return std::make_tuple(out, torch::empty({0}));
    }
}

/*

*/
template <typename scalar_t, typename coordinate_t>
__global__ void
convection_bw_kernel(
    const torch::
        PackedTensorAccessor32<coordinate_t, 2, torch::RestrictPtrTraits> g0_a,
    const torch::PackedTensorAccessor32<scalar_t, 4, torch::RestrictPtrTraits>
        grad_a,
    torch::PackedTensorAccessor32<scalar_t, 4, torch::RestrictPtrTraits>
        input_grad_a,
    const torch::PackedTensorAccessor32<scalar_t, 5, torch::RestrictPtrTraits>
        out_grad_field_a,
    torch::PackedTensorAccessor32<coordinate_t, 5, torch::RestrictPtrTraits>
        g0_splits_a)
{
    const int32_t thread_idx = linear_idx();

    const int32_t h = grad_a.size(2);
    const int32_t w = grad_a.size(3);

    const int32_t b = thread_idx / grad_a.stride(0);
    const int32_t c = (thread_idx % grad_a.stride(0)) / grad_a.stride(1);
    const int32_t j = (thread_idx % grad_a.stride(1)) / grad_a.stride(2);
    const int32_t k = thread_idx % grad_a.stride(2);

    if (b >= grad_a.size(0))
    {
        return;
    }

    const coordinate_t y_sample = g0_a[c][0] + static_cast<coordinate_t>(j);
    const coordinate_t x_sample = g0_a[c][1] + static_cast<coordinate_t>(k);

    if (y_sample <= -1 || y_sample >= h || x_sample <= -1 || x_sample >= w)
    {
        return;
    }

    const auto base_offset_j = lt_cu_intfrac<coordinate_t>(y_sample);
    const int32_t base_j = static_cast<int32_t>(std::get<0>(base_offset_j));
    const scalar_t offset_j = static_cast<scalar_t>(std::get<1>(base_offset_j));

    const auto base_offset_k = lt_cu_intfrac<coordinate_t>(x_sample);
    const int32_t base_k = static_cast<int32_t>(std::get<0>(base_offset_k));
    const scalar_t offset_k = static_cast<scalar_t>(std::get<1>(base_offset_k));

    const scalar_t v00 =
        (base_j < 0 || base_k < 0) ? 0 : grad_a[b][c][base_j][base_k];
    const scalar_t v01 =
        (base_j < 0 || base_k + 1 >= w) ? 0 : grad_a[b][c][base_j][base_k + 1];
    const scalar_t v10 =
        (base_j + 1 >= h || base_k < 0) ? 0 : grad_a[b][c][base_j + 1][base_k];
    const scalar_t v11 = (base_j + 1 >= h || base_k + 1 >= w)
                             ? 0
                             : grad_a[b][c][base_j + 1][base_k + 1];

    const scalar_t v0 = (1 - offset_k) * v00 + offset_k * v01;
    const scalar_t v1 = (1 - offset_k) * v10 + offset_k * v11;
    const scalar_t v = (1 - offset_j) * v0 + offset_j * v1;

    input_grad_a[b][c][j][k] = v;

    const int32_t j2 = h - j - 1;
    const int32_t k2 = w - k - 1;

    const coordinate_t dy =
        static_cast<coordinate_t>(out_grad_field_a[b][c][j2][k2][0]);
    const coordinate_t dx =
        static_cast<coordinate_t>(out_grad_field_a[b][c][j2][k2][1]);
    const coordinate_t gval = static_cast<coordinate_t>(grad_a[b][c][j2][k2]);

    g0_splits_a[b][c][j][k][0] = gval * dy;
    g0_splits_a[b][c][j][k][1] = gval * dx;
}

/*

 */
std::tuple<torch::Tensor, torch::Tensor>
convection_bw_cuda(
    const torch::Tensor& g0,
    const torch::Tensor& grad,
    const torch::Tensor& out_grad_field)
{
    const auto input_grad = torch::zeros_like(grad);
    const auto g0_splits = torch::zeros_like(
        out_grad_field, torch::TensorOptions().dtype(g0.scalar_type()));

    AT_DISPATCH_FLOATING_TYPES(grad.scalar_type(), __func__, [&] {
        auto grad_a =
            grad.packed_accessor32<scalar_t, 4, torch::RestrictPtrTraits>();
        auto input_grad_a =
            input_grad
                .packed_accessor32<scalar_t, 4, torch::RestrictPtrTraits>();
        auto out_grad_field_a =
            out_grad_field
                .packed_accessor32<scalar_t, 5, torch::RestrictPtrTraits>();

        switch (g0.scalar_type())
        {

        case at::ScalarType::Float:
        {
            auto g0_a =
                g0.packed_accessor32<float, 2, torch::RestrictPtrTraits>();
            auto g0_splits_a =
                g0_splits
                    .packed_accessor32<float, 5, torch::RestrictPtrTraits>();

            void* args[] = {
                &g0_a, &grad_a, &input_grad_a, &out_grad_field_a, &g0_splits_a};

            CUDA_CALL(cudaLaunchKernel(
                (void*)convection_bw_kernel<scalar_t, float>,
                blocks(grad),
                threads(grad),
                args));

            break;
        }

        case at::ScalarType::Double:
        {
            auto g0_a =
                g0.packed_accessor32<double, 2, torch::RestrictPtrTraits>();
            auto g0_splits_a =
                g0_splits
                    .packed_accessor32<double, 5, torch::RestrictPtrTraits>();

            void* args[] = {
                &g0_a, &grad_a, &input_grad_a, &out_grad_field_a, &g0_splits_a};

            CUDA_CALL(cudaLaunchKernel(
                (void*)convection_bw_kernel<scalar_t, double>,
                blocks(grad),
                threads(grad),
                args));

            break;
        }

        default:
            TORCH_CHECK(
                false,
                __func__,
                " not implemented for '",
                toString(g0.scalar_type()),
                "' for the convection vector.");
        }
    });

    const auto g0_grad = g0_splits.sum({0, 2, 3});

    return std::make_tuple(input_grad, g0_grad);
}

} // namespace r2
} // namespace lietorch