

#include "../src/torch_include.h"
#include "gtest/gtest.h"

#ifdef _WIN32
#include <windows.h>
#endif

#include "../src/dbg.h"
#include <filesystem>

#include "../src/lru_cache.h"

TEST(GENERAL, sample_test)
{
    using namespace torch::indexing;
    const auto a = torch::ones({2, 3, 8, 5, 5, 3});
    const auto b = torch::ones({2, 3, 8, 5, 5});
    const auto c = torch::zeros({3, 3});

    const auto ab = a * b.index({Ellipsis, None});
    const auto s = ab.sum({0, 2, 3, 4});
    // dbg(s);

    EXPECT_EQ(1, 1);
}

#ifdef _WIN32
TEST(GENERAL, load_lietorch_dll)
{
    TCHAR szDir[MAX_PATH] = {0};

    GetModuleFileName(NULL, szDir, MAX_PATH);
    szDir[std::basic_string<TCHAR>(szDir).find_last_of("\\/")] = 0;
    szDir[std::basic_string<TCHAR>(szDir).find_last_of("\\/")] = 0;
    szDir[std::basic_string<TCHAR>(szDir).find_last_of("\\/")] = 0;

    std::basic_string<TCHAR> path(szDir);
    path.append("\\lietorch\\lib\\lietorch.dll");

    // HINSTANCE hGetProcIDDLL = LoadLibrary(path.data());
    HINSTANCE hGetProcIDDLL = LoadLibraryW(L"lietorch.dll");
    // dbg(hGetProcIDDLL);
    // dbg(GetLastError());
    EXPECT_TRUE(hGetProcIDDLL);
}
#endif

TEST(GENERAL, cuda_available) { EXPECT_TRUE(torch::hasCUDA()); }

TEST(GENERAL, lru_cache)
{
    // Check the workings of the double linked list implementation
    lietorch::caching::DoubleLinkedList<int, int> list{};
    list.add_page_to_head(1, 1);
    list.add_page_to_head(2, 2);
    list.add_page_to_head(3, 3);
    std::ostringstream stream;
    stream << list;
    list.remove_tail();
    stream << list;
    list.add_page_to_head(4, 4);
    stream << list;
    auto node = list.get_head()->next;
    list.move_page_to_head(node);
    stream << list;
    EXPECT_EQ(stream.str(), "(3, 2, 1)(3, 2)(4, 3, 2)(3, 4, 2)");

    // Check the tuple hashing functionality
    using std::hash;
    EXPECT_EQ(hash<float>()(1.0f), hash<float>()(1.0f));
    EXPECT_NE(hash<float>()(1.0f), hash<float>()(1.1f));

    using T = std::tuple<int, double, float>;
    EXPECT_EQ(hash<T>{}(T{42, 84.0, 1.0f}), hash<T>{}(T{42, 84.0, 1.0f}));
    EXPECT_NE(hash<T>{}(T{42, 84.0, 1.0f}), hash<T>{}(T{42, 84.0, 1.1f}));

    using lietorch::caching::LRUCache;
    using std::tuple;
    LRUCache<tuple<int, double>, double> cache(3);
    cache.store({1, 1.1}, -1.0);
    EXPECT_TRUE(cache.contains({1, 1.1}));
    EXPECT_FALSE(cache.contains({1, 1.2}));
    EXPECT_FALSE(cache.contains({2, 1.1}));

    EXPECT_EQ(cache.hits, 0);
    EXPECT_EQ(cache.lookup({1, 1.1}), -1.0);
    EXPECT_EQ(cache.hits, 1);

    cache.store({2, 2.2}, -2.0);
    cache.store({3, 3.3}, -3.0);
    cache.store({4, 4.4}, -4.0);

    EXPECT_EQ(cache.lookup({2, 2.2}), -2.0);
    EXPECT_EQ(cache.lookup({3, 3.3}), -3.0);
    EXPECT_EQ(cache.lookup({4, 4.4}), -4.0);
    EXPECT_FALSE(cache.lookup({1, 1.1}));

    EXPECT_EQ(cache.hits, 4);
    EXPECT_EQ(cache.misses, 1);

    cache.store({2, 2.2}, -5.0);
    EXPECT_EQ(cache.lookup({2, 2.2}), -5.0);
}
