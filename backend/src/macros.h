#pragma once
#ifndef LIETORCH_MACROS_H
#define LIETORCH_MACROS_H

/**
 * @file macros.h
 *
 */

#include <exception>

#define LT_ARGUMENT_CHECK(CHECK, FUNC, MSG)                                    \
    if (!(CHECK))                                                              \
    {                                                                          \
        throw std::runtime_error(">" #FUNC ": argument check failed: " #CHECK  \
                                 " : " #MSG);                                  \
    }

//

#endif