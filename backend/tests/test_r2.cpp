#include "../src/torch_include.h"
#include "gtest/gtest.h"

#include "../src/cpu/r2_cpu.h"
#include "../src/cuda/r2_cuda.cuh"
#include "../src/dbg.h"
#include "../src/lru_cache.h"
#include "../src/r2.h"

#include <chrono>

torch::Tensor
chb_cached(int orders, int height, int width)
{
    const auto opts =
        torch::TensorOptions().dtype(torch::kDouble).requires_grad(false);

    using namespace lietorch::caching;
    static LRUCache<std::tuple<int, int, int>, torch::Tensor> cache(3);

    if (const auto cached = cache.lookup({orders, height, width}))
    {
        return cached.value();
    }
    else
    {
        const auto basis =
            lietorch::r2::circular_harmonic_basis(orders, height, width, opts);
        cache.store({orders, height, width}, basis);
        return basis;
    }
}

torch::Tensor
chb_not_cached(int orders, int height, int width)
{
    const auto opts =
        torch::TensorOptions().dtype(torch::kDouble).requires_grad(false);

    return lietorch::r2::circular_harmonic_basis(orders, height, width, opts);
}

TEST(R2, circular_harmonic_basis)
{
    const auto opts =
        torch::TensorOptions().dtype(torch::kDouble).requires_grad(false);

    auto start = std::chrono::high_resolution_clock::now();
    for (int i = 0; i < 2; i++)
    {
        const auto x = chb_not_cached(5, 5, 5);
    }
    auto stop = std::chrono::high_resolution_clock::now();
    // dbg((std::chrono::duration<double>(stop - start).count()));

    start = std::chrono::high_resolution_clock::now();
    for (int i = 0; i < 2; i++)
    {
        const auto x = chb_cached(5, 5, 5);
    }
    stop = std::chrono::high_resolution_clock::now();
    // dbg((std::chrono::duration<double>(stop - start).count()));

    auto a = lietorch::r2::euclidean_norm_grid(5, 5, opts);
    // dbg(a);

    using namespace lietorch::caching;
    static LRUCache<std::tuple<int, int, torch::ScalarType>, torch::Tensor>
        cache(3);
    cache.store({1, 2, a.scalar_type()}, a);
    EXPECT_TRUE(a.allclose(cache.lookup({1, 2, a.scalar_type()}).value()));
}

TEST(R2, morphological_convolution_fw_cpu)
{
    {
        const auto options = torch::TensorOptions().dtype(torch::kFloat64);
        const double m = 10.0;

        auto f1 = torch::zeros({1, 1, 9, 9}, options);
        f1[0][0][4][4] = -1.0;
        const auto kernel1 =
            torch::tensor({{{m, 0.0, m}, {0.0, 0.0, m}, {m, 0.0, m}}}, options);

        const auto [out1_1, backindex1_1] =
            ::lietorch::r2::morphological_convolution_fw_cpu(f1, kernel1);
        const auto [out1_2, backindex1_2] =
            ::lietorch::r2::morphological_convolution_fw_cpu(out1_1, kernel1);
        const auto [out1_3, backindex1_3] =
            ::lietorch::r2::morphological_convolution_fw_cpu(out1_2, kernel1);
        const auto [out1_4, backindex1_4] =
            ::lietorch::r2::morphological_convolution_fw_cpu(out1_3, kernel1);

        // dbg(out1_4.abs());

        auto f2 = torch::zeros({1, 1, 3, 3}, options);
        f2[0][0][1][1] = -1.0;
        const auto kernel2 = torch::tensor(
            {{{m, 0.0, m}, {0.0, 0.0, 0.0}, {m, 0.0, m}}}, options);
        const auto [out2_1, backindex2_1] =
            ::lietorch::r2::morphological_convolution_fw_cpu(f2, kernel2);

        const auto out2_1_expected = torch::tensor(
            {{
                {{0, -1, 0}, {-1, -1, -1}, {0, -1, 0}},
            }},
            options);

        EXPECT_TRUE(out2_1.allclose(out2_1_expected, 1e-3));
    }

    {
        const auto options =
            torch::TensorOptions().dtype(torch::kFloat64).requires_grad(true);
        const double m = 10.0;

        auto f1 = torch::zeros({1, 1, 9, 9}, options).clone();
        f1[0][0][4][4] = -1.0;

        const auto kernel1 =
            torch::tensor({{{m, 0.0, m}, {0.0, 0.0, m}, {m, 0.0, m}}}, options);

        const auto [out1_1, backindex1_1] =
            ::lietorch::r2::morphological_convolution_fw_cpu(f1, kernel1);
        const auto [out1_2, backindex1_2] =
            ::lietorch::r2::morphological_convolution_fw_cpu(out1_1, kernel1);
        const auto [out1_3, backindex1_3] =
            ::lietorch::r2::morphological_convolution_fw_cpu(out1_2, kernel1);
        const auto [out1_4, backindex1_4] =
            ::lietorch::r2::morphological_convolution_fw_cpu(out1_3, kernel1);

        // dbg(out1_4.abs());

        auto f2 = torch::zeros({1, 1, 3, 3}, options).clone();
        f2[0][0][1][1] = -1.0;

        const auto kernel2 = torch::tensor(
            {{{m, 0.0, m}, {0.0, 0.0, 0.0}, {m, 0.0, m}}}, options);
        const auto [out2_1, backindex2_1] =
            ::lietorch::r2::morphological_convolution_fw_cpu(f2, kernel2);

        const auto out2_1_expected = torch::tensor(
            {{
                {{0, -1, 0}, {-1, -1, -1}, {0, -1, 0}},
            }},
            options);

        EXPECT_TRUE(out2_1.allclose(out2_1_expected, 1e-3));
    }

    {
        const auto options =
            torch::TensorOptions().dtype(torch::kFloat32).requires_grad(true);
        auto f = torch::randn({16, 32, 256, 256}, options);
        auto k = torch::randn({32, 5, 5}, options);

        auto start = std::chrono::high_resolution_clock::now();
        auto [out, backindex] =
            ::lietorch::r2::morphological_convolution_fw_cpu(f, k);
        auto stop = std::chrono::high_resolution_clock::now();
        // dbg((std::chrono::duration<double>(stop - start).count()));
    }
}

TEST(R2, morphological_convolution_fw_cuda)
{
    {
        const auto options =
            torch::TensorOptions().dtype(torch::kFloat64).device(torch::kCUDA);
        const double m = 10.0;

        auto f1 = torch::zeros({1, 1, 9, 9}, options);
        f1[0][0][4][4] = -1.0;
        const auto kernel1 =
            torch::tensor({{{m, 0.0, m}, {0.0, 0.0, m}, {m, 0.0, m}}}, options);

        const auto [out1_1, backindex1_1] =
            ::lietorch::r2::morphological_convolution_fw_cuda(f1, kernel1);
        const auto [out1_2, backindex1_2] =
            ::lietorch::r2::morphological_convolution_fw_cuda(out1_1, kernel1);
        const auto [out1_3, backindex1_3] =
            ::lietorch::r2::morphological_convolution_fw_cuda(out1_2, kernel1);
        const auto [out1_4, backindex1_4] =
            ::lietorch::r2::morphological_convolution_fw_cuda(out1_3, kernel1);

        // dbg(out1_4.abs());

        auto f2 = torch::zeros({1, 1, 3, 3}, options);
        f2[0][0][1][1] = -1.0;
        const auto kernel2 = torch::tensor(
            {{{m, 0.0, m}, {0.0, 0.0, 0.0}, {m, 0.0, m}}}, options);
        const auto [out2_1, backindex2_1] =
            ::lietorch::r2::morphological_convolution_fw_cuda(f2, kernel2);

        const auto out2_1_expected = torch::tensor(
            {{
                {{0, -1, 0}, {-1, -1, -1}, {0, -1, 0}},
            }},
            options);

        EXPECT_TRUE(out2_1.allclose(out2_1_expected, 1e-3));
    }

    {
        const auto options = torch::TensorOptions()
                                 .dtype(torch::kFloat64)
                                 .requires_grad(true)
                                 .device(torch::kCUDA);
        const double m = 10.0;

        auto f1 = torch::zeros({1, 1, 9, 9}, options).clone();
        f1[0][0][4][4] = -1.0;
        const auto kernel1 =
            torch::tensor({{{m, 0.0, m}, {0.0, 0.0, m}, {m, 0.0, m}}}, options);

        const auto [out1_1, backindex1_1] =
            ::lietorch::r2::morphological_convolution_fw_cuda(f1, kernel1);
        const auto [out1_2, backindex1_2] =
            ::lietorch::r2::morphological_convolution_fw_cuda(out1_1, kernel1);
        const auto [out1_3, backindex1_3] =
            ::lietorch::r2::morphological_convolution_fw_cuda(out1_2, kernel1);
        const auto [out1_4, backindex1_4] =
            ::lietorch::r2::morphological_convolution_fw_cuda(out1_3, kernel1);

        // dbg(out1_4.abs());

        auto f2 = torch::zeros({1, 1, 3, 3}, options).clone();
        f2[0][0][1][1] = -1.0;
        const auto kernel2 = torch::tensor(
            {{{m, 0.0, m}, {0.0, 0.0, 0.0}, {m, 0.0, m}}}, options);
        const auto [out2_1, backindex2_1] =
            ::lietorch::r2::morphological_convolution_fw_cuda(f2, kernel2);

        const auto out2_1_expected = torch::tensor(
            {{
                {{0, -1, 0}, {-1, -1, -1}, {0, -1, 0}},
            }},
            options);

        EXPECT_TRUE(out2_1.allclose(out2_1_expected, 1e-3));
    }

    {
        const auto options = torch::TensorOptions()
                                 .dtype(torch::kFloat32)
                                 .requires_grad(false)
                                 .device(torch::kCUDA);
        auto f = torch::randn({16, 32, 256, 256}, options);
        auto k = torch::randn({32, 5, 5}, options);

        auto start = std::chrono::high_resolution_clock::now();
        auto [out, backindex] =
            ::lietorch::r2::morphological_convolution_fw_cuda(f, k);
        auto stop = std::chrono::high_resolution_clock::now();
        // dbg((std::chrono::duration<double>(stop - start).count()));
    }
}

TEST(R2, morphological_convolution_fw_compare)
{
    const auto options =
        torch::TensorOptions().dtype(torch::kFloat32).device(torch::kCPU);

    const auto input = torch::randn({2, 3, 9, 9}, options);
    const auto kernel = torch::randn({3, 5, 5}, options);

    const auto [out_cpu, backindex_cpu] =
        ::lietorch::r2::morphological_convolution_fw(input, kernel);
    const auto [out_cuda, backindex_cuda] =
        ::lietorch::r2::morphological_convolution_fw(
            input.cuda(), kernel.cuda());

    EXPECT_TRUE(out_cpu.allclose(out_cuda.cpu(), 1e-4, 1e-5));

    const auto input2 = torch::randn({3, 2, 10, 10}, options);
    const auto kernel2 = torch::randn({2, 3, 3}, options);
    const auto [out2_cpu, backindex2_cpu] =
        ::lietorch::r2::morphological_convolution_fw(input2, kernel2);
    const auto [out2_cuda, backindex2_cuda] =
        ::lietorch::r2::morphological_convolution_fw(
            input2.cuda(), kernel2.cuda());

    EXPECT_TRUE(out2_cpu.allclose(out2_cuda.cpu(), 1e-4, 1e-5));
}

TEST(R2, morphological_convolution_bw_compare)
{
    const auto options = torch::TensorOptions()
                             .dtype(torch::kFloat64)
                             .device(torch::kCPU)
                             .requires_grad(true);

    const auto input = torch::randn({2, 3, 9, 9}, options);
    const auto kernel = torch::randn({3, 5, 5}, options);

    const auto [out_cpu, backindex_cpu] =
        ::lietorch::r2::morphological_convolution_fw(input, kernel);
    const auto [out_cuda, backindex_cuda] =
        ::lietorch::r2::morphological_convolution_fw(
            input.cuda(), kernel.cuda());

    EXPECT_TRUE(
        ((out_cpu - out_cuda.cpu()).abs() > 1e-3).sum().item().toInt() < 20);

    const auto grad = torch::randn_like(input);
    const auto [input_grad_cpu, kernel_grad_cpu] =
        ::lietorch::r2::morphological_convolution_bw(
            grad, backindex_cpu, kernel.sizes());

    EXPECT_EQ(input.sizes(), input_grad_cpu.sizes());
    EXPECT_EQ(kernel.sizes(), kernel_grad_cpu.sizes());

    const auto [input_grad_cuda, kernel_grad_cuda] =
        ::lietorch::r2::morphological_convolution_bw(
            grad.cuda(), backindex_cuda, kernel.sizes());

    EXPECT_EQ(input.sizes(), input_grad_cuda.sizes());
    EXPECT_EQ(kernel.sizes(), kernel_grad_cuda.sizes());

    EXPECT_LT(
        ((input_grad_cpu - input_grad_cuda.cpu()).abs() > 1e-3)
            .sum()
            .item()
            .toInt(),
        10);
    EXPECT_LT(
        ((kernel_grad_cpu - kernel_grad_cuda.cpu()).abs() > 1e-3)
            .sum()
            .item()
            .toInt(),
        10);
}

TEST(R2, morphological_kernel)
{
    const auto options = torch::TensorOptions(torch::kFloat32);
    auto p = torch::randn({16, 32, 5});
    auto pc = p.cuda();

    auto start = std::chrono::high_resolution_clock::now();
    auto k = lietorch::r2::morphological_kernel(p, 5, 0.65);
    auto stop = std::chrono::high_resolution_clock::now();
    // dbg((std::chrono::duration<double>(stop - start).count()));

    start = std::chrono::high_resolution_clock::now();
    auto k2 = lietorch::r2::morphological_kernel(p, 5, 0.65);
    stop = std::chrono::high_resolution_clock::now();
    // dbg((std::chrono::duration<double>(stop - start).count()));

    EXPECT_TRUE(k.allclose(k2));

    start = std::chrono::high_resolution_clock::now();
    auto k3 = lietorch::r2::morphological_kernel(pc, 5, 0.65);
    stop = std::chrono::high_resolution_clock::now();
    // dbg((std::chrono::duration<double>(stop - start).count()));

    start = std::chrono::high_resolution_clock::now();
    auto k4 = lietorch::r2::morphological_kernel(pc, 5, 0.65);
    stop = std::chrono::high_resolution_clock::now();
    // dbg((std::chrono::duration<double>(stop - start).count()));

    EXPECT_TRUE(k3.allclose(k4));

    auto p5 = torch::tensor({1.0, 1.0, 0.0}, options.requires_grad(true));
    auto k5 = lietorch::r2::morphological_kernel(p5, 2, 1.0);
    EXPECT_TRUE(k5.requires_grad());
}

TEST(R2, linear)
{
    const auto options = torch::TensorOptions(torch::kFloat32);
    const auto x = torch::tensor({{{{1}}, {{2}}}}, options);
    const auto w = torch::tensor({{1, 2, 3}, {-3, -2, -1}}, options);
    const auto y = lietorch::r2::linear(x, w);
    const auto y_expected = torch::tensor({{{-5}}, {{-2}}, {{1}}}, options);
    EXPECT_TRUE(y.allclose(y_expected));
}

TEST(R2, convection_cpu)
{
    const auto options =
        torch::TensorOptions(torch::kFloat32).requires_grad(true);
    const auto x = torch::tensor(
        {{{{0, 0, 0, 0, 0},
           {0, 0, 0, 0, 0},
           {0, 0, 1, 0, 0},
           {0, 0, 0, 0, 0},
           {0, 0, 0, 0, 0}}}},
        options);
    const auto v = torch::tensor({{-0.5, 1.2}}, options);

    const auto [y, gradfield] = lietorch::r2::convection_fw_cpu(x, v);

    // dbg(y, gradfield);

    const auto y_ex = torch::tensor(
        {{{{0.00, 0.00, 0.00, 0.00, 0.00},
           {0.00, 0.00, 0.00, 0.40, 0.10},
           {0.00, 0.00, 0.00, 0.40, 0.10},
           {0.00, 0.00, 0.00, 0.00, 0.00},
           {0.00, 0.00, 0.00, 0.00, 0.00}}}},
        options);
    const auto gradfield_ex = torch::tensor(
        {{{{{0.00, 0.00},
            {0.00, 0.00},
            {0.00, 0.00},
            {0.00, 0.00},
            {0.00, 0.00}},
           {{0.00, 0.00},
            {0.00, 0.00},
            {0.00, 0.00},
            {-0.8, -0.5},
            {-0.2, 0.5}},
           {{0.00, 0.00},
            {0.00, 0.00},
            {0.00, 0.00},
            {0.80, -0.5},
            {0.20, 0.50}},
           {{0.00, 0.00},
            {0.00, 0.00},
            {0.00, 0.00},
            {0.00, 0.00},
            {0.00, 0.00}},
           {{0.00, 0.00},
            {0.00, 0.00},
            {0.00, 0.00},
            {0.00, 0.00},
            {0.00, 0.00}}}}},
        options);
    EXPECT_TRUE(y.allclose(y_ex));
    EXPECT_TRUE(gradfield.allclose(gradfield_ex));

    const auto z = torch::tensor(
        {{{{0.00, 0.00, 0.00, 0.00, 0.00},
           {0.00, 0.00, 0.00, 1.00, 0.00},
           {0.00, 0.00, 0.00, 0.00, 0.00},
           {0.00, 0.00, 0.00, 0.00, 0.00},
           {0.00, 0.00, 0.00, 0.00, 0.00}}}},
        options);

    const auto [in_grad, v_grad] =
        lietorch::r2::convection_bw_cpu(v, z, gradfield);
    // dbg(in_grad, v_grad);

    const auto x2 =
        torch::tensor({{{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}}}, options);
    const auto v2 = torch::tensor({{0.1, 1.5}}, options);
    const auto [y2, gradfield2] = lietorch::r2::convection_fw_cpu(x2, v2);
    // dbg(y2, gradfield2);
    const auto [in_grad2, v_grad2] = lietorch::r2::convection_bw_cpu(
        v2, torch::ones({1, 1, 3, 3}, options), gradfield2);
    // dbg(in_grad2, v_grad2);
}

TEST(R2, convection_cuda)
{
    const auto options = torch::TensorOptions(torch::kFloat32)
                             .device(torch::kCUDA)
                             .requires_grad(true);
    const auto x = torch::tensor(
        {{{{0, 0, 0, 0, 0},
           {0, 0, 0, 0, 0},
           {0, 0, 1, 0, 0},
           {0, 0, 0, 0, 0},
           {0, 0, 0, 0, 0}}}},
        options);
    const auto v = torch::tensor({{-0.5, 1.2}}, options);

    const auto [y, gradfield] = lietorch::r2::convection_fw_cuda(x, v);

    // dbg(y, gradfield);

    const auto y_ex = torch::tensor(
        {{{{0.00, 0.00, 0.00, 0.00, 0.00},
           {0.00, 0.00, 0.00, 0.40, 0.10},
           {0.00, 0.00, 0.00, 0.40, 0.10},
           {0.00, 0.00, 0.00, 0.00, 0.00},
           {0.00, 0.00, 0.00, 0.00, 0.00}}}},
        options);
    const auto gradfield_ex = torch::tensor(
        {{{{{0.00, 0.00},
            {0.00, 0.00},
            {0.00, 0.00},
            {0.00, 0.00},
            {0.00, 0.00}},
           {{0.00, 0.00},
            {0.00, 0.00},
            {0.00, 0.00},
            {-0.8, -0.5},
            {-0.2, 0.5}},
           {{0.00, 0.00},
            {0.00, 0.00},
            {0.00, 0.00},
            {0.80, -0.5},
            {0.20, 0.50}},
           {{0.00, 0.00},
            {0.00, 0.00},
            {0.00, 0.00},
            {0.00, 0.00},
            {0.00, 0.00}},
           {{0.00, 0.00},
            {0.00, 0.00},
            {0.00, 0.00},
            {0.00, 0.00},
            {0.00, 0.00}}}}},
        options);
    EXPECT_TRUE(y.allclose(y_ex));
    EXPECT_TRUE(gradfield.allclose(gradfield_ex));

    const auto z = torch::tensor(
        {{{{0.00, 0.00, 0.00, 0.00, 0.00},
           {0.00, 0.00, 0.00, 1.00, 0.00},
           {0.00, 0.00, 0.00, 0.00, 0.00},
           {0.00, 0.00, 0.00, 0.00, 0.00},
           {0.00, 0.00, 0.00, 0.00, 0.00}}}},
        options);

    const auto [in_grad, v_grad] =
        lietorch::r2::convection_bw_cuda(v, z, gradfield);
    // dbg(in_grad, v_grad);

    const auto x2 =
        torch::tensor({{{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}}}, options);
    const auto v2 = torch::tensor({{0.1, 1.5}}, options);
    const auto [y2, gradfield2] = lietorch::r2::convection_fw_cuda(x2, v2);
    // dbg(y2, gradfield2);
    const auto [in_grad2, v_grad2] = lietorch::r2::convection_bw_cuda(
        v2, torch::ones({1, 1, 3, 3}, options), gradfield2);
    // dbg(in_grad2, v_grad2);
}