

#include "r2.h"
#include "cpu/r2_cpu.h"
#include "cuda/r2_cuda.cuh"
#include "dbg.h"
#include "lru_cache.h"
#include "macros.h"
#include "parallel.h"
#define _USE_MATH_DEFINES
#include <cmath>
#include <stdexcept>

namespace lietorch
{
namespace r2
{

/////////////////////////////////////////////////////
/////
/////   Utility functions for constructing kernels
/////
/////////////////////////////////////////////////////

/*

*/
torch::Tensor
circular_harmonic_basis(
    const int64_t orders,
    const int64_t height,
    int64_t width,
    const torch::TensorOptions& options)
{
    if (orders <= 0 || height <= 0 || width <= 0)
    {
        throw std::invalid_argument(
            "orders, height and width need to be positive");
    }

    const double rh = static_cast<double>(height - 1) / 2.0;
    const double rw = static_cast<double>(width - 1) / 2.0;
    const auto ys = torch::range(-rh, rh, 1.0, options);
    const auto xs = torch::range(-rw, rw, 1.0, options);

    const auto yx = torch::meshgrid({ys, xs}, "ij");
    const auto y = yx[0];
    const auto x = yx[1];

    const auto theta = torch::atan2(y, x);

    auto out = torch::empty({orders, height, width}, options);

    std::vector<int64_t> order_range(orders);
    std::iota(order_range.begin(), order_range.end(), 1);

    lietorch::for_each(
        order_range.begin(), order_range.end(), [&](const int64_t& order) {
            if (order == 1)
            {
                out[order - 1] = 1.0 / std::sqrt(2.0 * M_PI);
            }
            else if (order % 2 == 0)
            {
                const int64_t n = order / 2;
                out[order - 1] = torch::cos(n * theta).div(std::sqrt(M_PI));
            }
            else
            {
                const int64_t n = (order - 1) / 2;
                out[order - 1] = torch::sin(n * theta).div(std::sqrt(M_PI));
            }
        });

    return out;
}

/*

*/
torch::Tensor
euclidean_norm_grid(
    const int64_t height,
    const int64_t width,
    const torch::TensorOptions& options)
{
    if (height <= 0 || width <= 0)
    {
        throw std::invalid_argument("height and width need to be positive");
    }

    const double rh = static_cast<double>(height - 1) / 2.0;
    const double rw = static_cast<double>(width - 1) / 2.0;
    const auto ys = torch::range(-rh, rh, 1.0, options);
    const auto xs = torch::range(-rw, rw, 1.0, options);

    const auto yx = torch::meshgrid({ys, xs}, "ij");
    const auto y = yx[0];
    const auto x = yx[1];

    return torch::sqrt(x.pow(2) + y.pow(2));
}

/////////////////////////////////////////////////////
/////
/////   Morphological convolution
/////
/////////////////////////////////////////////////////

/*

 */
std::tuple<torch::Tensor, torch::Tensor>
morphological_convolution_fw(
    const torch::Tensor& input, const torch::Tensor& kernel)
{
    const auto input_arg = torch::TensorArg(input, "input", 1);
    const auto kernel_arg = torch::TensorArg(kernel, "kernel", 2);

    torch::checkAllDefined(__func__, {input_arg, kernel_arg});
    torch::checkDim(__func__, input_arg, 4);
    torch::checkDim(__func__, kernel_arg, 3);

    if (input.is_cuda())
    {
#if defined(WITH_CUDA)
        torch::checkAllSameGPU(__func__, {input_arg, kernel_arg});
        return morphological_convolution_fw_cuda(
            input.contiguous(), kernel.contiguous());
#else
        TORCH_CHECK(false, "Not compiled with CUDA support");
#endif
    }
    else
    {
        return morphological_convolution_fw_cpu(
            input.contiguous(), kernel.contiguous());
    }
}

/*

 */
std::tuple<torch::Tensor, torch::Tensor>
morphological_convolution_bw(
    const torch::Tensor& grad,
    const torch::Tensor& backindex,
    const torch::IntArrayRef kernel_sizes)
{
    const auto grad_arg = torch::TensorArg(grad, "grad", 1);
    const auto backindex_arg = torch::TensorArg(backindex, "backindex", 2);

    torch::checkAllDefined(__func__, {grad_arg, backindex_arg});
    torch::checkAllContiguous(__func__, {grad_arg, backindex_arg});
    torch::checkDim(__func__, grad_arg, 4);
    torch::checkDim(__func__, backindex_arg, 5);

    if (grad.is_cuda())
    {
#if defined(WITH_CUDA)
        torch::checkAllSameGPU(__func__, {grad_arg, backindex_arg});
        return morphological_convolution_bw_cuda(
            grad.contiguous(), backindex, kernel_sizes);
#else
        TORCH_CHECK(false, "Not compiled with CUDA support");
#endif
    }
    else
    {
        return morphological_convolution_bw_cpu(
            grad.contiguous(), backindex, kernel_sizes);
    }
}

/*


*/
torch::Tensor
morphological_convolution(
    const torch::Tensor& input, const torch::Tensor& kernel)
{
    return MorphologicalConvolution::apply(input, kernel)[0];
}

/*


*/
tensor_list
MorphologicalConvolution::forward(
    AutogradContext* ctx, torch::Tensor input, torch::Tensor kernel)
{
    const auto [out, back_index] =
        morphological_convolution_fw(input, kernel.to(input.scalar_type()));

    ctx->save_for_backward({back_index});
    ctx->saved_data["kernel_sizes"] = kernel.sizes();
    ctx->saved_data["kernel_scalar_type"] = kernel.scalar_type();

    return {out};
}

/*

*/
tensor_list
MorphologicalConvolution::backward(AutogradContext* ctx, tensor_list grads)
{
    const auto back_index = ctx->get_saved_variables()[0];
    const auto grad = grads[0];
    const auto kernel_sizes = ctx->saved_data["kernel_sizes"].toIntVector();
    const auto kernel_scalar_type =
        ctx->saved_data["kernel_scalar_type"].toScalarType();

    const auto [input_grad, kernel_grad] =
        morphological_convolution_bw(grad, back_index, kernel_sizes);

    return {input_grad, kernel_grad.to(kernel_scalar_type)};
}

////////////////////////////////////////////////
/////
/////   Morphological kernel
/////
////////////////////////////////////////////////

/*

 */
torch::Tensor
morphological_kernel(
    const torch::Tensor& finsler_params,
    const int64_t kernel_radius,
    const double alpha)
{
    using lietorch::caching::LRUCache;
    using std::tuple;
    static LRUCache<
        tuple<
            int64_t,
            int64_t,
            double,
            torch::ScalarType,
            torch::DeviceType,
            torch::DeviceIndex>,
        tuple<torch::Tensor, torch::Tensor>>
        cache(128);

    LT_ARGUMENT_CHECK(
        kernel_radius >= 0, __func__, "kernel_radius needs to be >= 0");
    LT_ARGUMENT_CHECK(
        alpha >= 0.55 && alpha <= 1.0,
        __func__,
        "alpha needs to be >= 0.55 and <=1.0");

    const int64_t order = finsler_params.size(-1);
    const int64_t ksize = 2 * kernel_radius + 1;
    const torch::ScalarType scalar = finsler_params.scalar_type();
    const torch::Device device = finsler_params.device();

    torch::Tensor a, b;

    if (const auto cached = cache.lookup(
            {order, ksize, alpha, scalar, device.type(), device.index()}))
    {
        a = std::get<0>(cached.value());
        b = std::get<1>(cached.value());
    }
    else
    {
        const auto options = torch::TensorOptions(scalar).device(device);
        const double beta = 2 * alpha / (2 * alpha - 1);
        const double c = (2 * alpha - 1) / pow(2 * alpha, beta);

        a = euclidean_norm_grid(ksize, ksize, options).pow(beta).mul(c);
        b = circular_harmonic_basis(order, ksize, ksize, options).mul(-beta);

        cache.store(
            {order, ksize, alpha, scalar, device.type(), device.index()},
            {a, b});
    }

    const auto w = finsler_params.unsqueeze(-1).unsqueeze(-1);
    return a * torch::exp(torch::sum(b * w, -3));
}

////////////////////////////////////////////////
/////
/////   Fractional Dilation
/////
////////////////////////////////////////////////

/*

 */
torch::Tensor
fractional_dilation(
    const torch::Tensor& input,
    const torch::Tensor& finsler_params,
    const int64_t kernel_radius,
    const double alpha)
{
    const auto input_arg = torch::TensorArg(input, "input", 1);
    const auto finsler_params_arg =
        torch::TensorArg(finsler_params, "finsler_params", 2);

    LT_ARGUMENT_CHECK(
        kernel_radius >= 0, __func__, "kernel_radius needs to be >= 0");
    LT_ARGUMENT_CHECK(
        alpha >= 0.55 && alpha <= 1.0,
        __func__,
        "alpha needs to be >= 0.55 and <=1.0");

    torch::checkAllDefined(__func__, {input_arg, finsler_params_arg});
    torch::checkDim(__func__, input_arg, 4);
    torch::checkDim(__func__, finsler_params_arg, 2);
    torch::checkSameType(__func__, input_arg, finsler_params_arg);

    const auto kernel =
        morphological_kernel(finsler_params, kernel_radius, alpha);

    return -morphological_convolution(-input, kernel);
}

/////////////////////////////////////////////////////
/////
/////   Fractional Erosion
/////
/////////////////////////////////////////////////////

/*

 */
torch::Tensor
fractional_erosion(
    const torch::Tensor& input,
    const torch::Tensor& finsler_params,
    const int64_t kernel_radius,
    const double alpha)
{
    const auto input_arg = torch::TensorArg(input, "input", 1);
    const auto finsler_params_arg =
        torch::TensorArg(finsler_params, "finsler_params", 2);

    LT_ARGUMENT_CHECK(
        kernel_radius >= 0, __func__, "kernel_radius needs to be >= 0");
    LT_ARGUMENT_CHECK(
        alpha >= 0.55 && alpha <= 1.0,
        __func__,
        "alpha needs to be >= 0.55 and <=1.0");

    torch::checkAllDefined(__func__, {input_arg, finsler_params_arg});
    torch::checkDim(__func__, input_arg, 4);
    torch::checkDim(__func__, finsler_params_arg, 2);
    torch::checkSameType(__func__, input_arg, finsler_params_arg);

    const auto kernel =
        morphological_kernel(finsler_params, kernel_radius, alpha);

    return morphological_convolution(input, kernel);
}

////////////////////////////////////////////////
/////
/////   Linear Combinations
/////
////////////////////////////////////////////////

/*

 */
torch::Tensor
linear(const torch::Tensor& input, const torch::Tensor& weight)
{
    const auto input2 = input.unsqueeze(-3);
    const auto weight2 = weight.unsqueeze(-1).unsqueeze(-1);
    return (input2 * weight2).sum(-4);
}

////////////////////////////////////////////////
/////
/////   Convection
/////
////////////////////////////////////////////////

/*

 */
std::tuple<torch::Tensor, torch::Tensor>
convection_fw(const torch::Tensor& input, const torch::Tensor& g0)
{
    const auto input_arg = torch::TensorArg(input, "input", 1);
    const auto g0_arg = torch::TensorArg(g0, "g0", 2);

    torch::checkAllDefined(__func__, {input_arg, g0_arg});
    torch::checkDim(__func__, input_arg, 4);
    torch::checkDim(__func__, g0_arg, 2);

    if (input.is_cuda())
    {
#if defined(WITH_CUDA)
        torch::checkAllSameGPU(__func__, {input_arg, g0_arg});
        return convection_fw_cuda(input.contiguous(), g0.contiguous());
#else
        TORCH_CHECK(false, "Not compiled with CUDA support");
#endif
    }
    else
    {
        return convection_fw_cpu(input.contiguous(), g0.contiguous());
    }
}

/*

 */
std::tuple<torch::Tensor, torch::Tensor>
convection_bw(
    const torch::Tensor& g0,
    const torch::Tensor& grad,
    const torch::Tensor& out_grad_field)
{
    const auto grad_arg = torch::TensorArg(grad, "grad", 1);
    const auto out_grad_field_arg =
        torch::TensorArg(out_grad_field, "out_grad_field", 2);
    const auto g0_arg = torch::TensorArg(g0, "g0", 3);

    torch::checkAllDefined(__func__, {grad_arg, out_grad_field_arg, g0_arg});
    torch::checkDim(__func__, grad_arg, 4);
    torch::checkDim(__func__, out_grad_field_arg, 5);
    torch::checkDim(__func__, g0_arg, 2);

    if (grad.is_cuda())
    {
#if defined(WITH_CUDA)
        torch::checkAllSameGPU(__func__, {grad_arg, out_grad_field_arg});
        return convection_bw_cuda(
            g0.contiguous(), grad.contiguous(), out_grad_field.contiguous());
#else
        TORCH_CHECK(false, "Not compiled with CUDA support");
#endif
    }
    else
    {
        return convection_bw_cpu(
            g0.contiguous(), grad.contiguous(), out_grad_field.contiguous());
    }
}

/*

 */
torch::Tensor
convection(const torch::Tensor& input, const torch::Tensor& g0)
{
    return Convection::apply(input, g0)[0];
}

tensor_list
Convection::forward(AutogradContext* ctx, torch::Tensor input, torch::Tensor g0)
{
    const auto [out, out_grad_field] = convection_fw(input, g0);

    ctx->save_for_backward({g0, out_grad_field});

    return {out};
}

tensor_list
Convection::backward(AutogradContext* ctx, tensor_list grads)
{
    const auto g0 = ctx->get_saved_variables()[0];
    const auto out_grad_field = ctx->get_saved_variables()[1];
    const auto grad = grads[0];

    const auto [input_grad, g0_grad] = convection_bw(g0, grad, out_grad_field);

    return {input_grad, g0_grad};
}

} // namespace r2
} // namespace lietorch