#pragma once
#ifndef LIETORCH_CPU_GENERIC_CPU_H
#define LIETORCH_CPU_GENERIC_CPU_H

/** @file generic_cpu.h CPU implementation of generic operations.
 *
 */

#include "../torch_include.h"

namespace lietorch
{
namespace generic
{

/**
 *
 *
 */
torch::Tensor
add_fw_cpu(const torch::Tensor& a, const torch::Tensor& b);

/**
 *
 *
 */
std::tuple<torch::Tensor, torch::Tensor>
add_bw_cpu(const torch::Tensor& grad);

/**
 *
 *
 */
std::tuple<torch::Tensor, torch::Tensor>
grayscale_dilation_2d_fw_cpu(
    const torch::Tensor& image, const torch::Tensor& filter);

/**
 *
 */
std::tuple<torch::Tensor, torch::Tensor>
grayscale_dilation_2d_bw_cpu(
    const torch::Tensor& backindex,
    const torch::Tensor& grad,
    const int64_t filter_h,
    const int64_t filter_w);

/**
 *
 */
std::tuple<torch::Tensor, torch::Tensor>
grayscale_erosion_2d_fw_cpu(
    const torch::Tensor& image, const torch::Tensor& filter);

/**
 *
 */
std::tuple<torch::Tensor, torch::Tensor>
grayscale_erosion_2d_bw_cpu(
    const torch::Tensor& backindex,
    const torch::Tensor& grad,
    const int64_t filter_h,
    const int64_t filter_w);

} // namespace generic
} // namespace lietorch

#endif