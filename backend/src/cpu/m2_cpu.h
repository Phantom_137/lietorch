#pragma once
#ifndef LIETORCH_CPU_M2_CPU_H
#define LIETORCH_CPU_M2_CPU_H

/** @file m2_cpu.h CPU implementation of M2 operations.
 *
 */

#include "../m2_element.h"
#include "../parallel.h"
#include "../torch_include.h"
#include "m2_interpolation_cpu.h"

namespace lietorch
{
namespace m2
{

/*

*/
std::tuple<torch::Tensor, torch::Tensor>
anisotropic_dilated_project_fw_cpu(
    const torch::Tensor& input,
    const double longitudinal,
    const double lateral,
    const double alpha,
    const double scale);

/*

*/
torch::Tensor
anisotropic_dilated_project_bw_cpu(
    const torch::Tensor& backindex,
    const torch::Tensor& grad,
    const torch::List<int64_t> input_shape);

/*

*/
std::tuple<torch::Tensor, torch::Tensor>
convection_fw_cpu(const torch::Tensor& input, const torch::Tensor& g0);

/*

*/
std::tuple<torch::Tensor, torch::Tensor>
convection_bw_cpu(
    const torch::Tensor& g0,
    const torch::Tensor& grad,
    const torch::Tensor& out_grad_field);

/*

*/
torch::Tensor
linear_convolution_fw_cpu(
    const torch::Tensor& input, const torch::Tensor& kernel);

/*

*/
std::tuple<torch::Tensor, torch::Tensor>
linear_convolution_bw_cpu(
    const torch::Tensor& grad,
    const torch::Tensor& input,
    const torch::Tensor& kernel);

/*

*/
std::tuple<torch::Tensor, torch::Tensor>
morphological_convolution_fw_cpu(
    const torch::Tensor& input, const torch::Tensor& kernel);

/*

*/
std::tuple<torch::Tensor, torch::Tensor>
morphological_convolution_bw_cpu(
    const torch::Tensor& grad,
    const torch::Tensor& backindex,
    const torch::IntArrayRef kernel_sizes);

/*

*/
torch::Tensor
rotated_kernel_stack_nearest_cpu(
    const torch::Tensor& kernel, const int64_t orientations);

/*

*/
template <typename scalar_t>
void
rotated_kernel_stack_trilinear_cpu(
    const torch::TensorAccessor<scalar_t, 4> kernel_a,
    torch::TensorAccessor<scalar_t, 5> kstack_a,
    const scalar_t boundary)
{
    using co_t = double;
    using element = ::lietorch::m2::element<co_t>;

    const auto ors = kstack_a.size(0);

    const auto kors = kernel_a.size(1);
    const auto kh = kernel_a.size(2);
    const auto kw = kernel_a.size(3);

    const co_t offset_t = static_cast<co_t>(kors - 1) / 2.0;
    const co_t offset_y = static_cast<co_t>(kh - 1) / 2.0;
    const co_t offset_x = static_cast<co_t>(kw - 1) / 2.0;

    const auto kors2 = kstack_a.size(2);
    const auto kh2 = kstack_a.size(3);
    const auto kw2 = kstack_a.size(4);

    const element g0(0.0, offset_y, offset_x);

    std::vector<int64_t> channel_range(kernel_a.size(0));
    std::iota(channel_range.begin(), channel_range.end(), 0);

    lietorch::for_each(
        channel_range.begin(), channel_range.end(), [&](const int64_t& c) {
            for (int ori = 0; ori < ors; ori ++)
            {
                const co_t theta = static_cast<co_t>(2.0 * M_PI) *
                                   static_cast<co_t>(ori) /
                                   static_cast<co_t>(ors);
                const element gtheta(theta, 0.0f, 0.0f); // rotate
                // shift center to origin, rotate and shift back.
                const element grot = g0 * gtheta.inv() * g0.inv();

                for (int i = 0; i < kors2; i++)
                {
                    for (int j = 0; j < kh2; j++)
                    {
                        for (int k = 0; k < kw2; k++)
                        {
                            const co_t i_sample = static_cast<co_t>(i * kors) /
                                                  static_cast<co_t>(kors2);
                            const co_t j_sample = static_cast<co_t>(j * kh) /
                                                  static_cast<co_t>(kh2);
                            const co_t k_sample = static_cast<co_t>(k * kw) /
                                                  static_cast<co_t>(kw2);
                            const element gker(
                                i_sample,
                                j_sample,
                                k_sample,
                                static_cast<co_t>(kors));
                            const element gsample = grot * gker;

                            kstack_a[ori][c][i][j][k] =
                                interpolate_trilinear<scalar_t, co_t>(
                                    kernel_a[c],
                                    i_sample,
                                    gsample.y,
                                    gsample.x,
                                    boundary);
                        }
                    }
                }
            }
        });
}

/*

*/
torch::Tensor
linear_fw_cpu(const torch::Tensor& input, const torch::Tensor& weight);

/*

*/
std::tuple<torch::Tensor, torch::Tensor>
linear_bw_cpu(
    const torch::Tensor& grad,
    const torch::Tensor& input,
    const torch::Tensor& weight);

} // namespace m2
} // namespace lietorch

#endif
