#pragma once
#ifndef LIETORCH_M2_ELEMENT_H
#define LIETORCH_M2_ELEMENT_H

/** @file m2_element.h
 *  @brief M2 group element representation and computations.
 *
 */

#include "vformat.h"

#ifdef __CUDA_ARCH__

#include "cuda/math_cuda.cuh"
#define FMOD(T, X, Y) ::lietorch::lt_cu_fmod<T>((X), (Y))
#define SPEC __forceinline__ __device__ __host__ constexpr
#define PI(T) ::lietorch::LT_CU_PI<T>()

#else

#define _USE_MATH_DEFINES
#include <math.h>
#define SPEC inline constexpr
#define PI(T) static_cast<T>(M_PI)
#define FMOD(T, X, Y) fmod((X), (Y))

#endif

namespace lietorch
{
namespace m2
{

/**
 * @class element
 * @brief Represents an element of M2 in `(theta, y, x)` coordinates with
 * support for measure the orientation dimension either in radians or tensor
 * coordinates.
 *
 * @tparam T Coordinate type.
 *
 * @param t
 * @param y
 * @param x
 * @param t_scale Determines how the t coordinate is interpreted.
 * With the default `t_scale=2*PI` the t coordinate will be interpreted as
 * radians (i.e. theta==t). To facilitate calculations with tensor coordinates
 * (indices) this parameter can be set to the size of the tensor in the
 * orientation dimension, the actual angle used for calculations will then be
 * `theta=2*PI*t/t_scale`.
 *
 */
template <typename T = float> struct alignas(T) element
{
    const T t; /**< Orientation coordinate, but not necessarily in radians. */
    const T y; /**< Y coordinate. */
    const T x; /**< X coordinate. */
    const T t_scale; /**< How to interpret the orientation coordinate, i.e. how
                        much t needs to increase or decrease to complete one
                        full rotation. */
    const T theta;   /**< Orientation coordinate in radians, regardless of the
                        scale of `t`. */

#pragma warning(disable : 4244)
    // Constructor
    SPEC element(T _t, T _y, T _x, T _t_scale = 2.0 * PI(T))
        : t(FMOD(T, _t, _t_scale)), y(_y), x(_x), t_scale(_t_scale),
          theta(FMOD(T, 2.0 * PI(T) * _t / _t_scale, 2 * PI(T)))
    {
        if (t_scale <= 0.0)
        {
#ifdef __CUDA_ARCH__

#else
            throw std::invalid_argument(vformat(
                "lietorch::m2::element constructor: t_scale needs to be "
                "strictly positive but equals %4.2f",
                t_scale));
#endif
        }
    }

    SPEC element(int64_t _t, int64_t _y, int64_t _x, int64_t _t_scale)
        : element(
              static_cast<T>(_t),
              static_cast<T>(_x),
              static_cast<T>(_y),
              static_cast<T>(_t_scale))
    {
    }
#pragma warning(default : 4244)

    // Copy constructor
    SPEC element(const element& g)
        : t(g.t), y(g.y), x(g.x), t_scale(g.t_scale), theta(g.theta)
    {
    }

    // Move constructor
    SPEC element(const element&& g)
        : t(g.t), y(g.y), x(g.x), t_scale(g.t_scale), theta(g.theta)
    {
    }

    /**
     * @brief Comparison (exact).
     */
    SPEC bool operator==(const element& g) const noexcept
    {
        return FMOD(T, theta - g.theta, 2 * PI(T)) == 0.0 && y == g.y &&
               x == g.x;
    }

    /**
     * @brief Comparison with a tolerance.
     */
    SPEC bool
    close_to(const element& g, T rtol = 1e-4, T atol = 1e-4) const noexcept
    {
        return abs(FMOD(T, theta - g.theta, 2 * PI(T))) <
                   atol + rtol * abs(g.theta) &&
               abs(y - g.y) < atol + rtol * abs(g.y) &&
               abs(x - g.x) < atol + rtol * abs(g.x);
    }

    /**
     * @brief Inverse element, the return value will have the same
     * `t_scale`.
     */
    SPEC element inv() const noexcept
    {
#ifdef __CUDA_ARCH__
        const auto cs = lt_cu_cossin<T>(theta);
        const T c = std::get<0>(cs);
        const T s = std::get<1>(cs);
#else
        const T c = cos(theta);
        const T s = sin(theta);
#endif
        return element<T>(-t, s * x - c * y, -c * x - s * y, t_scale);
    }

    /**
     * @brief Group product, the two element need not have the same
     * `t_scale`, but the returned element will have the same
     * `t_scale` as the first element.
     */
    SPEC element operator*(const element& g) const noexcept
    {
#ifdef __CUDA_ARCH__
        const auto cs = lt_cu_cossin<T>(theta);
        const T c = std::get<0>(cs);
        const T s = std::get<1>(cs);
#else
        const T c = cos(theta);
        const T s = sin(theta);
#endif

        const auto t_new = t + g.t * t_scale / g.t_scale;
        const auto y_new = y + s * g.x + c * g.y;
        const auto x_new = x + c * g.x - s * g.y;

        return element<T>(t_new, y_new, x_new, t_scale);
    }

    /**
     * @brief Returns the unit element.
     */
    static SPEC element e() noexcept { return element(0.0, 0.0, 0.0); }

    // cout << support
    friend std::ostream& operator<<(std::ostream& out, const element<T>& g)
    {
        out << vformat(
            "m2::element(% 3.2f/%3.2f,% 4.2f,% 4.2f)",
            g.t,
            g.t_scale,
            g.y,
            g.x);

        return out;
    }
}; // namespace m2

} // namespace m2
} // namespace lietorch

#undef PI
#undef SPEC
#undef FMOD

#endif